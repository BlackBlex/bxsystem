<?php

declare(strict_types=1);

namespace Filament;

use Closure;
use Filament\Support\Components\Component;
use Filament\Support\Facades\FilamentColor;
use Filament\Support\Facades\FilamentIcon;
use Filament\Support\Facades\FilamentView;

class Panel extends Component
{
    use Panel\Concerns\HasAssets;
    use Panel\Concerns\HasAuth;
    use Panel\Concerns\HasAvatars;
    use Panel\Concerns\HasBrandLogo;
    use Panel\Concerns\HasBrandName;
    use Panel\Concerns\HasBreadcrumbs;
    use Panel\Concerns\HasColors;
    use Panel\Concerns\HasComponents;
    use Panel\Concerns\HasDarkMode;
    use Panel\Concerns\HasFavicon;
    use Panel\Concerns\HasFont;
    use Panel\Concerns\HasGlobalSearch;
    use Panel\Concerns\HasIcons;
    use Panel\Concerns\HasId;
    use Panel\Concerns\HasMaxContentWidth;
    use Panel\Concerns\HasMiddleware;
    use Panel\Concerns\HasNavigation;
    use Panel\Concerns\HasNotifications;
    use Panel\Concerns\HasPlugins;
    use Panel\Concerns\HasRenderHooks;
    use Panel\Concerns\HasRoutes;
    use Panel\Concerns\HasSidebar;
    use Panel\Concerns\HasSpaMode;
    use Panel\Concerns\HasTenancy;
    use Panel\Concerns\HasTheme;
    use Panel\Concerns\HasTopbar;
    use Panel\Concerns\HasTopNavigation;
    use Panel\Concerns\HasUnsavedChangesAlerts;
    use Panel\Concerns\HasUserMenu;

    public const BYPASSPERMISSION = 'permission_by_pass';

    protected ?Closure $bootUsing = null;

    protected bool $isDefault = false;

    /**
     * @var array<string>
     */
    protected array $permissions = [];

    public static function make(): static
    {
        $static = app(static::class);
        $static->configure();

        return $static;
    }

    public function boot(): void
    {
        $this->registerAssets();

        FilamentColor::register($this->getColors());

        FilamentIcon::register($this->getIcons());

        FilamentView::spa($this->hasSpaMode());

        $this->registerRenderHooks();

        foreach ($this->plugins as $plugin) {
            $plugin->boot($this);
        }

        if ($callback = $this->bootUsing) {
            $callback($this);
        }
    }

    public function bootUsing(?Closure $callback): static
    {
        $this->bootUsing = $callback;

        return $this;
    }

    public function default(bool $condition = true): static
    {
        $this->isDefault = $condition;

        return $this;
    }

    /**
     * Get what kind of permission need to access
     *
     * @return array<string> $permissions
     */
    public function getPermission(): array
    {
        return $this->permissions;
    }

    public function isDefault(): bool
    {
        return $this->isDefault;
    }

    /**
     * Set what kind of permission need to access
     *
     * @param  array<string>  $permissions
     */
    public function permission(array $permissions): static
    {
        $this->permissions = $permissions;

        return $this;
    }

    public function register(): void
    {
        $this->registerLivewireComponents();
        $this->registerLivewirePersistentMiddleware();

        if (app()->runningInConsole()) {
            $this->registerAssets();
        }
    }
}
