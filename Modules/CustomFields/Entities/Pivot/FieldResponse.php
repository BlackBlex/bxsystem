<?php

declare(strict_types=1);

namespace Modules\CustomFields\Entities\Pivot;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\CustomFields\Database\Factories\FieldResponseFactory;
use Modules\CustomFields\Entities\Field;
use Modules\CustomFields\Enums\FieldRule;
use Modules\CustomFields\Enums\FieldType;

class FieldResponse extends Model
{
    use HasFactory;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<string>
     */
    protected $fillable = ['value'];

    /**
     * Get the field that owns the FieldResponse
     */
    public function field(): BelongsTo
    {
        return $this->belongsTo(Field::class, 'field_id');
    }

    /**
     * Get the model that owns the FieldResponse
     */
    public function model(): MorphTo
    {
        return $this->morphTo('model');
    }

    protected static function newFactory(): FieldResponseFactory
    {
        return FieldResponseFactory::new();
    }

    /**
     * Cast value in this corresponsive type
     *
     * @return Attribute<mixed, string>
     */
    protected function value(): Attribute
    {
        return new Attribute(
            get: function (?string $value): mixed {
                if ($value === null) {
                    return null;
                }

                if ($this->field->type->getType() === FieldType::Toggle) {
                    return (bool) $value;
                }

                if ($this->field->type->getRule() === FieldRule::Numeric || $this->field->type->getType() === FieldType::Select) {
                    return (int) $value;
                }

                if ($this->field->type->getType() === FieldType::File || $this->field->type->getType() === FieldType::CheckboxList) {
                    return (array) \Safe\json_decode($value !== '' ? $value : '{}');
                }

                return $value;
            },
            set: function (mixed $value): string {
                if (is_array($value)) {
                    return \Safe\json_encode($value);
                }

                if ($this->field->type->getType() === FieldType::Toggle) {
                    return (string) ((int) $value);
                }

                return (string) $value;
            },
        );
    }
}
