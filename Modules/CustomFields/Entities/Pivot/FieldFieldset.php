<?php

declare(strict_types=1);

namespace Modules\CustomFields\Entities\Pivot;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\CustomFields\Entities\Field;
use Modules\CustomFields\Entities\Fieldset;

class FieldFieldset extends Pivot
{
    use SoftDeletes;

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'required' => 'boolean',
        'searchable' => 'boolean',
        'show_in_table' => 'boolean',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array<string>
     */
    protected $fillable = ['order', 'required', 'searchable', 'show_in_table'];

    public function field(): BelongsTo
    {
        return $this->belongsTo(Field::class);
    }

    public function fieldset(): BelongsTo
    {
        return $this->belongsTo(Fieldset::class);
    }
}
