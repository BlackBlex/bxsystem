<?php

declare(strict_types=1);

namespace Modules\CustomFields\Entities\Pivot;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\CustomFields\Database\Factories\FieldsetModelFactory;
use Modules\CustomFields\Entities\Fieldset;

class FieldsetModel extends Model
{
    use HasFactory;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<string>
     */
    protected $fillable = ['model_type'];

    /**
     * Get the field that owns the FieldsetModel
     */
    public function fieldset(): BelongsTo
    {
        return $this->belongsTo(Fieldset::class, 'fieldset_id');
    }

    protected static function newFactory(): FieldsetModelFactory
    {
        return FieldsetModelFactory::new();
    }
}
