<?php

declare(strict_types=1);

namespace Modules\CustomFields\Entities;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\CustomFields\Casts\FieldTypeCast;
use Modules\CustomFields\Database\Factories\FieldFactory;
use Modules\CustomFields\Entities\Pivot\FieldFieldset;
use Modules\CustomFields\Entities\Pivot\FieldResponse;
use Modules\CustomFields\Enums\FieldRule;
use Modules\CustomFields\Enums\FieldType;

class Field extends Model
{
    use HasFactory;
    use SoftDeletes;

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'options' => 'array',
        'type' => FieldTypeCast::class,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array<string>
     */
    protected $fillable = ['type', 'title', 'hint', 'options', 'default_value', 'format'];

    /**
     * The fieldsets that belong to the Field
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany<Fieldset>
     */
    public function fieldsets(): BelongsToMany
    {
        return $this->belongsToMany(Fieldset::class)
            ->wherePivotNull('deleted_at')
            ->withPivot(['order', 'required', 'searchable', 'show_in_table'])
            ->withTimestamps()
            ->using(FieldFieldset::class);
    }

    /**
     * Get all of the responses for the Field
     */
    public function responses(): HasMany
    {
        return $this->hasMany(FieldResponse::class, 'field_id');
    }

    protected static function newFactory(): FieldFactory
    {
        return FieldFactory::new();
    }

    /**
     * Cast default value in this corresponsive type
     *
     * @return Attribute<mixed, never>
     */
    protected function defaultValue(): Attribute
    {
        return new Attribute(
            get: function (?string $value): mixed {
                if ($this->type->getType() === FieldType::Toggle) {
                    return (bool) $value;
                }

                if ($this->type->getRule() === FieldRule::Numeric || $this->type->getType() === FieldType::Select) {
                    return (int) $value;
                }

                if ($this->type->getType() === FieldType::File || $this->type->getType() === FieldType::CheckboxList) {
                    return (array) \Safe\json_decode($value !== null ? $value : '{}');
                }

                return $value;
            }
        );
    }

    /**
     * Return the rule for current field
     */
    protected function rule(): Attribute
    {
        return new Attribute(
            get: fn (): FieldRule => $this->type->getRule()
        );
    }
}
