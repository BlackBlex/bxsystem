<?php

declare(strict_types=1);

namespace Modules\CustomFields\Entities\Types;

use Filament\Forms\Components\Actions\Action;
use Filament\Forms\Components\KeyValue;
use Filament\Forms\Components\Select as FilamentSelect;
use Filament\Forms\Get;
use Modules\CustomFields\Entities\Field;
use Modules\CustomFields\Enums\FieldRule;
use Modules\CustomFields\Enums\FieldType;

class Select extends Base
{
    /**
     * Get rules that only correspond to the type
     *
     * @return array<FieldRule>
     */
    public static function getAvailableRules(): array
    {
        return [];
    }

    public function getFilamentComponent(string $identificator, Field $fieldInstance): FilamentSelect
    {
        $result = FilamentSelect::make($identificator);

        if ($fieldInstance->options !== null) {
            $result->options($fieldInstance->options);
        }

        return $result;
    }

    public function getFormOptions(): array
    {
        $result = [];

        $result[] = FilamentSelect::make('default_value_select')
            ->label(__('default_value'))
            ->options(fn (Get $get): array => $get('options'))
            ->searchable();

        $result[] = KeyValue::make('options')
            ->label(__('options'))
            ->addActionLabel(__('options.add'))
            ->addAction(
                fn (Action $action) => $action
                    ->button(),
            )
            ->columnSpanFull()
            ->keyPlaceholder(__('value.internal'))
            ->reorderable()
            ->required()
            ->valuePlaceholder(__('value.to.show'));

        return $result;
    }

    public function getType(): FieldType
    {
        return FieldType::Select;
    }

    public function handleRequestOnFieldCreateSave(array $data): array
    {
        $data['default_value'] = $data['default_value_select'];
        unset($data['default_value_select']);

        return $data;
    }

    public function handleRequestOnFieldFill(array $data): array
    {
        $data['default_value_select'] = $data['default_value'];

        return $data;
    }

    public function setMaxSize(int $newMaxSize): static
    {
        return $this;
    }

    public function setMinSize(int $newMinSize): static
    {
        return $this;
    }

    public function setSize(int $newSize): static
    {
        return $this;
    }
}
