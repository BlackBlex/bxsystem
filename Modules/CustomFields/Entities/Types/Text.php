<?php

declare(strict_types=1);

namespace Modules\CustomFields\Entities\Types;

use Filament\Forms\Components\Group;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Get;
use Illuminate\Support\Facades\Crypt;
use Modules\CustomFields\Entities\Field;
use Modules\CustomFields\Enums\FieldRule;
use Modules\CustomFields\Enums\FieldType;
use Rawilk\FilamentPasswordInput\Password;

class Text extends Base
{
    /**
     * Get rules that only correspond to the type
     *
     * @return array<FieldRule>
     */
    public static function getAvailableRules(): array
    {
        $allFieldRules = FieldRule::cases();
        $excludeRules = [FieldRule::Date, FieldRule::AfterDate, FieldRule::BeforeDate];

        $result = array_filter($allFieldRules, function (FieldRule $rule) use ($excludeRules) {
            return ! in_array($rule, $excludeRules, true);
        });

        return $result;
    }

    public function getFilamentComponent(string $identificator, ?Field $fieldInstance): TextInput
    {
        $result = TextInput::make($identificator);

        if ($this->getRule() == FieldRule::Password) {
            $result = Password::make($identificator)
                ->copyable()
                ->hidePasswordManagerIcons()
                ->regeneratePassword();
        }

        if ($this->getMinSize() !== 0) {
            $result->minLength($this->getMinSize());
        }

        if ($this->getMaxSize() !== -1) {
            $result->maxLength($this->getMaxSize());
        }

        if ($this->getSize() !== -1) {
            $result->length($this->getSize());
        }

        $result->alpha($this->getRule() === FieldRule::Alpha);
        $result->alphaDash($this->getRule() === FieldRule::AlphaDash);
        $result->alphaNum($this->getRule() === FieldRule::AlphaNumeric);
        $result->email($this->getRule() === FieldRule::Email);
        $result->ip($this->getRule() === FieldRule::IP);
        $result->ipv4($this->getRule() === FieldRule::IPv4);
        $result->ipv6($this->getRule() === FieldRule::IPv6);
        $result->macAddress($this->getRule() === FieldRule::MAC);
        $result->numeric($this->getRule() === FieldRule::Numeric);
        $result->activeUrl($this->getRule() === FieldRule::URL);

        if ($this->getRule() === FieldRule::Telephone) {
            $result->tel();
            $result->telRegex('/^[0-9]{10}$/');
            $result->minLength(10);
            $result->maxLength(10);
            $result->length(10);
        }

        if ($fieldInstance !== null) {
            if ($this->getRule() === FieldRule::Regex) {
                $result->regex($fieldInstance->format);
            } elseif ($fieldInstance->format !== null && str_contains($fieldInstance->format, '=')) {
                $data = explode('=', $fieldInstance->format);
                $format = $data[0];
                $placeholder = $data[1];

                $result->mask($format);
                $result->placeholder($placeholder);
            }
        }

        return $result;
    }

    public function getFormOptions(): array
    {
        $result = parent::getSizesFormOptions();

        $result[] = Group::make()
            ->columns(2)
            ->schema([
                Select::make('rule')
                    ->columnSpan(fn (Get $get) => $get('rule') === FieldRule::Regex ? 1 : 'full')
                    ->label(__('rule'))
                    ->live(onBlur: false, debounce: 10)
                    ->options(function (): array {
                        $options = $this->getAvailableRules();
                        $flattenOptions = [];

                        foreach ($options as $option) {
                            $flattenOptions[$option->value] = $option->getLabel();
                        }

                        return $flattenOptions;
                    })
                    ->default(FieldRule::Any),
                TextInput::make('format')
                    ->reactive()
                    ->label(__('rules.regex'))
                    ->visible(fn (Get $get) => $get('rule') === FieldRule::Regex ? true : false),
            ]);

        $result[] = TextInput::make('default_value_text')
            ->label(__('default_value'))
            ->alpha(fn (Get $get) => $get('rule') === FieldRule::Alpha)
            ->alphaDash(fn (Get $get) => $get('rule') === FieldRule::AlphaDash)
            ->alphaNum(fn (Get $get) => $get('rule') === FieldRule::AlphaNumeric)
            ->email(fn (Get $get) => $get('rule') === FieldRule::Email)
            ->ip(fn (Get $get) => $get('rule') === FieldRule::IP)
            ->ipv4(fn (Get $get) => $get('rule') === FieldRule::IPv4)
            ->ipv6(fn (Get $get) => $get('rule') === FieldRule::IPv6)
            ->macAddress(fn (Get $get) => $get('rule') === FieldRule::MAC)
            ->numeric(fn (Get $get) => $get('rule') === FieldRule::Numeric)
            ->activeUrl(fn (Get $get) => $get('rule') === FieldRule::URL)
            ->tel(fn (Get $get) => $get('rule') === FieldRule::Telephone)
            ->regex(fn (Get $get) => $get('rule') === FieldRule::Regex ? $get('format') : ($get('rule') === FieldRule::Telephone ? '/^[0-9]{10}$/' : null))
            ->visible(fn (Get $get) => $get('type') === FieldType::Text && $get('rule') !== FieldRule::Password);

        $result[] = Password::make('default_value_password')
            ->label(__('default_value'))
            ->copyable()
            ->regeneratePassword()
            ->visible(fn (Get $get) => $get('type') === FieldType::Text && $get('rule') === FieldRule::Password);

        return $result;
    }

    public function getType(): FieldType
    {
        return FieldType::Text;
    }

    public function handleRequestOnFieldCreateSave(array $data): array
    {
        $data['size'] = (int) $data['size'];
        $data['min_size'] = (int) $data['min_size'];
        $data['max_size'] = (int) $data['max_size'];

        $data['size'] = $data['size'] < -1 ? -1 : $data['size'];
        $data['min_size'] = $data['min_size'] < 0 ? 0 : $data['min_size'];
        $data['max_size'] = $data['max_size'] < -1 ? -1 : $data['max_size'];

        $data = parent::handleSizeRequest($data);

        if ($data['rule'] !== null) {
            $this->setRule($data['rule']);
            unset($data['rule']);
        }

        if ($this->getRule() === FieldRule::Password) {
            if ($data['default_value_password'] !== null) {
                $data['default_value'] = Crypt::encrypt($data['default_value_password']);
            } else {
                $data['default_value'] = null;
            }

            unset($data['default_value_password']);
        } else {
            $data['default_value'] = $data['default_value_text'];
            unset($data['default_value_text']);
        }

        return $data;
    }

    public function handleRequestOnFieldFill(array $data): array
    {
        $data['rule'] = $this->getRule();
        $data['size'] = $this->getSize();
        $data['min_size'] = $this->getMinSize();
        $data['max_size'] = $this->getMaxSize();

        if ($this->getRule() === FieldRule::Password) {
            if ($data['default_value'] !== null) {
                $data['default_value_password'] = Crypt::decrypt($data['default_value']);
            }
        } else {
            $data['default_value_text'] = $data['default_value'];
        }

        return $data;
    }

    public function setMaxSize(int $newMaxSize): static
    {
        if ($newMaxSize < $this->size) {
            $this->size = $newMaxSize;
        }

        $this->maxSize = $newMaxSize;

        return $this;
    }

    public function setMinSize(int $newMinSize): static
    {
        if ($newMinSize > $this->size) {
            $this->size = $newMinSize;
        }

        $this->minSize = $newMinSize;

        return $this;
    }

    public function setSize(int $newSize): static
    {
        if ($this->maxSize !== -1 && $newSize > $this->maxSize) {
            $this->maxSize = $newSize;
        }

        if ($this->maxSize !== 0 && $newSize < $this->minSize) {
            $this->minSize = $newSize;
        }

        $this->size = $newSize;

        return $this;
    }
}
