<?php

declare(strict_types=1);

namespace Modules\CustomFields\Entities\Types;

use Filament\Forms\Components\Toggle as FilamentToggle;
use Modules\CustomFields\Entities\Field;
use Modules\CustomFields\Enums\FieldRule;
use Modules\CustomFields\Enums\FieldType;

class Toggle extends Base
{
    /**
     * Get rules that only correspond to the type
     *
     * @return array<FieldRule>
     */
    public static function getAvailableRules(): array
    {
        return [];
    }

    public function getFilamentComponent(string $identificator, Field $fieldInstance): FilamentToggle
    {
        $result = FilamentToggle::make($identificator);

        return $result;
    }

    public function getFormOptions(): array
    {
        $result = [];

        $result[] = FilamentToggle::make('default_value_bool')
            ->label(__('default_value'));

        return $result;
    }

    public function getType(): FieldType
    {
        return FieldType::Toggle;
    }

    public function handleRequestOnFieldCreateSave(array $data): array
    {
        $data['default_value'] = $data['default_value_bool'];
        unset($data['default_value_bool']);

        return $data;
    }

    public function handleRequestOnFieldFill(array $data): array
    {
        $data['default_value_bool'] = $data['default_value'];

        return $data;
    }

    public function setMaxSize(int $newMaxSize): static
    {
        return $this;
    }

    public function setMinSize(int $newMinSize): static
    {
        return $this;
    }

    public function setSize(int $newSize): static
    {
        return $this;
    }
}
