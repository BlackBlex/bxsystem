<?php

declare(strict_types=1);

namespace Modules\CustomFields\Entities\Types;

use Filament\Forms\Components\FileUpload;
use Filament\Forms\Components\Group;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\TextInput;
use Illuminate\Database\Eloquent\Model;
use Livewire\Features\SupportFileUploads\TemporaryUploadedFile;
use Modules\CustomFields\Entities\Field;
use Modules\CustomFields\Enums\FieldRule;
use Modules\CustomFields\Enums\FieldType;
use Modules\CustomFields\Enums\FileType;

class File extends Base
{
    protected string $filename = '';

    protected FileType $fileTypeAllow = FileType::Any;

    /**
     * Get rules that only correspond to the type
     *
     * @return array<FieldRule>
     */
    public static function getAvailableRules(): array
    {
        return [];
    }

    public function deserialize(array $data): bool
    {
        if (parent::deserialize($data)) {
            $this->filename = $data[4];
            $this->fileTypeAllow = FileType::tryFromName($data[5]);

            return true;
        }

        return false;
    }

    public function getFilamentComponent(string $identificator, Field $fieldInstance): FileUpload
    {
        $result = FileUpload::make($identificator);

        $result->multiple($this->getMinSize() > 1 || $this->getMaxSize() > 1)
            ->disk('uploads')
            ->directory(str_replace(' ', '_', strtolower($fieldInstance->title)))
            ->visibility('private')
            ->loadingIndicatorPosition('left')
            ->removeUploadedFileButtonPosition('right')
            ->openable();

        $result->getUploadedFileNameForStorageUsing(
            function (TemporaryUploadedFile $file, ?Model $record): string {
                $result = str($file->getClientOriginalExtension())
                    ->prepend('.')
                    ->prepend(now()->format('dmy_his'))
                    ->prepend('_');

                if ($record !== null) {
                    $result = $result->prepend($record->getKey());
                } else {
                    $result = $result->prepend('toFutureReplace');
                }

                $result = $result->prepend($this->filename !== '' ? $this->filename . '-' : '');

                return $result->toString();
            }
        );

        if ($this->fileTypeAllow !== FileType::Any) {
            if ($this->fileTypeAllow === FileType::Images) {
                $result->image();
            } else {
                $result->acceptedFileTypes($this->fileTypeAllow->parseMimeTypes());
            }
        }

        if ($this->getMinSize() !== 0) {
            $result->minFiles($this->getMinSize());
        }

        if ($this->getMaxSize() !== -1) {
            $result->maxFiles($this->getMaxSize());
        }

        if ($this->getSize() !== -1) {
            $result->maxSize($this->getSize() * 1024);
        }

        return $result;
    }

    public function getFilenameBase(): string
    {
        return $this->filename;
    }

    public function getFileTypeAllow(): FileType
    {
        return $this->fileTypeAllow;
    }

    public function getFormOptions(): array
    {
        $result = $this->getSizesFormOptions('file_size.hint', 'file_size_min.hint', 'file_size_max.hint', true);

        $result[] = Group::make()
            ->columns(2)
            ->schema([
                TextInput::make('filename')
                    ->label(__('file_name'))
                    ->maxLength(255),
                Select::make('file_type')
                    ->label(__('type'))
                    ->options(function (): array {
                        $options = FileType::cases();
                        $flattenOptions = [];

                        foreach ($options as $option) {
                            $flattenOptions[$option->name] = $option->getLabel();
                        }

                        return $flattenOptions;
                    })
                    ->default(FileType::Any->name)
                    ->required()
                    ->selectablePlaceholder(false),
            ]);

        return $result;
    }

    public function getType(): FieldType
    {
        return FieldType::File;
    }

    public function handleRequestOnFieldCreateSave(array $data): array
    {
        $data['size'] = (int) $data['size'];
        $data['min_size'] = (int) $data['min_size'];
        $data['max_size'] = (int) $data['max_size'];

        $data['size'] = $data['size'] < -1 ? -1 : $data['size'];
        $data['min_size'] = $data['min_size'] < 0 ? 0 : $data['min_size'];
        $data['max_size'] = $data['max_size'] < -1 ? -1 : $data['max_size'];

        if ($data['min_size'] === 0) {
            $data['min_size'] = 1;
        }

        if ($data['max_size'] === -1) {
            $data['max_size'] = 1;
        }

        $data = parent::handleSizeRequest($data);

        $filename = (string) $data['filename'];
        unset($data['filename']);

        $this->filename = $filename;
        $this->fileTypeAllow = FileType::tryFromName($data['file_type']);
        unset($data['file_type']);

        return $data;
    }

    public function handleRequestOnFieldFill(array $data): array
    {
        $data['filename'] = $this->filename;
        $data['file_type'] = $this->fileTypeAllow->name;

        $data['size'] = $this->getSize();
        $data['min_size'] = $this->getMinSize();
        $data['max_size'] = $this->getMaxSize();

        return $data;
    }

    public function serialize(): string
    {
        $data = $this->prepareDefaultData();

        // 4to index
        $data[] = base64_encode($this->filename);
        // 5to index
        $data[] = base64_encode($this->fileTypeAllow->name);

        return base64_encode(implode('|', $data));
    }

    public function setMaxSize(int $newMaxSize): static
    {
        $this->maxSize = $newMaxSize;

        return $this;
    }

    public function setMinSize(int $newMinSize): static
    {
        $this->minSize = $newMinSize;

        return $this;
    }

    public function setSize(int $newSize): static
    {
        $this->size = $newSize;

        return $this;
    }
}
