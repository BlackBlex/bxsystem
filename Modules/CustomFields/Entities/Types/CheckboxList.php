<?php

declare(strict_types=1);

namespace Modules\CustomFields\Entities\Types;

use Filament\Forms\Components\Actions\Action;
use Filament\Forms\Components\CheckboxList as FilamentCheckboxList;
use Filament\Forms\Components\Group;
use Filament\Forms\Components\KeyValue;
use Filament\Forms\Components\Select;
use Filament\Forms\Get;
use Modules\CustomFields\Entities\Field;
use Modules\CustomFields\Enums\FieldRule;
use Modules\CustomFields\Enums\FieldType;

class CheckboxList extends Base
{
    /**
     * Get rules that only correspond to the type
     *
     * @return array<FieldRule>
     */
    public static function getAvailableRules(): array
    {
        return [];
    }

    public function getFilamentComponent(string $identificator, Field $fieldInstance): FilamentCheckboxList
    {
        $result = FilamentCheckboxList::make($identificator);

        if ($fieldInstance->options !== null) {
            $result->options($fieldInstance->options);
        }

        return $result;
    }

    public function getFormOptions(): array
    {
        $result = [];

        $result[] = Group::make()
            ->columns(3)
            ->schema([
                Select::make('default_value_checkbox')
                    ->multiple()
                    ->columnSpan(1)
                    ->label(__('default_value'))
                    ->options(fn (Get $get): array => $get('options'))
                    ->searchable(),
                KeyValue::make('options')
                    ->columnSpan(2)
                    ->label(__('options'))
                    ->addActionLabel(__('options.add'))
                    ->addAction(
                        fn (Action $action) => $action
                            ->button(),
                    )
                    ->keyPlaceholder(__('value.internal'))
                    ->reorderable()
                    ->required()
                    ->valuePlaceholder(__('value.to.show')),
            ]);

        return $result;
    }

    public function getType(): FieldType
    {
        return FieldType::CheckboxList;
    }

    public function handleRequestOnFieldCreateSave(array $data): array
    {
        $data['default_value'] = $data['default_value_checkbox'];
        unset($data['default_value_checkbox']);

        return $data;
    }

    public function handleRequestOnFieldFill(array $data): array
    {
        $data['default_value_checkbox'] = $data['default_value'];

        return $data;
    }

    public function setMaxSize(int $newMaxSize): static
    {
        return $this;
    }

    public function setMinSize(int $newMinSize): static
    {
        return $this;
    }

    public function setSize(int $newSize): static
    {
        return $this;
    }
}
