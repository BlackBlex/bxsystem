<?php

declare(strict_types=1);

namespace Modules\CustomFields\Entities\Types;

use Filament\Forms\Components\Checkbox;
use Filament\Forms\Components\DateTimePicker as FilamentDateTimePicker;
use Filament\Forms\Components\Group;
use Filament\Forms\Components\Placeholder;
use Filament\Forms\Components\Radio;
use Filament\Forms\Components\Repeater;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Get;
use Filament\Forms\Set;
use Illuminate\Support\Carbon;
use Modules\CustomFields\Entities\Field;
use Modules\CustomFields\Enums\FieldRule;
use Modules\CustomFields\Enums\FieldType;
use Modules\CustomFields\Helper\CustomFieldHelper;

class DateTimePicker extends Base
{
    protected int $dependsOn = -1;

    protected bool $hasDate = true;

    protected bool $hasTime = true;

    protected bool $setToNow = false;

    /**
     * @var array<array<string, string>>
     */
    protected array $timeForAdd = [];

    /**
     * @var array<int, FilamentDateTimePicker>
     */
    private static array $otherComponents = [];

    /**
     * Get rules that only correspond to the type
     *
     * @return array<FieldRule>
     */
    public static function getAvailableRules(): array
    {
        return [];
    }

    public function deserialize(array $data): bool
    {
        if (parent::deserialize($data)) {
            $this->dependsOn = (int) $data[4];
            $this->hasDate = (bool) $data[5];
            $this->hasTime = (bool) $data[6];
            $this->setToNow = (bool) $data[7];
            $this->timeForAdd = \Safe\json_decode($data[8], true);

            return true;
        }

        return false;
    }

    public function getFilamentComponent(string $identificator, Field $fieldInstance): FilamentDateTimePicker
    {
        $result = FilamentDateTimePicker::make($identificator);

        $result->date($this->hasDate);
        $result->time($this->hasTime);
        $result->seconds(false);

        if ($this->setToNow) {
            $result->afterStateHydrated(fn (FilamentDateTimePicker $component) => $component->state(now()->format($component->getFormat())))
                ->default(now()->format($result->getFormat()));
        }

        if ($this->dependsOn !== -1) {
            if ($this->rule === FieldRule::AfterDate) {
                $result->after(CustomFieldHelper::getCustomFieldKey() . $this->dependsOn);
            } else {
                $result->before(CustomFieldHelper::getCustomFieldKey() . $this->dependsOn);
            }

            if (count($this->timeForAdd) > 0) {
                if (array_key_exists($this->dependsOn, self::$otherComponents)) {
                    $dependDatePicker = self::$otherComponents[$this->dependsOn];

                    $dependDatePicker->afterStateUpdated(function (Set $set, string $state) use ($identificator, $result): void {
                        $date = $this->getTimeForAdd(Carbon::parse($state));

                        $set($identificator, $date->format($result->getFormat()));
                    });

                    $result->afterStateHydrated(function (FilamentDateTimePicker $component, Get $get): void {
                        $date = $this->getTimeForAdd(Carbon::parse($get(CustomFieldHelper::getCustomFieldKey() . $this->dependsOn)));

                        $component->state($date->format($component->getFormat()));

                    });
                }
            }
        } else {
            $result->reactive();

            self::$otherComponents[$fieldInstance->id] = $result;
        }

        return $result;
    }

    public function getFormOptions(): array
    {
        $result = [];

        $result[] = Group::make()
            ->columns(3)
            ->schema([
                Group::make()
                    ->columns(1)
                    ->schema([
                        Checkbox::make('has_date')
                            ->reactive()
                            ->label(__('has_date'))
                            ->default($this->hasDate),
                        Checkbox::make('has_time')
                            ->reactive()
                            ->label(__('has_time'))
                            ->default($this->hasTime),
                        Checkbox::make('set_to_now')
                            ->reactive()
                            ->label(__('set_to_now'))
                            ->default($this->setToNow)
                            ->hintIcon('heroicon-m-question-mark-circle', __('set_to_now.hint')),
                        Placeholder::make('---'),
                        Select::make('depends_on')
                            ->options(function (Field $record): array {
                                /** @var \Illuminate\Database\Eloquent\Collection<int, Field> */
                                $fields = Field::where('id', '!=', $record->id)->get();
                                $result = [];
                                /** @var Field $field */
                                foreach ($fields as $field) {
                                    if ($field->type instanceof DateTimePicker) {
                                        $result[$field->id] = $field->title;
                                    }
                                }

                                return $result;
                            }),
                        Radio::make('after_depends')
                            ->hiddenLabel()
                            ->default($this->rule === FieldRule::AfterDate ? 1 : 0)
                            ->options([
                                1 => __('after_of'),
                                0 => __('before_of'),
                            ]),
                    ]),
                Repeater::make('times_for_add')
                    ->addActionLabel(__('filament-forms::components.builder.actions.add_between.label') . ' ' . strtolower(__('time')))
                    ->columnSpan(2)
                    ->columns(2)
                    ->defaultItems(0)
                    ->label(__('add_time_to_datetime'))
                    ->reorderable(false)
                    ->schema([
                        TextInput::make('amount')
                            ->numeric()
                            ->default(0)
                            ->minValue(0)
                            ->inputMode('decimal'),
                        Select::make('kind')
                            ->disableOptionWhen(function (string $value, Get $get): bool {
                                if (in_array($value, ['s', 'mi', 'h'], true)) {
                                    return ! (bool) $get('../../has_time');
                                } elseif (in_array($value, ['d', 'w', 'mo', 'y'], true)) {
                                    return ! (bool) $get('../../has_date');
                                }

                                return false;
                            })
                            ->in(fn (Select $component): array => array_keys($component->getEnabledOptions()))
                            ->options([
                                's' => __('seconds'),
                                'mi' => __('minutes'),
                                'h' => __('hours'),
                                'd' => __('days'),
                                'w' => __('weeks'),
                                'mo' => __('months'),
                                'y' => __('years'),
                            ])
                            ->selectablePlaceholder(false)
                            ->required(),
                    ]),
            ]);

        return $result;
    }

    public function getType(): FieldType
    {
        return FieldType::DateTimePicker;
    }

    public function handleRequestOnFieldCreateSave(array $data): array
    {
        $this->hasDate = $data['has_date'];
        $this->hasTime = $data['has_time'];
        $this->setToNow = $data['set_to_now'];
        $this->timeForAdd = $data['times_for_add'];
        $this->dependsOn = $data['depends_on'] !== null ? (int) $data['depends_on'] : -1;
        $this->rule = $data['after_depends'] === 0 ? FieldRule::BeforeDate : FieldRule::AfterDate;

        unset($data['has_date']);
        unset($data['has_time']);
        unset($data['set_to_now']);
        unset($data['times_for_add']);
        unset($data['depends_on']);
        unset($data['after_depends']);

        return $data;
    }

    public function handleRequestOnFieldFill(array $data): array
    {
        $data['has_date'] = $this->hasDate;
        $data['has_time'] = $this->hasTime;
        $data['set_to_now'] = $this->setToNow;
        $data['times_for_add'] = $this->timeForAdd;
        $data['depends_on'] = $this->dependsOn;
        $data['after_depends'] = $this->rule === FieldRule::AfterDate ? 1 : 0;

        return $data;
    }

    public function serialize(): string
    {
        $data = $this->prepareDefaultData();

        // 4to index
        $data[] = base64_encode((string) $this->dependsOn);
        // 5to index
        $data[] = base64_encode((string) $this->hasDate);
        // 6to index
        $data[] = base64_encode((string) $this->hasTime);
        // 7to index
        $data[] = base64_encode((string) $this->setToNow);
        // 8to index
        $data[] = base64_encode(\Safe\json_encode($this->timeForAdd));

        return base64_encode(implode('|', $data));
    }

    public function setMaxSize(int $newMaxSize): static
    {
        return $this;
    }

    public function setMinSize(int $newMinSize): static
    {
        return $this;
    }

    public function setSize(int $newSize): static
    {
        return $this;
    }

    private function getTimeForAdd(Carbon $date): Carbon
    {
        foreach ($this->timeForAdd as $time) {
            $amount = (int) $time['amount'];
            $kind = $time['kind'];

            if ($kind === 's') {
                $date->addSeconds($amount);
            }
            if ($kind === 'mi') {
                $date->addMinutes($amount);
            }
            if ($kind === 'h') {
                $date->addHours($amount);
            }
            if ($kind === 'd') {
                $date->addDays($amount);
            }
            if ($kind === 'w') {
                $date->addWeeks($amount);
            }
            if ($kind === 'mo') {
                $date->addMonths($amount);
            }
            if ($kind === 'y') {
                $date->addYears($amount);
            }
        }

        return $date;
    }
}
