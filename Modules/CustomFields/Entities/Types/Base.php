<?php

declare(strict_types=1);

namespace Modules\CustomFields\Entities\Types;

use Filament\Forms\Components\Component;
use Filament\Forms\Components\Group;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Get;
use Illuminate\Support\HtmlString;
use Modules\CustomFields\Contracts\FieldStructure;
use Modules\CustomFields\Enums\FieldRule;

abstract class Base implements FieldStructure
{
    protected int $maxSize = -1;

    protected int $minSize = 0;

    protected FieldRule $rule = FieldRule::Any;

    protected int $size = -1;

    public function deserialize(array $data): bool
    {
        $rule = FieldRule::tryFrom($data[0]);
        $size = (int) $data[1];
        $minSize = (int) $data[2];
        $maxSize = (int) $data[3];

        if ($rule !== null) {
            $this->setRule($rule);

            if ($maxSize !== -1) {
                $this->setMaxSize($maxSize);
            }
            if ($minSize !== 0) {
                $this->setMinSize($minSize);
            }
            if ($size !== -1) {
                $this->setSize($size);
            }

            return true;
        }

        return false;
    }

    final public function getMaxSize(): int
    {
        return $this->maxSize;
    }

    final public function getMinSize(): int
    {
        return $this->minSize;
    }

    final public function getRule(): FieldRule
    {
        return $this->rule;
    }

    final public function getSize(): int
    {
        return $this->size;
    }

    public function serialize(): string
    {
        $data = $this->prepareDefaultData();

        return base64_encode(implode('|', $data));
    }

    final public function setRule(FieldRule $newRule): static
    {
        $this->rule = $newRule;

        return $this;
    }

    /**
     * Get size options for this field
     *
     * @return array<Component>
     */
    final protected function getSizesFormOptions(string $keySizeHint = 'text_size.hint', string $keySizeMinHint = 'text_size_min.hint', string $keySizeMaxHint = 'text_size_max.hint', bool $isFile = false): array
    {
        $result = [
            Group::make()
                ->columns(3)
                ->schema([
                    TextInput::make('size')
                        ->label(__('size'))
                        ->hint(new HtmlString(__($keySizeHint)))
                        ->numeric()
                        ->default(-1)
                        ->minValue(-1),
                    TextInput::make('min_size')
                        ->label(__('size_min'))
                        ->hint(new HtmlString(__($keySizeMinHint)))
                        ->numeric()
                        ->default(0)
                        ->minValue(0)
                        ->maxValue(function (Get $get) use ($isFile) {
                            if (! $isFile) {
                                $size = (int) $get('size');

                                if ($size != -1) {
                                    return $size;
                                }
                            }

                            return null;
                        }),
                    TextInput::make('max_size')
                        ->label(__('size_max'))
                        ->hint(new HtmlString(__($keySizeMaxHint)))
                        ->numeric()
                        ->default(-1)
                        ->minValue(function (Get $get) use ($isFile) {
                            if (! $isFile) {
                                return (int) $get('size');
                            }

                            return -1;
                        }),
                ]),
        ];

        return $result;
    }

    /**
     * Get the data from the array and set in the current instance
     *
     * @param  array<mixed>  $data
     * @return array<mixed>
     */
    final protected function handleSizeRequest(array $data): array
    {
        if ($data['size'] !== -1) {
            $this->setSize($data['size']);
            unset($data['size']);
        }

        if ($data['min_size'] !== 0) {
            $this->setMinSize($data['min_size']);
            unset($data['min_size']);
        }

        if ($data['max_size'] !== -1) {
            $this->setMaxSize($data['max_size']);
            unset($data['max_size']);
        }

        return $data;
    }

    /**
     * Get processed data from default attributes
     *
     * @return array<string>
     */
    final protected function prepareDefaultData(): array
    {
        $data = [
            base64_encode($this->getType()->value),
            base64_encode($this->getRule()->value),
            base64_encode((string) $this->getSize()),
            base64_encode((string) $this->getMinSize()),
            base64_encode((string) $this->getMaxSize()),
        ];

        return $data;
    }
}
