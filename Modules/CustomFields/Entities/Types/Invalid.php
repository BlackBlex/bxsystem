<?php

declare(strict_types=1);

namespace Modules\CustomFields\Entities\Types;

use Filament\Forms\Components\Field;
use Modules\CustomFields\Entities\Field as EntitiesField;
use Modules\CustomFields\Enums\FieldRule;
use Modules\CustomFields\Enums\FieldType;

class Invalid extends Base
{
    /**
     * Get rules that only correspond to the type
     *
     * @return array<FieldRule>
     */
    public static function getAvailableRules(): array
    {
        return [];
    }

    public function getFilamentComponent(string $identificator, EntitiesField $fieldInstance): Field
    {
        return Field::make('INVALID');
    }

    public function getFormOptions(): array
    {
        return [];
    }

    public function getType(): FieldType
    {
        return FieldType::Invalid;
    }

    public function handleRequestOnFieldCreateSave(array $data): array
    {
        return $data;
    }

    public function handleRequestOnFieldFill(array $data): array
    {
        return $data;
    }

    public function setMaxSize(int $newMaxSize): static
    {
        return $this;
    }

    public function setMinSize(int $newMinSize): static
    {
        return $this;
    }

    public function setSize(int $newSize): static
    {
        return $this;
    }
}
