<?php

declare(strict_types=1);

namespace Modules\CustomFields\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\CustomFields\Database\Factories\FieldsetFactory;
use Modules\CustomFields\Entities\Pivot\FieldFieldset;
use Modules\CustomFields\Entities\Pivot\FieldsetModel;

class Fieldset extends Model
{
    use HasFactory;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<string>
     */
    protected $fillable = ['name'];

    public function fieldFieldsets(): HasMany
    {
        return $this->hasMany(FieldFieldset::class);
    }

    /**
     * Get all of the models for the Fieldset
     */
    public function models(): HasMany
    {
        return $this->hasMany(FieldsetModel::class, 'fieldset_id');
    }

    protected static function newFactory(): FieldsetFactory
    {
        return FieldsetFactory::new();
    }
}
