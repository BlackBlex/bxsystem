<?php

declare(strict_types=1);

namespace Modules\CustomFields\Traits;

use Illuminate\Database\Eloquent\Relations\MorphMany;
use Modules\CustomFields\Entities\Pivot\FieldResponse;

trait HasCustomFields
{
    public function responses(): MorphMany
    {
        return $this->morphMany(FieldResponse::class, 'model');
    }

    /**
     * Get a flat array with the values of responses with field_id as key
     *
     * @return array<int, mixed>
     */
    public function responses_flat(): array
    {
        $this->load('responses');

        return $this->responses->pluck('value', 'field_id')->toArray();
    }
}
