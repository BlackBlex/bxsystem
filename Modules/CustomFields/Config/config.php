<?php

declare(strict_types=1);

return [
    'name' => 'CustomFields',
    'panel_title' => [
        'admin' => 'custom_fields',
    ],
];
