<?php

declare(strict_types=1);

namespace Modules\CustomFields\Filament\Admin\Resources;

use Filament\Forms;
use Filament\Forms\Components\Actions\Action;
use Filament\Forms\Components\Repeater;
use Filament\Forms\Components\Tabs;
use Filament\Forms\Form;
use Filament\Forms\Get;
use Filament\Forms\Set;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use Modules\CustomFields\Entities\Field;
use Modules\CustomFields\Entities\Fieldset;
use Modules\CustomFields\Entities\Pivot\FieldFieldset;
use Modules\CustomFields\Entities\Pivot\FieldsetModel;
use Modules\CustomFields\Filament\Admin\Resources\FieldsetResource\Pages;
use Modules\CustomFields\Helper\FindModelHelper;
use Modules\CustomFields\Traits\HasCustomFields;

class FieldsetResource extends Resource
{
    protected static ?string $model = Fieldset::class;

    protected static ?string $navigationIcon = 'heroicon-o-rectangle-stack';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Tabs::make('')
                    ->tabs([
                        Tabs\Tab::make(__('basic.information'))
                            ->schema([
                                Forms\Components\TextInput::make('name')
                                    ->label(__('name'))
                                    ->required()
                                    ->maxLength(255),
                                Repeater::make('models')
                                    ->addActionLabel(__('filament-actions::associate.single.modal.heading', ['label' => __('model')]))
                                    ->label(__('models'))
                                    ->relationship()
                                    ->simple(
                                        Forms\Components\Select::make('model_type')
                                            ->label(__('type'))
                                            ->options(
                                                /** @return array<string> */
                                                function (Get $get, ?string $state): array {
                                                    /** @var array<string, array<string, mixed>> $items */
                                                    $items = $get('../../models');
                                                    /** @var \Illuminate\Support\Collection<int, string> $usedModels */
                                                    $usedModels = collect($items)->pluck('model_type')->diff([$state]);

                                                    return FindModelHelper::allFrom([app_path(), base_path('Modules')], [base_path(), base_path('Modules')], ['', 'Modules'], HasCustomFields::class)->diff($usedModels)->mapWithKeys(fn ($item) => [$item => $item])->toArray();
                                                })
                                    )
                                    ->deleteAction(
                                        fn (Action $action) => $action->action(function (array $arguments, Repeater $component): void {
                                            $items = $component->getState();
                                            $data = $items[$arguments['item']];
                                            unset($items[$arguments['item']]);
                                            $component->state($items);

                                            $fieldsetModel = FieldsetModel::whereId($data['id'])->whereModelType($data['model_type'])->first();

                                            if ($fieldsetModel !== null) {
                                                $fieldsetModel->delete();
                                            }
                                        }),
                                    )
                                    ->saveRelationshipsUsing(function (array $state, Fieldset $record): void {
                                        foreach ($state as $stateKey => $stateValue) {
                                            if ($stateValue['model_type'] === null) {
                                                continue;
                                            }

                                            if (str_starts_with($stateKey, 'record')) {
                                                $fieldsetModel = FieldsetModel::whereId($stateValue['id'])->first();

                                                if ($fieldsetModel !== null) {
                                                    $fieldsetModel->model_type = $stateValue['model_type'];
                                                    $fieldsetModel->save();
                                                }
                                            } else {
                                                $fieldsetModel = FieldsetModel::whereFieldsetId($record->id)->whereModelType($stateValue['model_type'])->onlyTrashed()->first();

                                                if ($fieldsetModel !== null) {
                                                    $fieldsetModel->restore();
                                                } else {
                                                    $fieldsetModel = new FieldsetModel();
                                                    $fieldsetModel->fill(['model_type' => $stateValue['model_type']]);
                                                    $fieldsetModel->fieldset()->associate($record);
                                                    $record->models()->save($fieldsetModel);
                                                }
                                            }
                                        }
                                    }),
                            ]),
                        Tabs\Tab::make(__('fields'))
                            ->schema([
                                Repeater::make('fieldFieldsets')
                                    ->addActionLabel(__('filament-actions::associate.single.modal.heading', ['label' => __('field')]))
                                    ->hiddenLabel()
                                    ->relationship()
                                    ->defaultItems(0)
                                    ->reorderable(true)
                                    ->orderColumn('order')
                                    ->afterStateUpdated(function (array $state, Set $set): void {
                                        if (count($state) === 0) {
                                            return;
                                        }

                                        $order = 1;
                                        foreach ($state as $key => $_value) {
                                            $set('fieldFieldsets.' . $key . '.order', $order);
                                            $order += 1;
                                        }
                                    })
                                    ->schema([
                                        Forms\Components\TextInput::make('order')
                                            ->label(__('order'))
                                            ->numeric()
                                            ->readOnly(),
                                        Forms\Components\Select::make('field_id')
                                            ->label(__('field'))
                                            ->options(function (Get $get, ?string $state): array {
                                                /** @var array<string, array<string, mixed>> $items */
                                                $items = $get('../../fields');
                                                /** @var \Illuminate\Support\Collection<int, string> $usedFields */
                                                $usedFields = collect($items)->pluck('field_id')->diff([$state]);

                                                return Field::query()->whereNotIn('id', $usedFields)->pluck('title', 'id')->toArray();
                                            })
                                            ->searchable(),
                                        Forms\Components\Fieldset::make('')
                                            ->schema([
                                                Forms\Components\Toggle::make('required')->label(__('required')),
                                                Forms\Components\Toggle::make('searchable')->label(__('searchable')),
                                                Forms\Components\Toggle::make('show_in_table')->label(__('show_in_table'))->columnSpanFull(),
                                            ])
                                            ->columns(3)
                                            ->columnSpan(2),
                                    ])
                                    ->deleteAction(
                                        fn (Action $action) => $action->action(function (array $arguments, Repeater $component): void {
                                            $items = $component->getState();
                                            $data = $items[$arguments['item']];
                                            unset($items[$arguments['item']]);
                                            $component->state($items);

                                            $fieldFieldset = FieldFieldset::whereFieldId($data['field_id'])->whereFieldsetId($data['fieldset_id'])->first();
                                            $fieldFieldset->delete();
                                        }),
                                    )
                                    ->saveRelationshipsUsing(function (array $state, Fieldset $record): void {
                                        foreach ($state as $stateKey => $stateValue) {
                                            if ($stateValue['field_id'] === null) {
                                                continue;
                                            }

                                            if (str_starts_with($stateKey, 'record')) {
                                                $fieldFieldset = FieldFieldset::whereId($stateValue['id'])->first();

                                                if ($fieldFieldset !== null) {
                                                    $fieldFieldset->field_id = $stateValue['field_id'];
                                                    $fieldFieldset->order = $stateValue['order'];
                                                    $fieldFieldset->required = $stateValue['required'];
                                                    $fieldFieldset->searchable = $stateValue['searchable'];
                                                    $fieldFieldset->show_in_table = $stateValue['show_in_table'];
                                                    $fieldFieldset->save();
                                                }
                                            } else {
                                                $fieldFieldset = FieldFieldset::whereFieldsetId($record->id)->whereFieldId($stateValue['field_id'])->onlyTrashed()->first();

                                                if ($fieldFieldset !== null) {
                                                    $fieldFieldset->restore();
                                                } else {
                                                    $fieldFieldset = new FieldFieldset();

                                                    $fieldFieldset->fill([
                                                        'order' => $stateValue['order'],
                                                        'required' => $stateValue['required'],
                                                        'searchable' => $stateValue['searchable'],
                                                        'show_in_table' => $stateValue['show_in_table'],
                                                    ]);

                                                    $fieldFieldset->field()->associate((int) $stateValue['field_id']);
                                                    $fieldFieldset->fieldset()->associate($record);

                                                    $record->fieldFieldsets()->save($fieldFieldset);
                                                }
                                            }
                                        }
                                    })
                                    ->columns(4),
                            ]),
                    ])
                    ->columnSpanFull()
                    ->persistTabInQueryString(),
            ]);
    }

    public static function getEloquentQuery(): Builder
    {
        return parent::getEloquentQuery()
            ->withoutGlobalScopes([
                SoftDeletingScope::class,
            ]);
    }

    public static function getModelLabel(): string
    {
        return __('fieldset');
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListFieldsets::route('/'),
            'create' => Pages\CreateFieldset::route('/create'),
            'view' => Pages\ViewFieldset::route('/{record}'),
            'edit' => Pages\EditFieldset::route('/{record}/edit'),
        ];
    }

    public static function getPluralModelLabel(): string
    {
        return __('fieldset');
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('name')
                    ->label(__('name'))
                    ->searchable(),
                Tables\Columns\TextColumn::make('fieldfieldsets.field.title')
                    ->label(__('fields'))
                    ->searchable(),
                Tables\Columns\TextColumn::make('models.model_type')
                    ->label(__('models'))
                    ->formatStateUsing(function (string $state) {
                        $result = [];
                        $models = explode(', ', $state);
                        foreach ($models as $key) {
                            $pieces = \Safe\preg_split('/\\\\/', $key);
                            $result[] = end($pieces);
                        }

                        return implode(', ', $result);
                    })
                    ->searchable(),
                Tables\Columns\TextColumn::make('created_at')
                    ->label(__('created_at'))
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('updated_at')
                    ->label(__('updated_at'))
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
            ])
            ->filters([
                Tables\Filters\TrashedFilter::make(),
            ])
            ->actions([
                Tables\Actions\ViewAction::make(),
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                    Tables\Actions\ForceDeleteBulkAction::make(),
                    Tables\Actions\RestoreBulkAction::make(),
                ]),
            ]);
    }
}
