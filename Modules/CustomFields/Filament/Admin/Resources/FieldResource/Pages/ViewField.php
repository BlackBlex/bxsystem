<?php

declare(strict_types=1);

namespace Modules\CustomFields\Filament\Admin\Resources\FieldResource\Pages;

use Filament\Actions;
use Filament\Resources\Pages\ViewRecord;
use Modules\CustomFields\Filament\Admin\Resources\FieldResource;

class ViewField extends ViewRecord
{
    protected static string $resource = FieldResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\EditAction::make(),
        ];
    }

    protected function mutateFormDataBeforeFill(array $data): array
    {
        $data = $data['type']->handleRequestOnFieldFill($data);
        $data['type'] = $data['type']->getType();

        return $data;
    }
}
