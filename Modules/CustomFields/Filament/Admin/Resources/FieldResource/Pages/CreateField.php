<?php

declare(strict_types=1);

namespace Modules\CustomFields\Filament\Admin\Resources\FieldResource\Pages;

use Filament\Resources\Pages\CreateRecord;
use Modules\CustomFields\Contracts\FieldStructure;
use Modules\CustomFields\Enums\FieldRule;
use Modules\CustomFields\Filament\Admin\Resources\FieldResource;

class CreateField extends CreateRecord
{
    protected static bool $canCreateAnother = false;

    protected static string $resource = FieldResource::class;

    protected function beforeValidate(): void
    {
        if ($this->data !== null && array_key_exists('rule', $this->data) && $this->data['rule'] === FieldRule::Telephone) {
            $this->data['default_value_text'] = str_replace([' ', '-'], '', $this->data['default_value_text'] ?? '');
        }
    }

    protected function mutateFormDataBeforeCreate(array $data): array
    {
        /** @var class-string<FieldStructure> $classNameFQN */
        $classNameFQN = '\\Modules\\CustomFields\\Entities\\Types\\' . ucfirst($data['type']->value);
        $reflectionClass = new \ReflectionClass($classNameFQN);
        /** @var FieldStructure $instance */
        $instance = $reflectionClass->newInstance();

        $data['type'] = $instance;

        $data = $instance->handleRequestOnFieldCreateSave($data);

        return $data;
    }
}
