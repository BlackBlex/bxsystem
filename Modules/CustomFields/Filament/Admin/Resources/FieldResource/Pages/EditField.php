<?php

declare(strict_types=1);

namespace Modules\CustomFields\Filament\Admin\Resources\FieldResource\Pages;

use Filament\Actions;
use Filament\Resources\Pages\EditRecord;
use Modules\CustomFields\Contracts\FieldStructure;
use Modules\CustomFields\Enums\FieldRule;
use Modules\CustomFields\Filament\Admin\Resources\FieldResource;

class EditField extends EditRecord
{
    protected static string $resource = FieldResource::class;

    protected function beforeValidate(): void
    {
        if ($this->data !== null && array_key_exists('rule', $this->data) && $this->data['rule'] === FieldRule::Telephone) {
            $this->data['default_value_text'] = str_replace([' ', '-'], '', $this->data['default_value_text'] ?? '');
        }
    }

    protected function getHeaderActions(): array
    {
        return [
            Actions\ViewAction::make(),
            Actions\DeleteAction::make(),
            Actions\ForceDeleteAction::make(),
            Actions\RestoreAction::make(),
        ];
    }

    protected function mutateFormDataBeforeFill(array $data): array
    {
        $data = $data['type']->handleRequestOnFieldFill($data);
        $data['type'] = $data['type']->getType();

        return $data;
    }

    protected function mutateFormDataBeforeSave(array $data): array
    {
        if (! in_array('format', $data, true)) {
            $data['format'] = null;
        }

        if (array_key_exists('type', $data)) {
            /** @var class-string<FieldStructure> $classNameFQN */
            $classNameFQN = '\\Modules\\CustomFields\\Entities\\Types\\' . ucfirst($data['type']->value);
            $reflectionClass = new \ReflectionClass($classNameFQN);
            /** @var FieldStructure $instance */
            $instance = $reflectionClass->newInstance();

            $data['type'] = $instance;

            $data = $instance->handleRequestOnFieldCreateSave($data);
        }

        return $data;
    }
}
