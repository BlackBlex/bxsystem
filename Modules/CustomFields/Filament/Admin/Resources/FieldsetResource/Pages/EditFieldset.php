<?php

declare(strict_types=1);

namespace Modules\CustomFields\Filament\Admin\Resources\FieldsetResource\Pages;

use Filament\Actions;
use Filament\Resources\Pages\EditRecord;
use Modules\CustomFields\Filament\Admin\Resources\FieldsetResource;

class EditFieldset extends EditRecord
{
    protected static string $resource = FieldsetResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\ViewAction::make(),
            Actions\DeleteAction::make(),
            Actions\ForceDeleteAction::make(),
            Actions\RestoreAction::make(),
        ];
    }
}
