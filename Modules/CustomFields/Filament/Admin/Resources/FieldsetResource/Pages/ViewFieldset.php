<?php

declare(strict_types=1);

namespace Modules\CustomFields\Filament\Admin\Resources\FieldsetResource\Pages;

use Filament\Actions;
use Filament\Resources\Pages\ViewRecord;
use Modules\CustomFields\Filament\Admin\Resources\FieldsetResource;

class ViewFieldset extends ViewRecord
{
    protected static string $resource = FieldsetResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\EditAction::make(),
        ];
    }
}
