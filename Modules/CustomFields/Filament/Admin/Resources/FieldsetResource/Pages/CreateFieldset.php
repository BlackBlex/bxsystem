<?php

declare(strict_types=1);

namespace Modules\CustomFields\Filament\Admin\Resources\FieldsetResource\Pages;

use Filament\Resources\Pages\CreateRecord;
use Modules\CustomFields\Filament\Admin\Resources\FieldsetResource;

class CreateFieldset extends CreateRecord
{
    protected static string $resource = FieldsetResource::class;
}
