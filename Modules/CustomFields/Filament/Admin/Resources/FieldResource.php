<?php

declare(strict_types=1);

namespace Modules\CustomFields\Filament\Admin\Resources;

use Filament\Forms\Components\CheckboxList;
use Filament\Forms\Components\Component;
use Filament\Forms\Components\Group;
use Filament\Forms\Components\Section;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Form;
use Filament\Forms\Get;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use Modules\CustomFields\Contracts\FieldStructure;
use Modules\CustomFields\Entities\Field;
use Modules\CustomFields\Entities\Pivot\FieldFieldset;
use Modules\CustomFields\Entities\Types\Base;
use Modules\CustomFields\Entities\Types\File;
use Modules\CustomFields\Enums\FieldRule;
use Modules\CustomFields\Enums\FieldType;
use Modules\CustomFields\Filament\Admin\Resources\FieldResource\Pages;
use Modules\CustomFields\Helper\FindModelHelper;

class FieldResource extends Resource
{
    protected static ?string $model = Field::class;

    protected static ?string $navigationIcon = 'heroicon-o-rectangle-stack';

    /**
     * @var array<Component>
     */
    private static array $fieldsOptions = [];

    public static function form(Form $form): Form
    {
        if (count(self::$fieldsOptions) === 0) {
            self::loadFieldsOptions();
        }

        return $form
            ->columns(4)
            ->schema([
                Group::make()
                    ->columnSpan(3)
                    ->schema([
                        Section::make(__('field.information'))
                            ->icon('heroicon-o-bars-3-bottom-left')
                            ->columns(2)
                            ->schema([
                                Select::make('type')
                                    ->label(__('type'))
                                    ->options(FieldType::class)->default(FieldType::Text)
                                    ->reactive()
                                    ->required()
                                    ->selectablePlaceholder(false),
                                TextInput::make('title')
                                    ->label(__('title'))
                                    ->required()
                                    ->maxLength(255),
                                TextInput::make('hint')
                                    ->columnSpanFull()
                                    ->label(__('hint'))
                                    ->maxLength(255),
                            ]),
                        Section::make(__('field.options'))
                            ->icon('heroicon-o-pencil-square')
                            ->schema(fn () => self::$fieldsOptions),
                    ]),
                Group::make()
                    ->columnSpan(1)
                    ->schema([
                        Section::make(__('fieldset'))
                            ->icon('forkawesome-object-group')
                            ->schema([
                                CheckboxList::make('fieldsets')
                                    ->bulkToggleable()
                                    ->hiddenLabel()
                                    ->relationship('fieldsets', 'name')
                                    ->saveRelationshipsUsing(function (Field $record, array $state): void {
                                        // bulkToggleable duplicate some items when you use select all
                                        $state = array_unique($state, SORT_NUMERIC);
                                        $state = array_map('intval', $state);

                                        $normalFieldFieldset = FieldFieldset::whereFieldId($record->id)->get();
                                        $deletedFieldFieldset = FieldFieldset::onlyTrashed()->whereFieldId($record->id)->get();

                                        foreach ($normalFieldFieldset as $fieldFieldset) {
                                            if (in_array($fieldFieldset->fieldset_id, $state, true)) {
                                                $state = array_filter($state, fn ($m) => $m != $fieldFieldset->fieldset_id);

                                                continue;
                                            }

                                            $fieldFieldset->delete();
                                        }

                                        foreach ($deletedFieldFieldset as $fieldFieldset) {
                                            if (! in_array($fieldFieldset->fieldset_id, $state, true)) {
                                                continue;
                                            }

                                            $fieldFieldset->restore();
                                            $state = array_filter($state, fn ($m) => $m != $fieldFieldset->fieldset_id);
                                        }

                                        $states = [];

                                        foreach ($state as $individualState) {
                                            $lastField = FieldFieldset::whereFieldsetId($individualState)->count() + 1;

                                            $states[$individualState] = ['order' => $lastField];
                                        }

                                        $record->fieldsets()->syncWithoutDetaching($states);
                                    }),
                            ]),
                    ]),
            ]);
    }

    public static function getEloquentQuery(): Builder
    {
        return parent::getEloquentQuery()
            ->withoutGlobalScopes([
                SoftDeletingScope::class,
            ]);
    }

    public static function getModelLabel(): string
    {
        return __('field');
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListFields::route('/'),
            'create' => Pages\CreateField::route('/create'),
            'view' => Pages\ViewField::route('/{record}'),
            'edit' => Pages\EditField::route('/{record}/edit'),
        ];
    }

    public static function getPluralModelLabel(): string
    {
        return __('fields');
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('title')
                    ->label(__('title'))
                    ->searchable()
                    ->wrap(),
                Tables\Columns\TextColumn::make('type')
                    ->label(__('type'))
                    ->formatStateUsing(function (Field $record) {
                        return $record->type->getType()->name;
                    })
                    ->searchable(),
                Tables\Columns\TextColumn::make('rule')
                    ->label(__('rule'))
                    ->formatStateUsing(function (Field $record) {
                        if ($record->type instanceof File) {
                            return $record->type->getFileTypeAllow()->getLabel();
                        } else {
                            return $record->type->getRule()->getLabel();
                        }
                    })
                    ->searchable(),
                Tables\Columns\TextColumn::make('hint')
                    ->label(__('hint'))
                    ->searchable()
                    ->wrap(),
                Tables\Columns\TextColumn::make('default_value')
                    ->label(__('default_value'))
                    ->formatStateUsing(function (Field $record) {
                        if ($record->type->getType() === FieldType::Toggle) {
                            /** @var bool $boolResult */
                            $boolResult = $record->default_value;
                            $result = __($boolResult === true ? 'filament-forms::components.select.boolean.true' : 'filament-forms::components.select.boolean.false');
                        } elseif ($record->type->getType() === FieldType::Select) {
                            $toSearch = $record->default_value ?? '';
                            $options = $record->options ?? [];

                            if ($toSearch !== '' && array_key_exists($toSearch, $options)) {
                                $result = $options[$toSearch];
                            } else {
                                $result = '';
                            }
                        } elseif ($record->type->getType() === FieldType::CheckboxList) {
                            $result = '';

                            /** @var array<string> $selectedOptions */
                            $selectedOptions = $record->default_value;
                            $viewOptions = [];
                            $options = $record->options ?? [];

                            foreach ($selectedOptions as $selectedOption) {
                                $viewOptions[] = $options[$selectedOption];
                            }

                            $result = implode(', ', $viewOptions);
                        } else {
                            $result = $record->default_value;
                        }

                        if ($record->type->getType() === FieldType::Text && $record->type->getRule() === FieldRule::Telephone && $record->default_value !== '') {
                            $telephoneNumber = $record->default_value ?? '0000000000';
                            $result = substr($telephoneNumber, 0, 3) . ' ' . substr($telephoneNumber, 3, 3) . ' ' . substr($telephoneNumber, 6);
                        }

                        return $result;
                    })
                    ->searchable(),
                Tables\Columns\TextColumn::make('fieldsets.name')
                    ->label(__('fieldset'))
                    ->searchable(),
                Tables\Columns\TextColumn::make('created_at')
                    ->label(__('created_at'))
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('updated_at')
                    ->label(__('updated_at'))
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
            ])
            ->filters([
                Tables\Filters\TrashedFilter::make(),
            ])
            ->actions([
                Tables\Actions\ViewAction::make(),
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                    Tables\Actions\ForceDeleteBulkAction::make(),
                    Tables\Actions\RestoreBulkAction::make(),
                ]),
            ]);
    }

    private static function loadFieldsOptions(): void
    {
        $fieldTypes = FindModelHelper::all(module_path('CustomFields', 'Entities' . DIRECTORY_SEPARATOR . 'Types'), module_path('CustomFields', 'Entities' . DIRECTORY_SEPARATOR . 'Types'), 'Modules\\CustomFields\\Entities\\Types', null, Base::class)->filter(fn (string $type) => ! str_ends_with($type, 'Invalid'));

        /** @var class-string<FieldStructure> $fieldType */
        foreach ($fieldTypes as $fieldType) {
            $reflectionClass = new \ReflectionClass($fieldType);

            /** @var FieldStructure $instance */
            $instance = $reflectionClass->newInstance();
            $options = $instance->getFormOptions();

            if (count($options) > 0) {
                array_push(self::$fieldsOptions, Group::make($options)->visible(fn (Get $get) => $get('type') === $instance->getType()));
            }

        }
    }
}
