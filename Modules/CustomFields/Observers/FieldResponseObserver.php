<?php

declare(strict_types=1);

namespace Modules\CustomFields\Observers;

use Illuminate\Support\Facades\Storage;
use Modules\CustomFields\Entities\Pivot\FieldResponse;
use Modules\CustomFields\Entities\Types\File;

class FieldResponseObserver
{
    /**
     * Handle the FieldResponse "created" event.
     */
    public function created(FieldResponse $fieldResponse): void
    {
        if ($fieldResponse->field->type instanceof File) {
            $name = array_values((array) $fieldResponse->value)[0];
            $key = array_keys((array) $fieldResponse->value)[0];
            $value = $fieldResponse->value;

            if (str_contains($name, 'toFutureReplace')) {
                $newName = str_replace('toFutureReplace', (string) $fieldResponse->model_id, $name);
                Storage::disk('uploads')->move($name, $newName);
                $value[$key] = $newName;
                $fieldResponse->value = $value;

                $fieldResponse->saveQuietly();
            }
        }
    }

    /**
     * Handle the FieldResponse "deleted" event.
     */
    public function deleted(FieldResponse $fieldResponse): void
    {
        //
    }

    // /**
    //  * Handle the FieldResponse "restored" event.
    //  */
    // public function restored(FieldResponse $fieldResponse): void
    // {
    //     //
    // }

    /**
     * Handle the FieldResponse "force deleted" event.
     */
    public function forceDeleted(FieldResponse $fieldResponse): void
    {
        if ($fieldResponse->field->type instanceof File) {
            if (! is_null($fieldResponse->value)) {
                Storage::disk('uploads')->delete($fieldResponse->value);
            }
        }
    }

    /**
     * Handle the FieldResponse "updated" event.
     */
    public function updated(FieldResponse $fieldResponse): void
    {
        if ($fieldResponse->field->type instanceof File) {
            $uploadDisk = Storage::disk('uploads');

            /** @var string $originalName */
            $originalName = $fieldResponse->getOriginal('value');

            if ($fieldResponse->isDirty('value') && $uploadDisk->exists($originalName)) {
                $uploadDisk->delete($originalName);
            }
        }
    }
}
