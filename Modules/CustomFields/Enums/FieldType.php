<?php

declare(strict_types=1);

namespace Modules\CustomFields\Enums;

use Filament\Support\Contracts\HasLabel;

enum FieldType: string implements HasLabel
{
    case CheckboxList = 'checkboxList';
    case ColorPicker = 'colorPicker';
    case DateTimePicker = 'dateTimePicker';
    case File = 'file';
    case Invalid = 'ThisOnlyForCastingIssues';
    case Radio = 'radio';
    case RichEditor = 'richEditor';
    case Select = 'select';
    case Text = 'text';
    case TextArea = 'textArea';
    case Toggle = 'toggle';

    public function getLabel(): string
    {
        return $this->name;
    }
}
