<?php

declare(strict_types=1);

namespace Modules\CustomFields\Enums;

use Filament\Support\Contracts\HasLabel;

enum FileType: string implements HasLabel
{
    case Any = 'any';

    case Images = 'images';

    case MicrosoftExcel = 'application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.openxmlformats-officedocument.spreadsheetml.template,application/vnd.ms-excel.sheet.macroEnabled.12,application/vnd.ms-excel.template.macroEnabled.12,application/vnd.ms-excel.addin.macroEnabled.12,application/vnd.ms-excel.sheet.binary.macroEnabled.12';

    case MicrosoftPowerPoint = 'application/vnd.ms-powerpoint,application/vnd.openxmlformats-officedocument.presentationml.presentation,application/vnd.openxmlformats-officedocument.presentationml.template,application/vnd.openxmlformats-officedocument.presentationml.slideshow,application/vnd.ms-powerpoint.addin.macroEnabled.12,application/vnd.ms-powerpoint.presentation.macroEnabled.12,application/vnd.ms-powerpoint.template.macroEnabled.12,application/vnd.ms-powerpoint.slideshow.macroEnabled.12';

    case MicrosoftWord = 'application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.openxmlformats-officedocument.wordprocessingml.template,application/vnd.ms-word.document.macroEnabled.12,application/vnd.ms-word.template.macroEnabled.12';

    case Pdf = 'application/pdf';

    public static function tryFromName(string $name): FileType
    {
        $constName = self::class . '::' . $name;

        return defined($constName) ? constant($constName) : FileType::Any;
    }

    public function getLabel(): string
    {
        /** @var string $name */
        $name = \Safe\preg_replace('/(?<!^)[A-Z]/', '_$0', $this->name);

        return __('file_type.' . strtolower($name));
    }

    /**
     * Get array of mime types of file type selected
     *
     * @return array<string>
     */
    public function parseMimeTypes(): array
    {
        return explode(',', $this->value);
    }
}
