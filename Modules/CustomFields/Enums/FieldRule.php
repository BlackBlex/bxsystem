<?php

declare(strict_types=1);

namespace Modules\CustomFields\Enums;

use Filament\Support\Contracts\HasLabel;

enum FieldRule: string implements HasLabel
{
    case AfterDate = 'date|after';
    case Alpha = 'alpha:ascii';
    case AlphaDash = 'alpha_dash:ascii';
    case AlphaNumeric = 'alpha_num:ascii';
    case Any = 'any';
    case BeforeDate = 'date|before';
    case Date = 'date';
    case Email = 'email';
    case IP = 'ip';
    case IPv4 = 'ipv4';
    case IPv6 = 'ipv6';
    case MAC = 'mac';
    case Numeric = 'numeric';
    case Password = 'password';
    case Regex = 'regex';
    case Telephone = 'tel';
    case URL = 'url';

    public function getLabel(): string
    {
        $value = $this->value;

        $value = str_replace('|', '_', $value);
        $value = str_replace(':ascii', '', $value);

        return __('rules.' . $value);
    }
}
