<?php

declare(strict_types=1);

namespace Modules\CustomFields\Helper;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use ReflectionClass;
use SplFileInfo;

class FindModelHelper
{
    /**
     * @param  class-string  $implementClass
     * @return Collection<int, string>
     */
    public static function all(
        ?string $directory = null,
        ?string $basePath = null,
        ?string $baseNamespace = null,
        ?string $traitToSearch = null,
        ?string $implementClass = null
    ): Collection {
        $directory ??= app_path();
        $basePath ??= base_path();
        $baseNamespace ??= '';

        if (PHP_OS_FAMILY === 'Windows') {
            $directory = str_replace('/', DIRECTORY_SEPARATOR, $directory);
            $basePath = str_replace('/', DIRECTORY_SEPARATOR, $basePath);
        }

        /** @var Collection<int, string> $allFiles */
        $allFiles = collect(static::getFilesRecursively($directory));

        /** @var Collection<int, ReflectionClass> $reflectionItems */
        $reflectionItems = $allFiles->map(fn (string $class): SplFileInfo => new SplFileInfo($class))
            ->map(fn (SplFileInfo $file): string => self::fullQualifiedClassNameFromFile($file, $basePath, $baseNamespace))
            ->map(fn (string $class) => self::getReflectionClass($class));

        /** @var Collection<int, string> $resultItems */
        $resultItems = $reflectionItems
            // ->filter()
            ->filter(function (ReflectionClass $class) use ($implementClass): bool {
                $result = ! $class->isAbstract();

                if ($result && $implementClass != null) {
                    $result = $class->isSubclassOf($implementClass);
                } else {
                    $result = $class->isSubclassOf(Model::class);
                }

                return $result;
            })
            ->filter(function (ReflectionClass $class) use ($traitToSearch): bool {
                if ($traitToSearch === null) {
                    return true;
                }

                // Investigate: Why pass in normal use and fails in test?
                if (array_search($traitToSearch, $class->getTraitNames(), true) !== false) {
                    return true;
                }

                // Investigate: why fails in normal use and pass in test?
                if (in_array($traitToSearch, \Safe\class_uses($class->getName()), true)) {
                    return true;
                }

                return false;
            })
            ->map(fn (ReflectionClass $reflectionClass) => $reflectionClass->getName())
            ->values();

        return $resultItems;
    }

    /**
     * @param  array<string>  $directory
     * @param  array<string>  $basePath
     * @param  array<string>  $baseNamespace
     * @return Collection<int, string>
     */
    public static function allFrom(
        array $directory = [],
        array $basePath = [],
        array $baseNamespace = [],
        ?string $traitToSearch = null
    ): Collection {
        if (count($directory) !== count($baseNamespace) && count($directory) != count($basePath)) {
            return collect([]);
        }

        /** @var Collection<int, string> $result */
        $result = collect([]);

        for ($i = 0; $i < count($directory); $i++) {
            $result = $result->merge(static::all($directory[$i], $basePath[$i], $baseNamespace[$i], $traitToSearch));
        }

        return $result;
    }

    protected static function fullQualifiedClassNameFromFile(
        SplFileInfo $file,
        string $basePath,
        string $baseNamespace
    ): string {
        return Str::of($file->getRealPath())
            ->replaceFirst($basePath, '')
            ->replaceLast('.php', '')
            ->trim(DIRECTORY_SEPARATOR)
            ->ucfirst()
            ->replace(
                [DIRECTORY_SEPARATOR, 'App\\'],
                ['\\', app()->getNamespace()],
            )
            ->prepend($baseNamespace . '\\')->toString();
    }

    /**
     * Get all files from path recursively
     *
     * @return array<string>
     */
    protected static function getFilesRecursively(string $path): array
    {
        $rii = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path));
        $files = [];

        /** @var SplFileInfo $file */
        foreach ($rii as $file) {
            if ($file->isDir() || (! str_contains($file->getPathname(), 'Entities') && ! str_contains($file->getPathname(), 'Models')) || ! str_contains($file->getExtension(), 'php')) {
                continue;
            }
            $files[] = $file->getPathname();
        }

        return $files;
    }

    /**
     * Get a ReflectionClass object
     */
    private static function getReflectionClass(string $class): ReflectionClass
    {
        /**
         * @psalm-suppress ArgumentTypeCoercion
         */
        return new ReflectionClass($class);
    }
}
