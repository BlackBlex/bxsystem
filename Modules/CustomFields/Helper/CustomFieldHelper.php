<?php

declare(strict_types=1);

namespace Modules\CustomFields\Helper;

use App\Models\User;
use Filament\Forms;
use Filament\Forms\Components\Select;
use Filament\Tables;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\HtmlString;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use Modules\CustomFields\Entities\Field;
use Modules\CustomFields\Entities\Pivot\FieldResponse;
use Modules\CustomFields\Entities\Pivot\FieldsetModel;
use Modules\CustomFields\Entities\Types\Text;
use Modules\CustomFields\Enums\FieldRule;
use Modules\CustomFields\Enums\FieldType;

class CustomFieldHelper
{
    /**
     * Build columns for display data of custom fields
     *
     * @return array<Tables\Columns\Column>
     */
    public static function buildColumns(string $modelType): array
    {
        $columns = [];
        $fieldShowInTables = collect();

        $fieldsetModels = FieldsetModel::whereModelType($modelType)->get();

        foreach ($fieldsetModels as $fieldsetModel) {
            $fieldShowInTables = $fieldShowInTables->union($fieldsetModel->fieldset->fieldFieldsets()->where('show_in_table', 1)->get()->pluck('field'));
        }

        $curItem = 0;
        $maxItem = 1;

        /** @var Field $field */
        foreach ($fieldShowInTables as $field) {
            if ($field->type->getType() === FieldType::Toggle) {
                $column = Tables\Columns\IconColumn::make('responses.' . $curItem)
                    ->boolean();
            } else {
                $column = Tables\Columns\TextColumn::make('responses.' . $curItem . '.value')
                    ->formatStateUsing(function (Model $record) use ($field): string {
                        if (method_exists($record, 'responses_flat')) {
                            $responses = $record->responses_flat();

                            if ($field->type->getType() === FieldType::Toggle) {
                                $result = __($responses[$field->id] === true ? 'filament-forms::components.select.boolean.true' : 'filament-forms::components.select.boolean.false');
                            } elseif ($field->type->getType() === FieldType::Select) {
                                $options = $field->options ?? [];
                                if (count($options) > 0 && array_key_exists($field->id, $responses)) {
                                    $result = $options[$responses[$field->id]];
                                } else {
                                    $result = '';
                                }

                            } elseif ($field->type->getType() === FieldType::CheckboxList) {
                                $result = '';
                                $options = $field->options ?? [];

                                if (count($options) > 0 && array_key_exists($field->id, $responses)) {
                                    $selectedOptions = $responses[$field->id];
                                    $viewOptions = [];

                                    foreach ($selectedOptions as $selectedOption => $value) {
                                        if ($value === 1) {
                                            $viewOptions[] = $options[$selectedOption];
                                        }
                                    }

                                    $result = implode(', ', $viewOptions);
                                }
                            } else {
                                $result = $responses[$field->id];
                            }

                            if ($field->type->getType() === FieldType::Text && $field->type->getRule() === FieldRule::Telephone) {
                                $result = substr($responses[$field->id], 0, 3) . ' ' . substr($responses[$field->id], 3, 3) . ' ' . substr($responses[$field->id], 6);
                            }

                            return (string) $result;
                        }

                        return '';
                    });

                if ($field->type->getRule() === FieldRule::Numeric) {
                    $column = $column->numeric();
                }
            }

            $column->label($field->title);
            $column->searchable(
                query: function (Builder $query, string $search): Builder {
                    return $query->whereHas('responses', function (Builder $query2) use ($search): void {
                        $query2->where('value', 'like', "%{$search}%");
                    });
                }
            );

            if ($curItem > $maxItem) {
                $column->toggleable(isToggledHiddenByDefault: true);
            }

            $columns[] = $column;
            $curItem++;
        }

        return $columns;
    }

    /**
     * Build field for set or view data of custom fields
     *
     * @return array<Forms\Components\Tabs\Tab>
     */
    public static function buildForm(string $modelType, ?int $modelID = null): array
    {
        $schema = [];

        $fieldsetModels = FieldsetModel::whereModelType($modelType)->get();

        foreach ($fieldsetModels as $fieldsetModel) {
            $forms = [];
            $fieldsets = $fieldsetModel->fieldset->fieldFieldsets()->orderBy('order', 'asc')->get();
            $fields = $fieldsets->pluck('field');
            $fieldIDs = $fields->pluck('id');
            $fieldResponses = collect();

            if ($modelID != null) {
                /** @var Collection<int, FieldResponse> */
                $fieldResponses = FieldResponse::whereIn('field_id', $fieldIDs)->whereModelType($modelType)->whereModelId($modelID)->get();
            }

            foreach ($fieldsets as $fieldFieldset) {
                $input = null;

                /** @var FieldResponse|null $fieldResponse */
                $fieldResponse = $fieldResponses->where('field_id', $fieldFieldset->field->id)->first();

                if ($fieldResponse !== null) {
                    $defaultValue = $fieldResponse->value;
                } else {
                    $defaultValue = $fieldFieldset->field->default_value;
                }

                $fieldIdentificator = static::getCustomFieldKey() . $fieldFieldset->field->id;

                if ($fieldFieldset->field->type instanceof Text) {
                    if ($fieldFieldset->field->type->getRule() === FieldRule::Password) {
                        if ($defaultValue !== null) {
                            if (self::getCurrentUser()->isSuperAdmin()) {
                                $defaultValue = Crypt::decrypt($defaultValue);
                            } else {
                                $fieldFieldset->field->hint .= '<br>(' . __('only.admin.can.edit') . ')';
                                $defaultValue = '';
                            }
                        }

                        $fieldIdentificator .= '_encrypt';
                    }
                }

                $input = $fieldFieldset->field->type->getFilamentComponent($fieldIdentificator, $fieldFieldset->field);

                $input
                    ->label($fieldFieldset->field->title)
                    ->hint(new HtmlString($fieldFieldset->field->hint))
                    ->required($fieldFieldset->required);

                if ($defaultValue !== null && $defaultValue !== '') {
                    $input->afterStateHydrated(fn (Forms\Components\Field $component) => $component->state($defaultValue))
                        ->default($defaultValue);
                }

                if ($input instanceof Select) {
                    $input->searchable($fieldFieldset->searchable);
                }

                $forms[] = $input;
            }

            $schema[] = Forms\Components\Tabs\Tab::make($fieldsetModel->fieldset->name)
                ->schema($forms)
                ->columns(count($fieldIDs) > 1 ? 2 : 1);
        }

        return $schema;
    }

    public static function getCustomFieldKey(): string
    {
        return 'custom-field_';
    }

    /**
     * Handle request in creation or edit resource
     *
     * @param  array<string, mixed>  $data
     */
    public static function handleRequest(array $data, Model $model): void
    {
        if (count($data) === 0 or ! $model->exists) {
            return;
        }

        /** @var array<int, mixed> $fieldDatas */
        $fieldDatas = [];
        $fieldIDs = [];

        foreach ($data as $key => $val) {
            if (Str::startsWith($key, static::getCustomFieldKey())) {
                $keys = explode('_', $key);
                if (count($keys) === 3) {
                    if (self::getCurrentUser()->isSuperAdmin()) {
                        array_pop($keys);

                        if ($val !== null) {
                            $fieldDatas[end($keys)] = Crypt::encrypt($val);
                        } else {
                            $fieldDatas[end($keys)] = null;
                        }
                    }

                    continue;
                }

                $fieldDatas[end($keys)] = $val;
            }
        }

        // If don´t pass custom data, discard handle
        if (count($fieldDatas) === 0) {
            return;
        }

        ksort($fieldDatas);

        $fieldIDs = array_keys($fieldDatas);

        $fieldsAssignToModel = FieldsetModel::whereModelType($model::class)->get()->pluck('fieldset')->pluck('fields')->flatten()->pluck('id');

        if ($fieldsAssignToModel->count() === 0) {
            throw ValidationException::withMessages(['field_not_exists' => 'Not exist fields relation with this model: ' . $model::class]);
        }

        $fieldResponseDatas = collect($fieldDatas);
        $fieldDefaultValues = Field::whereIn('id', $fieldIDs)->get()->pluck('default_value', 'id')->sortKeys();

        $diffResponseDataAndDefaults = collect(self::diffAssoc($fieldResponseDatas->toArray(), $fieldDefaultValues->toArray()));
        $diffResponseDataAndDefaultIDs = $diffResponseDataAndDefaults->keys();

        $discardedResponses = collect(self::diffAssoc($fieldResponseDatas->toArray(), $diffResponseDataAndDefaults->toArray()));
        $discardedResponseIDs = $discardedResponses->keys();

        $fieldResponses = FieldResponse::whereModelType($model::class)->whereModelId($model->getKey())->whereIn('field_id', $diffResponseDataAndDefaultIDs->toArray())->get()->pluck('value', 'field_id')->sortKeys();
        $discardedFieldResponses = FieldResponse::whereModelType($model::class)->whereModelId($model->getKey())->whereIn('field_id', $discardedResponseIDs->toArray())->get()->pluck('value', 'field_id')->sortKeys();

        $diffResponseDataAndDefaults = collect(self::diffAssoc($diffResponseDataAndDefaults->toArray(), $fieldResponses->toArray()));
        $diffDiscardedResponses = collect(self::diffAssoc($discardedResponses->toArray(), $discardedFieldResponses->toArray(), true));

        $responseDataToSave = $diffResponseDataAndDefaults->union($diffDiscardedResponses);

        foreach ($responseDataToSave as $key => $value) {
            $response = FieldResponse::whereFieldId($key)->whereModelId($model->getKey())->whereModelType($model::class)->first();

            if ($response === null) {
                $fieldResponse = new FieldResponse();
                $fieldResponse->field()->associate((int) $key);
                $fieldResponse->model()->associate($model);
                $fieldResponse->value = $value;
                $fieldResponse->save();
            } elseif (! $response->wasRecentlyCreated) {
                $response->update(['value' => $value]);
            }
        }
    }

    /**
     * Get differences between the first array in the second array,
     * you would to ignore the non existent value in the second array
     *
     * @param  array<mixed>  $first
     * @param  array<mixed>  $second
     * @return array<mixed>
     */
    private static function diffAssoc(array $first, array $second, bool $discardNonExistent = false): array
    {
        $differents = [];

        foreach ($first as $key => $value) {
            if (! array_key_exists($key, $second)) {
                if (! $discardNonExistent) {
                    $differents[$key] = $value;
                }

                continue;
            }

            if ($value !== $second[$key]) {
                $differents[$key] = $value;

                continue;
            }

            if (is_array($value) && $value !== $second[$key]) {
                $differents[$key] = $value;
            }
        }

        return $differents;
    }

    private static function getCurrentUser(): User
    {
        /** @var User $user */
        $user = Auth::user();

        return $user;
    }
}
