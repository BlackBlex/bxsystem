<?php

declare(strict_types=1);

namespace Modules\CustomFields\Contracts;

use Filament\Forms\Components\Component;
use Filament\Forms\Components\Field as FilamentField;
use Modules\CustomFields\Entities\Field;
use Modules\CustomFields\Enums\FieldRule;
use Modules\CustomFields\Enums\FieldType;

interface FieldStructure
{
    /**
     * Get rules that only correspond to the type
     *
     * @return array<FieldRule>
     */
    public static function getAvailableRules(): array;

    /**
     * Set the data from the array in the current instance
     *
     * @param  array<string>  $data
     */
    public function deserialize(array $data): bool;

    public function getFilamentComponent(string $identificator, Field $fieldInstance): FilamentField;

    /**
     * Get options for this field
     *
     * @return array<Component>
     */
    public function getFormOptions(): array;

    public function getMaxSize(): int;

    public function getMinSize(): int;

    public function getRule(): FieldRule;

    public function getSize(): int;

    public function getType(): FieldType;

    /**
     * Get the data from the array and set in the current instance
     *
     * @param  array<mixed>  $data
     * @return array<mixed>
     */
    public function handleRequestOnFieldCreateSave(array $data): array;

    /**
     * Get the data from the current instance and put in passed array
     *
     * @param  array<mixed>  $data
     * @return array<mixed>
     */
    public function handleRequestOnFieldFill(array $data): array;

    public function serialize(): string;

    public function setMaxSize(int $newMaxSize): static;

    public function setMinSize(int $newMinSize): static;

    public function setRule(FieldRule $newRule): static;

    public function setSize(int $newSize): static;
}
