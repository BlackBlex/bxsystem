<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('uploads/view/{path}', function (string $path) {
    /** @var \Illuminate\Filesystem\FilesystemAdapter $uploadStorage */
    $uploadStorage = Storage::disk('uploads');

    if ($uploadStorage->exists($path)) {
        return $uploadStorage->response($path);
    }

    return response()->json(['message' => 'Not Found!'], 404);
})->name('upload.view')->where('path', '(.*)?')->middleware(['web', 'signed']);
