<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('field_responses');
    }

    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('field_responses', function (Blueprint $table): void {
            $table->id();
            $table->foreignId('field_id')->constrained('fields');
            $table->morphs('model');
            $table->text('value')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->unique(['field_id', 'model_type', 'model_id']);
        });
    }
};
