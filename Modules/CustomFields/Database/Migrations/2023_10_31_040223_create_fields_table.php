<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('fields');
    }

    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('fields', function (Blueprint $table): void {
            $table->id();
            $table->string('title')->unique();
            $table->text('type');
            $table->string('hint')->nullable();
            $table->json('options')->nullable();
            $table->text('default_value')->nullable();
            $table->text('format')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
};
