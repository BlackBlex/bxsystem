<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('field_fieldset');
    }

    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('field_fieldset', function (Blueprint $table): void {
            $table->id();
            $table->foreignId('field_id')->constrained('fields');
            $table->foreignId('fieldset_id')->constrained('fieldsets');
            $table->integer('order')->default(1);
            $table->boolean('required')->default(false);
            $table->boolean('searchable')->default(false);
            $table->boolean('show_in_table')->default(false);
            $table->unique(['field_id', 'fieldset_id']);
            $table->timestamps();
            $table->softDeletes();
        });
    }
};
