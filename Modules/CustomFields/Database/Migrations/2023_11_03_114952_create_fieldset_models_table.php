<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('fieldset_models');
    }

    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('fieldset_models', function (Blueprint $table): void {
            $table->id();
            $table->foreignId('fieldset_id')->constrained('fieldsets');
            $table->string('model_type');
            $table->timestamps();
            $table->softDeletes();

            $table->unique(['fieldset_id', 'model_type']);
        });
    }
};
