<?php

declare(strict_types=1);

namespace Modules\CustomFields\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Modules\CustomFields\Entities\Fieldset;

/**
 * @extends Factory<Fieldset>
 */
class FieldsetFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var class-string<Fieldset>
     */
    protected $model = Fieldset::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name' => $this->faker->unique()->words(2, true),
        ];
    }
}
