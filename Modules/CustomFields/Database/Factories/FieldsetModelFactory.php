<?php

declare(strict_types=1);

namespace Modules\CustomFields\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Modules\CustomFields\Entities\Pivot\FieldsetModel;

/**
 * @extends Factory<FieldsetModel>
 */
class FieldsetModelFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var class-string<FieldsetModel>
     */
    protected $model = FieldsetModel::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            //
        ];
    }
}
