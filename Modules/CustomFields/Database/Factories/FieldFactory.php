<?php

declare(strict_types=1);

namespace Modules\CustomFields\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Modules\CustomFields\Entities\Field;
use Modules\CustomFields\Entities\Types\Text;

/**
 * @extends Factory<Field>
 */
class FieldFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var class-string<Field>
     */
    protected $model = Field::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'type' => new Text(),
            'title' => $this->faker->unique()->words(3, true),
            'hint' => $this->faker->sentence(3),
        ];
    }
}
