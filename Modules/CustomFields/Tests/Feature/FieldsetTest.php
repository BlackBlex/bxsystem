<?php

declare(strict_types=1);

use Illuminate\Database\QueryException;
use Modules\CustomFields\Entities\Fieldset;

uses(Tests\Traits\RefreshTestDatabase::class);
uses(Tests\TestCase::class);

test('can create a fieldset', function (): void {
    $fieldset = Fieldset::factory()->create(['name' => 'Dummyset']);

    expect($fieldset->id)->not()->toBeNull();
});

test('can´t create a fieldset without name [Throw: QueryException]', function (): void {
    $_fieldset = Fieldset::factory()->create(['name' => null]);
})->throws(QueryException::class);
