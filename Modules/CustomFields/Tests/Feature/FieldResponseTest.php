<?php

declare(strict_types=1);

use App\Models\User;
use Illuminate\Database\QueryException;
use Modules\CustomFields\Entities\Field;
use Modules\CustomFields\Entities\Fieldset;
use Modules\CustomFields\Entities\Pivot\FieldFieldset;
use Modules\CustomFields\Entities\Pivot\FieldResponse;
use Modules\CustomFields\Entities\Pivot\FieldsetModel;
use Modules\CustomFields\Entities\Types\Text;
use Modules\Users\Entities\Employee;

uses(Tests\Traits\RefreshTestDatabase::class);
uses(Tests\TestCase::class);

beforeEach(function (): void {
    $this->field = Field::factory()->create(['type' => new Text(), 'title' => 'Dummy', 'hint' => 'Dummy fit message']);
    $this->fieldset = Fieldset::factory()->create(['name' => 'Dummyset']);
    $this->fieldsetModel = FieldsetModel::make(['model_type' => Employee::class]);
    $this->fieldsetModel->fieldset()->associate($this->fieldset);
    $this->fieldsetModel->save();

    $fieldFieldset = new FieldFieldset(['required' => true]);
    $fieldFieldset->field()->associate($this->field);
    $fieldFieldset->fieldset()->associate($this->fieldset);

    $this->fieldset->fieldFieldsets()->save($fieldFieldset);
});

test('can save a new response in employee', function (): void {
    $user = User::factory()->create();
    $employee = Employee::factory()->for($user)->create();
    $fieldResponse = FieldResponse::make();

    $fieldResponse->field()->associate($this->field);
    $fieldResponse->model()->associate($employee);
    $fieldResponse->value = 'Meh';
    $fieldResponse->save();

    $fieldResponse->refresh();

    expect($this->field->responses)->toHaveCount(1);
    expect($fieldResponse->field_id)->toBe($this->field->id);
    expect($fieldResponse->model_id)->toBe($employee->id);
});

test('can´t save a empty response [Throw: QueryException]', function (): void {
    $fieldResponse = FieldResponse::factory()->create([]);
})->throws(QueryException::class);
