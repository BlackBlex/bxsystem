<?php

declare(strict_types=1);

use Illuminate\Database\QueryException;
use Modules\CustomFields\Contracts\FieldStructure;
use Modules\CustomFields\Entities\Field;
use Modules\CustomFields\Entities\Types\Text;
use Modules\CustomFields\Enums\FieldRule;

uses(Tests\Traits\RefreshTestDatabase::class);
uses(Tests\TestCase::class);

test('can create a field', function (FieldStructure $type, ?string $format = null): void {
    $field = Field::factory()->create(['type' => $type, 'title' => 'Dummy', 'hint' => 'Dummy fit message', 'format' => $format]);
    $field->refresh();

    expect($field->id)->not()->toBeNull();
    expect($field->type)->toEqual($type);
    expect($field->format)->toEqual($format);
    expect($field->type->getFilamentComponent('2', $field))->toEqual($type->getFilamentComponent('2', $field));
})->with([
    'any text' => [fn (): FieldStructure => new Text()],
    'alpha text' => [function (): FieldStructure {
        $result = new Text();
        $result->setRule(FieldRule::Alpha);

        return $result;
    }],
    'alpha_dash text' => [function (): FieldStructure {
        $result = new Text();
        $result->setRule(FieldRule::AlphaDash);

        return $result;
    }],
    'alpha_num text' => [function (): FieldStructure {
        $result = new Text();
        $result->setRule(FieldRule::AlphaNumeric);

        return $result;
    }],
    'email text' => [function (): FieldStructure {
        $result = new Text();
        $result->setRule(FieldRule::Email);

        return $result;
    }],
    'ip text' => [function (): FieldStructure {
        $result = new Text();
        $result->setRule(FieldRule::IP);

        return $result;
    }],
    'ipv4 text' => [function (): FieldStructure {
        $result = new Text();
        $result->setRule(FieldRule::IPv4);

        return $result;
    }],
    'ipv6 text' => [function (): FieldStructure {
        $result = new Text();
        $result->setRule(FieldRule::IPv6);

        return $result;
    }],
    'mac text' => [function (): FieldStructure {
        $result = new Text();
        $result->setRule(FieldRule::MAC);

        return $result;
    }],
    'numeric text' => [function (): FieldStructure {
        $result = new Text();
        $result->setRule(FieldRule::Numeric);

        return $result;
    }],
    'password text' => [function (): FieldStructure {
        $result = new Text();
        $result->setRule(FieldRule::Password);

        return $result;
    }],
    'regex text' => [function (): FieldStructure {
        $result = new Text();
        $result->setRule(FieldRule::Regex);

        return $result;
    }, '\/^[0-9]{15}$\/'],
    'telephone text' => [function (): FieldStructure {
        $result = new Text();
        $result->setRule(FieldRule::Telephone);

        return $result;
    }],
    'format MM/DD/YYYY text' => [fn (): FieldStructure => new Text(), '99/99/9999=MM/DD/YYYY'],
]);

test('can´t create a field without title, type, hint [Throw: QueryException]', function (): void {
    $_field = Field::factory()->create(['title' => null, 'type' => null, 'hint' => null]);
})->throws(QueryException::class);
