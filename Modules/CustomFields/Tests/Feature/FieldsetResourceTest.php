<?php

declare(strict_types=1);

use App\Models\Permission;
use App\Models\Role;
use Filament\Actions\DeleteAction;
use Filament\Facades\Filament;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Str;
use Modules\CustomFields\Entities\Field;
use Modules\CustomFields\Entities\Fieldset;
use Modules\CustomFields\Filament\Admin\Resources\FieldsetResource;
use Modules\CustomFields\Filament\Admin\Resources\FieldsetResource\Pages\CreateFieldset;
use Modules\CustomFields\Filament\Admin\Resources\FieldsetResource\Pages\EditFieldset;
use Modules\CustomFields\Filament\Admin\Resources\FieldsetResource\Pages\ListFieldsets;
use Modules\CustomFields\Filament\Admin\Resources\FieldsetResource\Pages\ViewFieldset;
use Modules\Shared\Tests\TestCase;
use Modules\Users\Entities\Employee;

use function Pest\Faker\fake;
use function Pest\Livewire\livewire;

uses(Tests\Traits\RefreshTestDatabase::class);
uses(TestCase::class);

function createManagerRoleFieldset(): void
{
    $fieldsetPermissions = ['view_fieldset', 'view_any_fieldset', 'create_fieldset', 'update_fieldset', 'restore_fieldset', 'restore_any_fieldset', 'replicate_fieldset', 'reorder_fieldset', 'delete_fieldset', 'delete_any_fieldset', 'force_delete_fieldset', 'force_delete_any_fieldset'];

    $ManagerRole = Role::findOrCreate('Manager', 'web');

    if (! Permission::whereName($fieldsetPermissions[0])->exists()) {
        foreach ($fieldsetPermissions as $permission) {
            Permission::findOrCreate($permission, 'web');
        }
    }

    if (! $ManagerRole->hasAllPermissions($fieldsetPermissions)) {
        $ManagerRole->givePermissionTo($fieldsetPermissions);
    }
}

function createNewFieldset(): Fieldset
{
    return Fieldset::factory()->create(['name' => 'Dummy set']);
}

/** @return Collection<int, Field> */
function createSetOfFields(): Collection
{
    return Field::factory()->count(3)->create();
}

beforeEach(function (): void {
    Filament::setCurrentPanel(Filament::getPanel('customfields::admin'));

    createManagerRoleFieldset();

    $this->user = $this->createUser('first_name', 'Jovani', data: ['first_name' => 'Jovani']);
    $this->actingAs($this->user);

    if (! $this->user->hasRole('Manager')) {
        $this->user->assignRole('Manager');
    }
});

it('can render index page', function (): void {
    $this->get(FieldsetResource::getUrl('index'))->assertSuccessful();
});

it('can list fieldset', function (): void {
    $fieldset = createNewFieldset();

    livewire(ListFieldsets::class)
        ->assertCanSeeTableRecords([$fieldset]);
});

it('can render create page', function (): void {
    $this->get(FieldsetResource::getUrl('create'))->assertSuccessful();
});

it('can create new fieldset without model and fields', function (): void {
    $data = [
        'name' => fake()->unique()->words(3, true),
    ];

    livewire(CreateFieldset::class)
        ->fillForm($data)
        ->call('create')
        ->assertHasNoFormErrors();

    $fieldset = Fieldset::whereName($data['name'])->first();

    expect($fieldset)->not()->toBeNull();
});

it('can create new fieldset with model and without fields', function (): void {
    $data = [
        'name' => fake()->unique()->words(3, true),
    ];

    $data['models'][Str::uuid()->toString()] = ['model_type' => Employee::class];

    livewire(CreateFieldset::class)
        ->fillForm($data)
        ->call('create')
        ->assertHasNoFormErrors();

    $fieldset = Fieldset::whereName($data['name'])->first();

    expect($fieldset)->not()->toBeNull();
    expect($fieldset->models)->toHaveCount(1);
    expect($fieldset->models->pluck('model_type')->toArray())->toBe([Employee::class]);
});

it('can create new fieldset with model and fields', function (): void {
    $fields = createSetOfFields();

    $data = [
        'name' => fake()->unique()->words(3, true),
    ];

    $data['models'][Str::uuid()->toString()] = ['model_type' => Employee::class];
    $fieldNames = [];

    foreach ($fields as $index => $field) {
        $fieldNames[] = $field->title;
        $data['fieldFieldsets'][Str::uuid()->toString()] = [
            'order' => $index + 1,
            'field_id' => $field->id,
            'required' => fake()->boolean(),
            'searchable' => fake()->boolean(20),
            'show_in_table' => fake()->boolean(20),
        ];
    }

    livewire(CreateFieldset::class)
        ->fillForm($data)
        ->call('create')
        ->assertHasNoFormErrors();

    $fieldset = Fieldset::whereName($data['name'])->first();

    expect($fieldset)->not()->toBeNull();
    expect($fieldset->models)->toHaveCount(1);
    expect($fieldset->models->pluck('model_type')->toArray())->toBe([Employee::class]);
    expect($fieldset->fieldFieldsets)->toHaveCount(3);
    expect($fieldset->fieldFieldsets->pluck('field')->pluck('title')->toArray())->toBe($fieldNames);
});

it('can´t create new fieldset without title', function (): void {
    $data = [
        'name' => null,
    ];

    livewire(CreateFieldset::class)
        ->fillForm($data)
        ->call('create')
        ->assertHasFormErrors([
            'name' => 'required',
        ]);
});

it('can render edit page', function (): void {
    $fieldset = createNewFieldset();

    $this->get(FieldsetResource::getUrl('edit', ['record' => $fieldset->getKey()]))->assertSuccessful();
});

it('can retrieve field data from DB (edit)', function (): void {
    $fieldset = createNewFieldset();

    livewire(EditFieldset::class, [
        'record' => $fieldset->getKey(),
    ])
        ->assertFormSet([
            'name' => $fieldset->name,
        ]);
});

it('can edit field', function (): void {
    $fieldset = createNewFieldset();

    $data = [
        'name' => fake()->unique()->words(3, true),
    ];

    livewire(EditFieldset::class, [
        'record' => $fieldset->getKey(),
    ])
        ->fillForm($data)
        ->call('save')
        ->assertHasNoFormErrors();

    expect($fieldset->refresh())
        ->name->toBe($data['name']);
});

it('can´t edit field with empty data', function (): void {
    $fieldset = createNewFieldset();

    $data = [
        'name' => null,
    ];

    livewire(EditFieldset::class, [
        'record' => $fieldset->getKey(),
    ])
        ->fillForm($data)
        ->call('save')
        ->assertHasFormErrors([
            'name' => 'required',
        ]);
});

it('can delete field', function (): void {
    $fieldset = createNewFieldset();

    livewire(EditFieldset::class, [
        'record' => $fieldset->getKey(),
    ])
        ->callAction(DeleteAction::class);

    $fieldset->refresh();

    expect($fieldset->trashed())->toBeTrue();
});

it('can´t delete field (missing permission)', function (): void {
    $otherUser = $this->createUser('first_name', 'Viewer', data: ['first_name' => 'Viewer']);
    $this->actingAs($otherUser);

    if (! $otherUser->hasRole('Manager')) {
        $otherUser->assignRole('Manager');

        $role = Role::findByName('Manager', 'web');
        $role->revokePermissionTo(['delete_fieldset', 'delete_any_fieldset', 'force_delete_fieldset', 'force_delete_any_fieldset']);
    }

    $fieldset = createNewFieldset();

    livewire(EditFieldset::class, [
        'record' => $fieldset->getKey(),
    ])
        ->assertActionHidden(DeleteAction::class);
});

it('can render view page', function (): void {
    $fields = createSetOfFields();

    $data = [
        'name' => fake()->unique()->words(3, true),
    ];

    $data['models'][Str::uuid()->toString()] = ['model_type' => Employee::class];
    $fieldNames = [];

    foreach ($fields as $index => $field) {
        $fieldNames[] = $field->title;
        $data['fieldFieldsets'][Str::uuid()->toString()] = [
            'order' => $index + 1,
            'field_id' => $field->id,
            'required' => fake()->boolean(),
            'searchable' => fake()->boolean(20),
            'show_in_table' => fake()->boolean(20),
        ];
    }

    livewire(CreateFieldset::class)
        ->fillForm($data)
        ->call('create')
        ->assertHasNoFormErrors();

    $fieldset = Fieldset::whereName($data['name'])->first();

    $this->get(FieldsetResource::getUrl('view', ['record' => $fieldset->getKey()]))->assertSuccessful();
});

it('can retrieve field data from DB (view)', function (): void {
    $fieldset = createNewFieldset();

    livewire(ViewFieldset::class, [
        'record' => $fieldset->getKey(),
    ])
        ->assertFormSet([
            'name' => $fieldset->name,
        ]);
});
