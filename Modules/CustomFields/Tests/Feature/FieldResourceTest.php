<?php

declare(strict_types=1);

use App\Models\Permission;
use App\Models\Role;
use Filament\Actions\DeleteAction;
use Filament\Facades\Filament;
use Illuminate\Database\Eloquent\Collection;
use Modules\CustomFields\Contracts\FieldStructure;
use Modules\CustomFields\Entities\Field;
use Modules\CustomFields\Entities\Fieldset;
use Modules\CustomFields\Entities\Types\Select;
use Modules\CustomFields\Entities\Types\Text;
use Modules\CustomFields\Entities\Types\Toggle;
use Modules\CustomFields\Enums\FieldRule;
use Modules\CustomFields\Enums\FieldType;
use Modules\CustomFields\Filament\Admin\Resources\FieldResource;
use Modules\CustomFields\Filament\Admin\Resources\FieldResource\Pages\CreateField;
use Modules\CustomFields\Filament\Admin\Resources\FieldResource\Pages\EditField;
use Modules\CustomFields\Filament\Admin\Resources\FieldResource\Pages\ListFields;
use Modules\CustomFields\Filament\Admin\Resources\FieldResource\Pages\ViewField;
use Modules\Shared\Tests\TestCase;

use function Pest\Livewire\livewire;

uses(Tests\Traits\RefreshTestDatabase::class);
uses(TestCase::class);

function createManagerRoleField(): void
{
    $fieldPermissions = ['view_field', 'view_any_field', 'create_field', 'update_field', 'restore_field', 'restore_any_field', 'replicate_field', 'reorder_field', 'delete_field', 'delete_any_field', 'force_delete_field', 'force_delete_any_field'];

    $ManagerRole = Role::findOrCreate('Manager', 'web');

    if (! Permission::whereName($fieldPermissions[0])->exists()) {
        foreach ($fieldPermissions as $permission) {
            Permission::findOrCreate($permission, 'web');
        }
    }

    if (! $ManagerRole->hasAllPermissions($fieldPermissions)) {
        $ManagerRole->givePermissionTo($fieldPermissions);
    }
}

function createNewField(): Field
{
    return Field::factory()->create(['type' => new Text(), 'title' => 'Dummy', 'hint' => 'Dummy fit message']);
}

/** @return Collection<int, Fieldset> */
function createSetOfFieldsets(): Collection
{
    return Fieldset::factory()->count(3)->create();
}

beforeEach(function (): void {
    Filament::setCurrentPanel(Filament::getPanel('customfields::admin'));

    createManagerRoleField();

    $this->user = $this->createUser('first_name', 'Jovani', data: ['first_name' => 'Jovani']);
    $this->actingAs($this->user);

    if (! $this->user->hasRole('Manager')) {
        $this->user->assignRole('Manager');
    }
});

it('can render index page', function (): void {
    $this->get(FieldResource::getUrl('index'))->assertSuccessful();
});

it('can list field', function (): void {
    $field = createNewField();

    livewire(ListFields::class)
        ->assertCanSeeTableRecords([$field]);
});

it('can render create page', function (): void {
    $this->get(FieldResource::getUrl('create'))->assertSuccessful();
});

it('can create new field', function (FieldStructure $type, ?string $default_value_input = null, $default_value = null, $options = null): void {
    $title = $type->getType()->getLabel() . ' field';

    $data = [
        'type' => $type->getType()->value,
        'title' => $title,
        'rule' => $type->getRule()->value,
        'hint' => 'It´s a ' . $title . ' for testing',
    ];

    if ($options !== null) {
        $data['options'] = $options;
    }

    if ($default_value_input !== null) {
        $data[$default_value_input] = $default_value;
    }

    livewire(CreateField::class)
        ->fillForm($data)
        ->call('create')
        ->assertHasNoFormErrors();

    $field = Field::whereTitle($data['title'])->whereHint($data['hint'])->first();

    if ($default_value == null) {
        if ($type->getType() == FieldType::Toggle) {
            $default_value = false;
        }

        if ($type->getRule() == FieldRule::Numeric || $type->getType() == FieldType::Select) {
            $default_value = 0;
        }
    }

    expect($field)->not()->toBeNull();
    expect($field)->options->toBe($options ? $data['options'] : null)
        ->type->toEqual($type)
        ->default_value->toBe($default_value);
})->with([
    'text default_value' => [fn (): FieldStructure => new Text(), 'default_value_text', 'Testing'],
    'text not default_value' => [fn (): FieldStructure => new Text()],
    'number default_value' => [function (): FieldStructure {
        $result = new Text();
        $result->setRule(FieldRule::Numeric);

        return $result;
    }, 'default_value_text', 21],
    'number not default_value' => [function (): FieldStructure {
        $result = new Text();
        $result->setRule(FieldRule::Numeric);

        return $result;
    }],
    'toggle default_value' => [fn (): FieldStructure => new Toggle(), 'default_value_bool', true],
    'toggle not default_value' => [fn (): FieldStructure => new Toggle()],
    'select default_value' => [fn (): FieldStructure => new Select(), 'default_value_select', 2, [1 => 'None', 2 => 'Over here', 3 => 'Not yet']],
    'select not default_value' => [fn (): FieldStructure => new Select(), null, null, [1 => 'None', 2 => 'Over here', 3 => 'Not yet']],
]);

it('can´t create new field without title', function (): void {
    $data = [
        'title' => null,
    ];

    livewire(CreateField::class)
        ->fillForm($data)
        ->call('create')
        ->assertHasFormErrors([
            'title' => 'required',
        ]);
});

it('can render edit page', function (): void {
    $field = createNewField();

    $this->get(FieldResource::getUrl('edit', ['record' => $field->getKey()]))->assertSuccessful();
});

it('can retrieve field data from DB (edit)', function (): void {
    $field = createNewField();

    livewire(EditField::class, [
        'record' => $field->getKey(),
    ])
        ->assertFormSet([
            'type' => $field->type->getType(),
            'title' => $field->title,
            'hint' => $field->hint,
        ]);
});

it('can edit text field to toggle', function (): void {
    $field = createNewField();

    $toggleType = new Toggle();

    $data = [
        'type' => $toggleType->getType()->value,
        'title' => 'Number',
        'hint' => 'It´s a toggle field for testing',
        'default_value_bool' => true,
    ];

    livewire(EditField::class, [
        'record' => $field->getKey(),
    ])
        ->fillForm($data)
        ->call('save')
        ->assertHasNoFormErrors();

    expect($field->refresh())
        ->type->toEqual($toggleType)
        ->title->toBe($data['title'])
        ->hint->toBe($data['hint'])
        ->default_value->toBe($data['default_value_bool']);
});

it('can´t edit field with empty data', function (): void {
    $field = createNewField();

    $data = [
        'title' => null,
    ];

    livewire(EditField::class, [
        'record' => $field->getKey(),
    ])
        ->fillForm($data)
        ->call('save')
        ->assertHasFormErrors([
            'title' => 'required',
        ]);
});

it('can delete field', function (): void {
    $field = createNewField();

    livewire(EditField::class, [
        'record' => $field->getKey(),
    ])
        ->callAction(DeleteAction::class);

    $field->refresh();

    expect($field->trashed())->toBeTrue();
});

it('can´t delete field (missing permission)', function (): void {
    $otherUser = $this->createUser('first_name', 'Viewer', data: ['first_name' => 'Viewer']);
    $this->actingAs($otherUser);

    if (! $otherUser->hasRole('Manager')) {
        $otherUser->assignRole('Manager');

        $role = Role::findByName('Manager', 'web');
        $role->revokePermissionTo(['delete_field', 'delete_any_field', 'force_delete_field', 'force_delete_any_field']);
    }

    $field = createNewField();

    livewire(EditField::class, [
        'record' => $field->getKey(),
    ])
        ->assertActionHidden(DeleteAction::class);
});

it('can render view page', function (): void {
    $field = createNewField();

    $this->get(FieldResource::getUrl('view', ['record' => $field->getKey()]))->assertSuccessful();
});

it('can retrieve text field data from DB (view)', function (): void {
    $field = createNewField();

    livewire(ViewField::class, [
        'record' => $field->getKey(),
    ])
        ->assertFormSet([
            'type' => $field->type->getType(),
            'title' => $field->title,
            'hint' => $field->hint,
        ]);
});

it('can retrieve toggle field data from DB (view)', function (): void {
    $field = Field::factory()->create(['type' => new Toggle(), 'title' => 'Toggle field', 'hint' => 'It´s toggle']);

    livewire(ViewField::class, [
        'record' => $field->getKey(),
    ])
        ->assertFormSet([
            'type' => $field->type->getType(),
            'title' => $field->title,
            'hint' => $field->hint,
        ]);
});
