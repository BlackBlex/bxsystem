<?php

declare(strict_types=1);

use Modules\CustomFields\Entities\Field;
use Modules\CustomFields\Entities\Fieldset;
use Modules\CustomFields\Entities\Pivot\FieldFieldset;
use Modules\CustomFields\Entities\Types\Text;

uses(Tests\Traits\RefreshTestDatabase::class);
uses(Tests\TestCase::class);

test('can assign to field in fieldset', function (): void {
    $field = Field::factory()->create(['type' => new Text(), 'title' => 'Dummy', 'hint' => 'Dummy fit message']);
    $fieldset = Fieldset::factory()->create(['name' => 'Dummyset']);

    $fieldFieldset = new FieldFieldset(['required' => true]);
    $fieldFieldset->field()->associate($field);
    $fieldFieldset->fieldset()->associate($fieldset);

    $fieldset->fieldFieldsets()->save($fieldFieldset);

    expect($fieldset->fieldFieldsets)->toHaveCount(1);
    expect($fieldset->fieldFieldsets->first()->field->title)->toBe($field->title);
});

test('can´t assign to null field in employee [Throw: PDOException]', function (): void {
    $field = null;
    $fieldset = Fieldset::factory()->create(['name' => 'Dummyset']);

    $fieldset->fieldFieldsets()->save(new FieldFieldset(['field_id' => null, 'required' => true, 'fieldset_id' => $fieldset->id]));
})->throws(PDOException::class);

test('can´t view a assigned field in fieldset', function (): void {
    $field = Field::factory()->create(['type' => new Text(), 'title' => 'Dummy', 'hint' => 'Dummy fit message']);
    $fieldset = Fieldset::factory()->create(['name' => 'Dummyset']);

    $availableFieldsBefore = Field::doesntHave('fieldsets')->get();

    $fieldFieldset = new FieldFieldset();
    $fieldFieldset->field()->associate($field);
    $fieldFieldset->fieldset()->associate($fieldset);

    $fieldset->fieldFieldsets()->save($fieldFieldset);

    $availableFieldsAfter = Field::doesntHave('fieldsets')->get();

    expect($availableFieldsBefore)->toHaveCount(1);
    expect($availableFieldsAfter)->toHaveCount(0);
});
