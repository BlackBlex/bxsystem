<?php

declare(strict_types=1);

use Filament\Facades\Filament;
use Modules\CustomFields\Entities\Field;
use Modules\CustomFields\Entities\Types\Text;
use Modules\CustomFields\Enums\FieldRule;
use Modules\CustomFields\Enums\FieldType;
use Modules\CustomFields\Enums\FileType;
use Modules\CustomFields\Filament\Admin\Resources\FieldResource;
use Modules\CustomFields\Filament\Admin\Resources\FieldResource\Pages\CreateField;
use Modules\Shared\Tests\TestCase;

use function Pest\Livewire\livewire;

uses(Tests\Traits\RefreshTestDatabase::class);
uses(TestCase::class);

beforeEach(function (): void {
    Filament::setCurrentPanel(Filament::getPanel('customfields::admin'));

    createManagerRoleField();

    $this->user = $this->createUser('first_name', 'Jovani', data: ['first_name' => 'Jovani']);
    $this->actingAs($this->user);

    if (! $this->user->hasRole('Manager')) {
        $this->user->assignRole('Manager');
    }
});

it('can render create page', function (): void {
    $this->get(FieldResource::getUrl('create'))->assertSuccessful();
});

it('can create new text field', function (FieldRule $rule, ?string $default_value_input = null, $default_value = null, $assertFail = [], $format = null): void {
    $type = new Text();
    $type->setRule($rule);

    $title = $type->getType()->getLabel() . ' field';

    $data = [
        'type' => $type->getType()->value,
        'title' => $title,
        'rule' => $type->getRule()->value,
        'hint' => 'It´s a ' . $title . ' for testing',
    ];

    if ($format !== null) {
        $data['format'] = $format;
    }

    if ($default_value_input !== null) {
        $data[$default_value_input] = $default_value;
    }

    $testField = livewire(CreateField::class)
        ->fillForm($data)
        ->call('create');

    if (count($assertFail) > 0) {
        $testField->assertHasFormErrors($assertFail);
    } else {
        $testField->assertHasNoFormErrors();

        $field = Field::whereTitle($data['title'])->whereHint($data['hint'])->first();

        expect($field)->not()->toBeNull()
            ->type->toEqual($type)
            ->default_value->toBe($default_value);
    }
})->with([
    'Any rule with correct value' => [FieldRule::Any, 'default_value_text', 'Testing'],
    'Alpha rule with correct value' => [FieldRule::Alpha, 'default_value_text', 'Testing'],
    'Alpha rule with wrong value' => [FieldRule::Alpha, 'default_value_text', 'Test_ting', ['default_value_text']],
    'AlphaDash rule with correct value' => [FieldRule::AlphaDash, 'default_value_text', 'Test_ing'],
    'AlphaDash rule with wrong value' => [FieldRule::AlphaDash, 'default_value_text', 'Test_#ting', ['default_value_text']],
    'AlphaNumeric rule with correct value' => [FieldRule::AlphaNumeric, 'default_value_text', 'Test9ing'],
    'AlphaNumeric rule with wrong value' => [FieldRule::AlphaNumeric, 'default_value_text', 'Tes9t#ting', ['default_value_text']],
    'Email rule with correct value' => [FieldRule::Email, 'default_value_text', 'example@example.com'],
    'Email rule with wrong value' => [FieldRule::Email, 'default_value_text', '@example.com', ['default_value_text']],
    'IPv4 rule with correct value' => [FieldRule::IPv4, 'default_value_text', '192.168.1.20'],
    'IPv4 rule with wrong value' => [FieldRule::IPv4, 'default_value_text', '192.194-91.2', ['default_value_text']],
    'IPv6 rule with correct value' => [FieldRule::IPv6, 'default_value_text', '9fe1:d873:98ed:a3bc:95f2:2c6d:1841:2c33'],
    'IPv6 rule with wrong value' => [FieldRule::IPv6, 'default_value_text', '9fe1:d8xd:98ed:a3bc:95f2:2c6d:1841:2c33', ['default_value_text']],
    'MAC rule with correct value' => [FieldRule::MAC, 'default_value_text', 'b2:f3:7a:8b:b6:e8'],
    'MAC rule with wrong value' => [FieldRule::MAC, 'default_value_text', 'b2:f3:7x.8b:b6:e8', ['default_value_text']],
    'Regex (IMEI) rule with correct value' => [FieldRule::Regex, 'default_value_text', '505750911153492', [], '/^[0-9]{15}$/'],
    'Regex (IMEI) rule with wrong value' => [FieldRule::Regex, 'default_value_text', 'd12a12', ['default_value_text'], '/^[0-9]{15}$/'],
    'Telephone rule with correct value' => [FieldRule::Telephone, 'default_value_text', '1234567890'],
    'Telephone rule with wrong value' => [FieldRule::Telephone, 'default_value_text', '1234#46701', ['default_value_text']],
    'URL rule with correct value' => [FieldRule::URL, 'default_value_text', 'https://google.com'],
    'URL rule with wrong value' => [FieldRule::URL, 'default_value_text', 'https://example.test', ['default_value_text']],
]);

it('can create new file field', function (FileType $type, ?string $filename = null): void {
    $title = $type->name . ' field' . $filename !== null ? ' with name' . $filename : '';

    $data = [
        'type' => FieldType::File->value,
        'title' => $title,
        'file_type' => $type->name,
    ];

    if ($filename !== null) {
        $data['filename'] = $filename;
    }

    livewire(CreateField::class)
        ->fillForm($data)
        ->call('create')
        ->assertHasNoFormErrors();

    $field = Field::whereTitle($data['title'])->first();

    expect($field)->not()->toBeNull();
    expect($field->type->getType())->toEqual(FieldType::File);

    expect($field->type->getFileTypeAllow())->toBe($type);

    if ($filename) {
        expect($field->type->getFilenameBase())->toBe($filename);
    }
})->with([
    'Any file without initial name' => [FileType::Any],
    'Any file with initial name' => [FileType::Any, 'Any File'],
    'Microsoft Excel file without initial name' => [FileType::MicrosoftExcel],
    'Microsoft Excel file with initial name' => [FileType::MicrosoftExcel, 'Microsoft Excel File'],
    'Microsoft PowerPoint file without initial name' => [FileType::MicrosoftPowerPoint],
    'Microsoft PowerPoint file with initial name' => [FileType::MicrosoftPowerPoint, 'Microsoft PowerPoint File'],
    'Microsoft Word file without initial name' => [FileType::MicrosoftWord],
    'Microsoft Word file with initial name' => [FileType::MicrosoftWord, 'Microsoft Word File'],
]);
