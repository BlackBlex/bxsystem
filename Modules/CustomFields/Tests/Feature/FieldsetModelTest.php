<?php

declare(strict_types=1);

use Illuminate\Database\QueryException;
use Modules\CustomFields\Entities\Field;
use Modules\CustomFields\Entities\Fieldset;
use Modules\CustomFields\Entities\Pivot\FieldFieldset;
use Modules\CustomFields\Entities\Pivot\FieldsetModel;
use Modules\CustomFields\Entities\Types\Text;
use Modules\Users\Entities\Employee;

uses(Tests\Traits\RefreshTestDatabase::class);
uses(Tests\TestCase::class);

test('can assign to dummy field in employee', function (): void {
    $field = Field::factory()->create(['type' => new Text(), 'title' => 'Dummy', 'hint' => 'Dummy fit message']);
    $fieldset = Fieldset::factory()->create(['name' => 'Dummyset']);

    $fieldsetModel = FieldsetModel::make(['model_type' => Employee::class]);
    $fieldsetModel->fieldset()->associate($fieldset);
    $fieldsetModel->save();

    $fieldFieldset = new FieldFieldset(['required' => true]);
    $fieldFieldset->field()->associate($field);
    $fieldFieldset->fieldset()->associate($fieldset);

    $fieldset->fieldFieldsets()->save($fieldFieldset);

    expect($fieldsetModel->id)->not()->toBeNull();
    expect($fieldsetModel->model_type)->toBe('Modules\\Users\\Entities\\Employee');
    expect($fieldsetModel->fieldset->id)->toBe($fieldset->id);
});

test('can´t assign to dummy field in null [Throw: QueryException]', function (): void {
    $field = Field::factory()->create(['type' => new Text(), 'title' => 'Dummy', 'hint' => 'Dummy fit message']);
    $fieldset = Fieldset::factory()->create(['name' => 'Dummyset']);
    $fieldsetModel = FieldsetModel::factory()->create(['fieldset_id' => $fieldset->id]);
})->throws(QueryException::class);
