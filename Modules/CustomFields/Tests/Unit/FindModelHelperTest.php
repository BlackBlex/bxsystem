<?php

declare(strict_types=1);

use Illuminate\Support\Collection;
use Modules\CustomFields\Helper\FindModelHelper;
use Modules\CustomFields\Traits\HasCustomFields;

uses(Tests\TestCase::class);

test('Can find all models without trait from path', function (string $directory, string $basePath, string $baseNamespace, int $count): void {
    $models = FindModelHelper::all($directory, $basePath, $baseNamespace);

    $modelsExpectation = expect($models);
    $modelsExpectation->toBeInstanceOf(Collection::class);
    $modelsExpectation->toHaveCount($count);
})->with([
    'App' => [fn (): string => app_path(), fn (): string => base_path(), '', 4],
    'Modules' => [fn (): string => base_path('Modules'), fn (): string => base_path('Modules'), 'Modules', 13],
]);

test('Can find all models with HasCustomFields trait from path', function (string $directory, string $basePath, string $baseNamespace, int $count): void {
    $models = FindModelHelper::all($directory, $basePath, $baseNamespace, HasCustomFields::class);

    $modelsExpectation = expect($models);
    $modelsExpectation->toBeInstanceOf(Collection::class);
    $modelsExpectation->toHaveCount($count);
})->with([
    'App' => [fn (): string => app_path(), fn (): string => base_path(), '', 0],
    'Modules' => [fn (): string => base_path('Modules'), fn (): string => base_path('Modules'), 'Modules', 1],
]);

test('Can find all models without trait from multiple paths', function (): void {
    $models = FindModelHelper::allFrom([app_path(), base_path('Modules')], [base_path(), base_path('Modules')], ['', 'Modules']);

    $modelsExpectation = expect($models);
    $modelsExpectation->toBeInstanceOf(Collection::class);
    $modelsExpectation->toHaveCount(17);
});

test('Can find all models from with HasCustomFields trait multiple paths', function (): void {
    $models = FindModelHelper::allFrom([app_path(), base_path('Modules')], [base_path(), base_path('Modules')], ['', 'Modules'], HasCustomFields::class);

    $modelsExpectation = expect($models);
    $modelsExpectation->toBeInstanceOf(Collection::class);
    $modelsExpectation->toHaveCount(1);
});
