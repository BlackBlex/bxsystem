<?php

declare(strict_types=1);

namespace Modules\CustomFields\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Modules\CustomFields\Entities\Field;
use Modules\CustomFields\Entities\Fieldset;
use Modules\CustomFields\Policies\FieldPolicy;
use Modules\CustomFields\Policies\FieldsetPolicy;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The model to policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        Field::class => FieldPolicy::class,
        Fieldset::class => FieldsetPolicy::class,
    ];

    /**
     * Register any application authentication / authorization services.
     */
    public function boot(): void
    {
        $this->registerPolicies();
    }
}
