<?php

declare(strict_types=1);

namespace Modules\CustomFields\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Modules\CustomFields\Entities\Pivot\FieldResponse;
use Modules\CustomFields\Observers\FieldResponseObserver;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The model observers to register.
     *
     * @var array<string, string|object|array<int, string|object>>
     */
    protected $observers = [
        FieldResponse::class => FieldResponseObserver::class,
    ];
}
