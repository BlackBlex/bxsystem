<?php

declare(strict_types=1);

namespace Modules\CustomFields\Casts;

use Illuminate\Contracts\Database\Eloquent\CastsAttributes;
use Illuminate\Database\Eloquent\Model;
use Modules\CustomFields\Contracts\FieldStructure;
use Modules\CustomFields\Entities\Types\Invalid;
use Modules\CustomFields\Enums\FieldType;

/**
 * @template-implements CastsAttributes<FieldStructure, mixed>
 */
final class FieldTypeCast implements CastsAttributes
{
    /**
     * Transform the attribute from the underlying model values.
     *
     * @param  array<string, mixed>  $attributes
     */
    public function get(Model $model, string $key, mixed $value, array $attributes): FieldStructure
    {
        if ($value === '') {
            return new Invalid();
        }

        $dataB64 = explode('|', \Safe\base64_decode($value));
        $dataB64 = array_reverse($dataB64);
        $type = \Safe\base64_decode(array_pop($dataB64));
        $dataB64 = array_reverse($dataB64);
        $data = [];

        foreach ($dataB64 as $b64) {
            $data[] = \Safe\base64_decode($b64);
        }

        if ($type !== '') {
            $type = FieldType::tryFrom($type);

            if ($type !== null) {
                /** @var class-string<FieldStructure> $classNameFQN */
                $classNameFQN = '\\Modules\\CustomFields\\Entities\\Types\\' . ucfirst($type->value);

                $reflectionClass = new \ReflectionClass($classNameFQN);
                /** @var FieldStructure $instance */
                $instance = $reflectionClass->newInstance();

                if ($instance->deserialize($data)) {
                    return $instance;
                }
            }
        }

        return new Invalid();
    }

    /**
     * Transform the attribute to its underlying model values.
     *
     * @param  array<string, mixed>  $attributes
     */
    public function set(Model $model, string $key, mixed $value, array $attributes): string
    {
        if ($value === null) {
            return '';
        }

        if (! $value instanceof FieldStructure) {
            throw new \InvalidArgumentException();
        }

        if ($value instanceof Invalid) {
            return '';
        }

        return $value->serialize();
    }
}
