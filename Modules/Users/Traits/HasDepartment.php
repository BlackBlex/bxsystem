<?php

declare(strict_types=1);

namespace Modules\Users\Traits;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Modules\Users\Entities\Department;

trait HasDepartment
{
    public function chargeOfDepartment(): HasOne
    {
        return $this->hasOne(Department::class, 'boss_id', 'user_id');
    }

    public function department(): BelongsTo
    {
        return $this->belongsTo(Department::class, 'department_id');
    }

    protected function initializeHasDepartment(): void
    {
        $this->fillable[] = 'department_id';
    }
}
