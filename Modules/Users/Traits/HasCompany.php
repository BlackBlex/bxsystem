<?php

declare(strict_types=1);

namespace Modules\Users\Traits;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Modules\Users\Entities\Company;

trait HasCompany
{
    public function company(): BelongsTo
    {
        return $this->belongsTo(Company::class, 'company_id');
    }

    protected function initializeHasCompany(): void
    {
        $this->fillable[] = 'company_id';
    }
}
