<?php

declare(strict_types=1);

namespace Modules\Users\Traits;

use Illuminate\Database\Eloquent\Relations\HasOne;
use Modules\Users\Entities\Employee;

trait HasEmployee
{
    public function employee(): HasOne
    {
        return $this->hasOne(Employee::class);
    }
}
