<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('departments');
    }

    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('departments', function (Blueprint $table): void {
            $table->id();
            $table->string('name')->unique();
            $table->string('description');
            $table->foreignId('boss_id')->nullable()->constrained('users');
            $table->foreignId('parent_id')->nullable()->constrained('departments');
            $table->boolean('is_auxiliary')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });
    }
};
