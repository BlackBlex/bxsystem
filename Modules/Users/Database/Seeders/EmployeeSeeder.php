<?php

declare(strict_types=1);

namespace Modules\Users\Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Modules\Users\Entities\Employee;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        /** @var User $user */
        $user = User::findOrFail(1);
        Employee::factory()->for($user)->create();
    }
}
