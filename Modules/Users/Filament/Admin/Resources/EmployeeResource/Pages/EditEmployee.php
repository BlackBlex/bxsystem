<?php

declare(strict_types=1);

namespace Modules\Users\Filament\Admin\Resources\EmployeeResource\Pages;

use Filament\Actions;
use Filament\Forms\Components\Tabs;
use Filament\Forms\Form;
use Filament\Resources\Pages\EditRecord;
use Illuminate\Database\Eloquent\Model;
use Modules\CustomFields\Helper\CustomFieldHelper;
use Modules\Users\Filament\Admin\Resources\EmployeeResource;

class EditEmployee extends EditRecord
{
    protected static string $resource = EmployeeResource::class;

    public function afterSave(): void
    {
        $id = -1;

        /** @var Model $currentModel */
        $currentModel = $this->getRecord();

        if ($this->record instanceof Model) {
            $id = $this->record->getKey();
        } else {
            $id = (int) $this->record;
        }

        CustomFieldHelper::handleRequest($this->data ?? [], $currentModel);
    }

    public function form(Form $form): Form
    {
        $parentSchema = $form->getComponents();
        $parentContainer = reset($parentSchema);

        if ($parentContainer instanceof Tabs) {
            /** @var array<\Filament\Forms\Components\Tabs\Tab> $components */
            $components = $parentContainer->getChildComponents();

            $id = -1;

            if ($this->record instanceof Model) {
                $id = $this->record->getKey();
            } else {
                $id = (int) $this->record;
            }
            $components = array_merge($components, CustomFieldHelper::buildForm($this->getModel(), $id));

            $parentContainer->tabs($components);
        }

        return $form->schema($parentSchema);
    }

    protected function getHeaderActions(): array
    {
        return [
            Actions\ViewAction::make(),
            Actions\DeleteAction::make(),
            Actions\ForceDeleteAction::make(),
            Actions\RestoreAction::make(),
        ];
    }
}
