<?php

declare(strict_types=1);

namespace Modules\Users\Filament\Admin\Resources\EmployeeResource\Pages;

use Filament\Forms\Components\Tabs;
use Filament\Forms\Form;
use Filament\Resources\Pages\CreateRecord;
use Illuminate\Database\Eloquent\Model;
use Modules\CustomFields\Helper\CustomFieldHelper;
use Modules\Users\Filament\Admin\Resources\EmployeeResource;

class CreateEmployee extends CreateRecord
{
    protected static string $resource = EmployeeResource::class;

    public function afterCreate(): void
    {
        $id = -1;

        if ($this->record instanceof Model) {
            $id = $this->record->getKey();
        } else {
            $id = (int) $this->record;
        }

        CustomFieldHelper::handleRequest($this->data ?? [], $this->record);
    }

    public function form(Form $form): Form
    {
        $parentSchema = $form->getComponents();
        $parentContainer = reset($parentSchema);

        if ($parentContainer instanceof Tabs) {
            /** @var array<\Filament\Forms\Components\Tabs\Tab> $components */
            $components = $parentContainer->getChildComponents();

            $id = -1;

            if ($this->record instanceof Model) {
                $id = $this->record->getKey();
            } else {
                $id = (int) $this->record;
            }
            $components = array_merge($components, CustomFieldHelper::buildForm($this->getModel(), $id));

            $parentContainer->tabs($components);
        }

        return $form->schema($parentSchema);
    }
}
