<?php

declare(strict_types=1);

namespace Modules\Users\Filament\Admin\Resources\CompanyResource\Pages;

use Filament\Resources\Pages\CreateRecord;
use Modules\Users\Filament\Admin\Resources\CompanyResource;

class CreateCompany extends CreateRecord
{
    protected static string $resource = CompanyResource::class;
}
