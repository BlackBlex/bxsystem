<?php

declare(strict_types=1);

namespace Modules\Users\Filament\Admin\Resources\CompanyResource\RelationManagers;

use BezhanSalleh\FilamentShield\Support\Utils;
use Filament\Forms\Form;
use Filament\Resources\RelationManagers\RelationManager;
use Filament\Tables;
use Filament\Tables\Actions\Action;
use Filament\Tables\Actions\ActionGroup;
use Filament\Tables\Actions\BulkAction;
use Filament\Tables\Contracts\HasTable;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use Modules\Users\Entities\Company;
use Modules\Users\Entities\Employee;
use Modules\Users\Filament\Admin\Resources\EmployeeResource;

class EmployeesRelationManager extends RelationManager
{
    protected static string $relationship = 'employees';

    public function form(Form $form): Form
    {
        return EmployeeResource::form($form);
    }

    public function table(Table $table): Table
    {
        return EmployeeResource::table($table)
            ->recordTitleAttribute('user.full_name')
            ->heading(__('employees'))
            ->recordTitle(fn (Employee $record): string => "{$record->user->full_name}")
            ->filters([
                Tables\Filters\TrashedFilter::make(),
            ])
            ->headerActions([
                Tables\Actions\CreateAction::make()
                    ->action(function ($data, Action $action, Form $form, HasTable $livewire) use ($table): void {
                        $model = $table->getModel();

                        /** @var Employee $record */
                        $record = new $model();
                        $record->fill($data);

                        /** @var Company $company */
                        $company = $this->getOwnerRecord();
                        $company->associateModel($record);

                        $form->model($record)->saveRelationships();
                        $livewire->mountedTableActionRecord($record->getKey());

                        $action->success();
                    })
                    ->createAnother(false),
                Tables\Actions\AssociateAction::make()
                    ->associateAnother(false)
                    ->preloadRecordSelect()
                    ->action(function ($data, Action $action) use ($table): void {
                        $relationship = Relation::noConstraints(fn () => $table->getRelationship());
                        $record = $relationship->getQuery()->find($data['recordId']);

                        /** @var Company $company */
                        $company = $this->getOwnerRecord();
                        $company->associateModel($record);

                        $action->success();
                    })
                    ->recordSelectOptionsQuery(fn (Builder $query) => $query
                        ->whereDoesntHave('company')
                        ->whereHas('user', fn (Builder $subQuery): Builder => $subQuery
                            ->whereDoesntHave('roles', fn (Builder $subQueryTwo): Builder => $subQueryTwo
                                ->where('name', Utils::getSuperAdminName())))),
            ])
            ->actions([
                Tables\Actions\ViewAction::make(),
                Tables\Actions\EditAction::make(),
                ActionGroup::make([
                    Tables\Actions\DissociateAction::make()
                        ->action(function ($record, Action $action): void {
                            /** @var Company $company */
                            $company = $this->getOwnerRecord();
                            $company->dissociateModel($record);
                            $action->success();
                        }),
                    Tables\Actions\DeleteAction::make(),
                    Tables\Actions\ForceDeleteAction::make(),
                    Tables\Actions\RestoreAction::make(),
                ]),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DissociateBulkAction::make()
                        ->action(function ($records, BulkAction $action): void {
                            /** @var Company $company */
                            $company = $this->getOwnerRecord();

                            $records->each(function ($record) use ($company): void {
                                $company->dissociateModel($record);
                            });

                            $action->success();
                        }),
                    Tables\Actions\DeleteBulkAction::make(),
                    Tables\Actions\RestoreBulkAction::make(),
                    Tables\Actions\ForceDeleteBulkAction::make(),
                ]),
            ])
            ->modifyQueryUsing(fn (Builder $query) => $query->withoutGlobalScopes([
                SoftDeletingScope::class,
            ]))
            ->inverseRelationship('company');
    }
}
