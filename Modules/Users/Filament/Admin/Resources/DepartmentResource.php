<?php

declare(strict_types=1);

namespace Modules\Users\Filament\Admin\Resources;

use Filament\Forms;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use Modules\Users\Entities\Department;
use Modules\Users\Entities\Employee;
use Modules\Users\Filament\Admin\Resources\DepartmentResource\Pages;

class DepartmentResource extends Resource
{
    protected static bool $isEditingBoss = false;

    protected static ?string $model = Department::class;

    protected static ?string $navigationIcon = 'heroicon-o-academic-cap';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\Select::make('boss_id')
                    ->label(__('boss'))
                    ->native(false)
                    ->options(
                        fn (?string $state) => Employee::query()
                            ->doesntHave('chargeOfDepartment')
                            ->orWhereHas('chargeOfDepartment', fn (Builder $query): Builder => $query->where('boss_id', $state))
                            ->with('user')
                            ->get()
                            ->pluck('user.full_name', 'user.id')
                            ->toArray()
                    )
                    ->searchable(),
                Forms\Components\Select::make('parent_id')
                    ->label(__('department') . ' ' . __('parent'))
                    ->options(Department::pluck('name', 'id')->toArray())
                    ->reactive()
                    ->preload(),
                Forms\Components\TextInput::make('name')
                    ->label(__('name'))
                    ->required()
                    ->maxLength(255),
                Forms\Components\TextInput::make('description')
                    ->label(__('description'))
                    ->required()
                    ->maxLength(255),
                Forms\Components\Checkbox::make('is_auxiliary')
                    ->label(__('is_auxiliary')),
            ]);
    }

    public static function getEloquentQuery(): Builder
    {
        return parent::getEloquentQuery()
            ->withoutGlobalScopes([
                SoftDeletingScope::class,
            ]);
    }

    public static function getModelLabel(): string
    {
        return __('department');
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListDepartments::route('/'),
            'create' => Pages\CreateDepartment::route('/create'),
            'view' => Pages\ViewDepartment::route('/{record}'),
            'edit' => Pages\EditDepartment::route('/{record}/edit'),
        ];
    }

    public static function getPluralModelLabel(): string
    {
        return __('departments');
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('boss.user.full_name')
                    ->label(__('boss'))
                    ->sortable(),
                Tables\Columns\TextColumn::make('parent.name')
                    ->label(__('department') . ' ' . __('parent'))
                    ->sortable(),
                Tables\Columns\TextColumn::make('name')
                    ->label(__('name'))
                    ->searchable(),
                Tables\Columns\TextColumn::make('description')
                    ->label(__('description'))
                    ->searchable(),
                Tables\Columns\TextColumn::make('created_at')
                    ->label(__('created_at'))
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('updated_at')
                    ->label(__('updated_at'))
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('deleted_at')
                    ->label(__('deleted_at'))
                    ->dateTime()
                    ->sortable(),
            ])
            ->filters([
                Tables\Filters\TrashedFilter::make(),
            ])
            ->actions([
                Tables\Actions\ViewAction::make(),
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                    Tables\Actions\ForceDeleteBulkAction::make(),
                    Tables\Actions\RestoreBulkAction::make(),
                ]),
            ])
            ->emptyStateActions([
                Tables\Actions\CreateAction::make(),
            ]);
    }
}
