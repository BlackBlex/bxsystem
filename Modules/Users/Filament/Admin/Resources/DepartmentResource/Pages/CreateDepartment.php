<?php

declare(strict_types=1);

namespace Modules\Users\Filament\Admin\Resources\DepartmentResource\Pages;

use Filament\Resources\Pages\CreateRecord;
use Modules\Users\Filament\Admin\Resources\DepartmentResource;

class CreateDepartment extends CreateRecord
{
    protected static bool $canCreateAnother = false;

    protected static string $resource = DepartmentResource::class;
}
