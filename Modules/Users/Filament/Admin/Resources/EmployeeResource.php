<?php

declare(strict_types=1);

namespace Modules\Users\Filament\Admin\Resources;

use Filament\Forms;
use Filament\Forms\Components\Section;
use Filament\Forms\Components\Tabs;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use Illuminate\Support\Facades\Hash;
use Modules\CustomFields\Helper\CustomFieldHelper;
use Modules\Users\Entities\Company;
use Modules\Users\Entities\Department;
use Modules\Users\Entities\Employee;
use Modules\Users\Filament\Admin\Resources\CompanyResource\RelationManagers\EmployeesRelationManager;
use Modules\Users\Filament\Admin\Resources\EmployeeResource\Pages;

class EmployeeResource extends Resource
{
    protected static ?string $model = Employee::class;

    protected static ?string $navigationIcon = 'heroicon-o-identification';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Tabs::make('')
                    ->tabs([
                        Tabs\Tab::make(__('basic.information'))
                            ->icon('heroicon-o-information-circle')
                            ->schema([
                                Section::make('')
                                    ->hiddenLabel()
                                    ->relationship('user')
                                    ->schema([
                                        Forms\Components\TextInput::make('first_name')
                                            ->label(__('first_name'))
                                            ->required()
                                            ->maxLength(255),
                                        Forms\Components\TextInput::make('last_name')
                                            ->label(__('last_name'))
                                            ->required()
                                            ->maxLength(255),
                                        Forms\Components\TextInput::make('email')
                                            ->label(__('filament-panels::pages/auth/login.form.email.label'))
                                            ->required()
                                            ->email()
                                            ->maxLength(255)
                                            ->columnSpanFull(),
                                        Forms\Components\TextInput::make('password')
                                            ->label(__('filament-panels::pages/auth/login.form.password.label'))
                                            ->password()
                                            ->minLength(8)
                                            ->maxLength(255)
                                            ->same('password_confirmation')
                                            ->dehydrateStateUsing(fn (string $state): string => Hash::make($state))
                                            ->dehydrated(fn (?string $state): bool => filled($state))
                                            ->required(fn (string $operation): bool => $operation === 'create'),
                                        Forms\Components\TextInput::make('password_confirmation')
                                            ->label(__('filament-panels::pages/auth/register.form.password_confirmation.label'))
                                            ->password()
                                            ->nullable()
                                            ->minLength(8)
                                            ->maxLength(255),
                                        Forms\Components\Select::make('roles')
                                            ->relationship('roles', 'name')
                                            ->multiple()
                                            ->preload()
                                            ->searchable(),
                                    ])
                                    ->columns(2),
                                Section::make('')
                                    ->hiddenLabel()
                                    ->schema([
                                        Forms\Components\TextInput::make('identifier')
                                            ->label(__('identifier'))
                                            ->required()
                                            ->numeric()
                                            ->minLength(11)
                                            ->maxLength(15),
                                        Forms\Components\Select::make('company_id')
                                            ->saveRelationshipsUsing(function (Employee $record, ?string $state): void {
                                                $company = Company::whereId($state)->first();

                                                if ($company !== null) {
                                                    $company->associateModel($record);
                                                }
                                            })
                                            ->options(Company::pluck('name', 'id')->toArray())
                                            ->preload()
                                            ->hiddenOn(EmployeesRelationManager::class)
                                            ->searchable(),
                                        Forms\Components\Select::make('department_id')
                                            ->options(Department::pluck('name', 'id')->toArray())
                                            ->preload(),
                                    ])
                                    ->columns(2),
                            ]),
                    ])
                    ->columnSpanFull()
                    ->persistTabInQueryString(),
            ]);
    }

    public static function getEloquentQuery(): Builder
    {
        return parent::getEloquentQuery()
            ->withoutGlobalScopes([
                SoftDeletingScope::class,
            ]);
    }

    public static function getModelLabel(): string
    {
        return __('employee');
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListEmployees::route('/'),
            'create' => Pages\CreateEmployee::route('/create'),
            'view' => Pages\ViewEmployee::route('/{record}'),
            'edit' => Pages\EditEmployee::route('/{record}/edit'),
        ];
    }

    public static function getPluralModelLabel(): string
    {
        return __('employees');
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function table(Table $table): Table
    {
        $columns = [
            Tables\Columns\TextColumn::make('user.full_name')
                ->label(__('name'))
                ->searchable(['first_name', 'last_name'])
                ->sortable(),
            Tables\Columns\TextColumn::make('user.email')
                ->label(__('filament-panels::pages/auth/login.form.email.label'))
                ->searchable()
                ->sortable(),
            Tables\Columns\TextColumn::make('identifier')
                ->label(__('identifier'))
                ->searchable(),
            Tables\Columns\TextColumn::make('user.roles.name')
                ->label('Rol')
                ->searchable(),
            Tables\Columns\TextColumn::make('created_at')
                ->label(__('created_at'))
                ->dateTime()
                ->sortable()
                ->toggleable(isToggledHiddenByDefault: true),
            Tables\Columns\TextColumn::make('updated_at')
                ->label(__('updated_at'))
                ->dateTime()
                ->sortable()
                ->toggleable(isToggledHiddenByDefault: true),
            Tables\Columns\TextColumn::make('deleted_at')
                ->label(__('deleted_at'))
                ->dateTime()
                ->sortable()
                ->toggleable(isToggledHiddenByDefault: true),
        ];

        $columns = array_merge($columns, CustomFieldHelper::buildColumns(static::getModel()));

        return $table
            ->columns($columns)
            ->filters([
                Tables\Filters\TrashedFilter::make(),
            ])
            ->actions([
                Tables\Actions\ViewAction::make(),
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                    Tables\Actions\ForceDeleteBulkAction::make(),
                    Tables\Actions\RestoreBulkAction::make(),
                ]),
            ]);
    }
}
