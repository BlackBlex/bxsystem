<?php

declare(strict_types=1);

namespace Modules\Users\Filament\Pages;

use App\Models\User;
use Filament\Notifications\Notification;
use Filament\Pages\Dashboard as BaseDashboard;
use Illuminate\Contracts\Support\Htmlable;
use Illuminate\Support\Facades\Auth;

class Dashboard extends BaseDashboard
{
    public function getTitle(): string|Htmlable
    {
        $title = static::$title;

        if (Auth::check()) {
            /** @var User $user */
            $user = Auth::user();

            $company = $user->employee->company;
            $companyName = null;

            if ($company !== null) {
                $companyName = $company->name;
            }

            if ($companyName !== null) {
                $title = "{$companyName}'s ";
            } else {
                $title = '';
            }
        }

        $title .= __('filament-panels::pages/dashboard.title');

        return $title;
    }

    public function mount(): void
    {
        if (request()->session()->has('error')) {
            $error = request()->session()->get('error');
            Notification::make()
                ->title($error['text'])
                ->icon($error['icon'])
                ->danger()
                ->send();
        }
    }
}
