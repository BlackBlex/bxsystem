<?php

declare(strict_types=1);

return [
    'name' => 'Users',
    'panel_title' => [
        'admin' => 'users',
    ],
];
