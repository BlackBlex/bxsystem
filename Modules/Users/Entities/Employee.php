<?php

declare(strict_types=1);

namespace Modules\Users\Entities;

use App\Models\User;
use Filament\Models\Contracts\HasName;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\CustomFields\Traits\HasCustomFields;
use Modules\Users\Database\Factories\EmployeeFactory;
use Modules\Users\Traits\HasCompany;
use Modules\Users\Traits\HasDepartment;

class Employee extends Model implements HasName
{
    use HasCompany;
    use HasCustomFields;
    use HasDepartment;
    use HasFactory;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<string>
     */
    protected $fillable = ['identifier'];

    public function getFilamentName(): string
    {
        return $this->user->full_name;
    }

    public function isSuperAdmin(): bool
    {
        return $this->user->isSuperAdmin();
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    protected static function newFactory(): EmployeeFactory
    {
        return EmployeeFactory::new();
    }
}
