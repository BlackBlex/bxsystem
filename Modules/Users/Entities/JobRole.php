<?php

declare(strict_types=1);

namespace Modules\Users\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Modules\Users\Database\Factories\JobRoleFactory;

class JobRole extends Model
{
    use HasFactory;

    protected $fillable = [];

    protected static function newFactory(): JobRoleFactory
    {
        return \Modules\Users\Database\Factories\JobRoleFactory::new();
    }
}
