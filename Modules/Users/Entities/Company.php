<?php

declare(strict_types=1);

namespace Modules\Users\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\FileShare\Entities\Folder;
use Modules\FileShare\Entities\Scopes\HasFolderScope;
use Modules\Users\Database\Factories\CompanyFactory;

class Company extends Model
{
    use HasFactory;
    use HasFolderScope;
    use SoftDeletes;

    protected $fillable = ['name', 'logo'];

    /**
     * @param  Model|array<Model>  $models
     */
    public function associateModel($models): void
    {
        if (! is_array($models)) {
            $models = [$models];
        }

        /** @var Folder $folder */
        $folder = $this->getFolderByName();
        $this->employees()->saveMany($models);

        if ($folder != null && $folder->createdBy === null) {
            $folder->createdBy()->associate(current($models));
            $folder->saveQuietly();
        }
    }

    /**
     * @param  Model|array<Model>  $models
     */
    public function dissociateModel($models): void
    {
        if (! is_array($models)) {
            $models = [$models];
        }

        /** @var Folder $folder */
        $folder = $this->getFolderByName();
        $hasCreatedBy = $folder->createdBy != null;

        foreach ($models as $model) {
            if (method_exists($model, 'company')) {
                $model->company()->dissociate();
            }

            $model->saveQuietly();

            if ($hasCreatedBy && $folder->createdBy->getKey() === $model->getKey()) {
                $folder->createdBy()->dissociate();
                $folder->saveQuietly();
                $hasCreatedBy = false;
            }
        }

        $this->load('employees');

        if ($folder->createdBy === null && $this->employees->count() > 0) {
            $folder->createdBy()->associate($this->employees->first());
            $folder->saveQuietly();
        }
    }

    public function employees(): HasMany
    {
        return $this->hasMany(Employee::class);
    }

    public function getNameOfModel(): string
    {
        return 'name';
    }

    protected static function newFactory(): CompanyFactory
    {
        return CompanyFactory::new();
    }
}
