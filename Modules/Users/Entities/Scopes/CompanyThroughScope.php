<?php

declare(strict_types=1);

namespace Modules\Users\Entities\Scopes;

use Illuminate\Database\Eloquent\Builder;
use Modules\Users\Entities\Company;

trait CompanyThroughScope
{
    public function scopeForCompany(Builder $query, string $through, Company $company): Builder
    {
        return $query->whereHas($through, fn (Builder $subQuery): Builder => $subQuery->where('company_id', $company->getKey()));
    }
}
