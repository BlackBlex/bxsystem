<?php

declare(strict_types=1);

namespace Modules\Users\Observers;

use Illuminate\Support\Facades\Storage;
use Modules\Users\Entities\Company;

class CompanyObserver
{
    /**
     * Handle the Company "created" event.
     */
    public function created(Company $company): void
    {
        if ($company->logo === null) {
            return;
        }

        $extension = $this->getExtension($company->logo);
        $newNameForLogo = $company->name . '.' . $extension;

        Storage::disk('logos')->move($company->logo, $newNameForLogo);

        $company->logo = $newNameForLogo;
        $company->saveQuietly();
    }

    /**
     * Handle the Company "force deleted" event.
     */
    public function forceDeleted(Company $company): void
    {
        if ($company->logo === null) {
            return;
        }

        $logoDisk = Storage::disk('logos');
        if (! $logoDisk->exists($company->logo)) {
            return;
        }

        $logoDisk->delete($company->logo);
    }

    // /**
    //  * Handle the Company "deleted" event.
    //  */
    // public function deleted(Company $company): void
    // {
    //     //
    // }

    /**
     * Handle the Company "updated" event.
     */
    public function updated(Company $company): void
    {
        if ($company->isDirty('logo') && $company->logo !== null) {
            $logoDisk = Storage::disk('logos');
            /** @var string $originalName */
            $originalName = $company->getOriginal('logo');

            if ($originalName !== '' && $logoDisk->exists($originalName)) {
                $logoDisk->delete($originalName);
            }

            $extension = $this->getExtension($company->logo);
            $newNameForLogo = $company->name . '.' . $extension;

            $logoDisk->move($company->logo, $newNameForLogo);

            $company->logo = $newNameForLogo;
            $company->saveQuietly();
        }
    }

    private function getExtension(string $filepath): string
    {
        return pathinfo($filepath, PATHINFO_EXTENSION);
    }

    // /**
    //  * Handle the Company "restored" event.
    //  */
    // public function restored(Company $company): void
    // {
    //     //
    // }
}
