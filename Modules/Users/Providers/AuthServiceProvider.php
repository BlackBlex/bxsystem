<?php

declare(strict_types=1);

namespace Modules\Users\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Modules\Users\Entities\Company;
use Modules\Users\Entities\Department;
use Modules\Users\Entities\Employee;
use Modules\Users\Policies\CompanyPolicy;
use Modules\Users\Policies\DepartmentPolicy;
use Modules\Users\Policies\EmployeePolicy;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The model to policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        Company::class => CompanyPolicy::class,
        Department::class => DepartmentPolicy::class,
        Employee::class => EmployeePolicy::class,
    ];

    /**
     * Register any application authentication / authorization services.
     */
    public function boot(): void
    {
        $this->registerPolicies();
    }
}
