<?php

declare(strict_types=1);

namespace Modules\Users\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Modules\Users\Entities\Company;
use Modules\Users\Observers\CompanyObserver;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The model observers to register.
     *
     * @var array<string, string|object|array<int, string|object>>
     */
    protected $observers = [
        Company::class => CompanyObserver::class,
    ];
}
