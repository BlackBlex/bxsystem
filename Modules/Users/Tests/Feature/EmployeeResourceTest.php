<?php

declare(strict_types=1);

use App\Models\Permission;
use App\Models\Role;
use Filament\Actions\DeleteAction;
use Filament\Facades\Filament;
use Modules\CustomFields\Entities\Field;
use Modules\CustomFields\Entities\Fieldset;
use Modules\CustomFields\Entities\Pivot\FieldFieldset;
use Modules\CustomFields\Entities\Pivot\FieldsetModel;
use Modules\CustomFields\Entities\Types\Text;
use Modules\CustomFields\Helper\CustomFieldHelper;
use Modules\Shared\Tests\TestCase;
use Modules\Users\Entities\Employee;
use Modules\Users\Filament\Admin\Resources\EmployeeResource;
use Modules\Users\Filament\Admin\Resources\EmployeeResource\Pages\CreateEmployee;
use Modules\Users\Filament\Admin\Resources\EmployeeResource\Pages\EditEmployee;
use Modules\Users\Filament\Admin\Resources\EmployeeResource\Pages\ListEmployees;
use Modules\Users\Filament\Admin\Resources\EmployeeResource\Pages\ViewEmployee;

use function Pest\Livewire\livewire;

uses(Tests\Traits\RefreshTestDatabase::class);
uses(TestCase::class);

function createHRroleEmployee(): void
{
    $employeePermissions = ['view_employee', 'view_any_employee', 'create_employee', 'update_employee', 'restore_employee', 'restore_any_employee', 'replicate_employee', 'reorder_employee', 'delete_employee', 'delete_any_employee', 'force_delete_employee', 'force_delete_any_employee'];

    $hrRole = Role::findOrCreate('HR', 'web');

    if (! Permission::whereName($employeePermissions[0])->exists()) {
        foreach ($employeePermissions as $permission) {
            Permission::findOrCreate($permission, 'web');
        }
    }

    if (! $hrRole->hasAllPermissions($employeePermissions)) {
        $hrRole->givePermissionTo($employeePermissions);
    }
}

function assignTestFieldToEmployee($required = false): Field
{
    $field = Field::factory()->create(['type' => new Text(), 'title' => 'Dummy', 'hint' => 'Dummy fit message']);
    $fieldset = Fieldset::factory()->create(['name' => 'Dummyset']);

    $fieldsetModel = FieldsetModel::make(['model_type' => Employee::class]);
    $fieldsetModel->fieldset()->associate($fieldset);
    $fieldsetModel->save();

    $fieldFieldset = new FieldFieldset(['required' => $required]);
    $fieldFieldset->field()->associate($field);
    $fieldFieldset->fieldset()->associate($fieldset);

    $fieldset->fieldFieldsets()->save($fieldFieldset);

    return $field;
}

beforeEach(function (): void {
    Filament::setCurrentPanel(Filament::getPanel('users::admin'));

    createHRroleEmployee();

    $this->user = $this->createUser('first_name', 'Jovani', data: ['first_name' => 'Jovani']);
    $this->actingAs($this->user);

    if (! $this->user->hasRole('HR')) {
        $this->user->assignRole('HR');
    }
});

it('can render index page', function (): void {
    $this->get(EmployeeResource::getUrl('index'))->assertSuccessful();
});

it('can list employee', function (): void {
    $employee = Employee::factory()->for($this->user)->create();

    livewire(ListEmployees::class)
        ->assertCanSeeTableRecords([$employee]);
});

it('can render create page', function (): void {
    $this->get(EmployeeResource::getUrl('create'))->assertSuccessful();
});

it('can create new employee', function (): void {
    $newEmployeeData = [
        'first_name' => 'Test',
        'last_name' => 'ing',
        'email' => 'testing@example.com',
        'password' => 'password',
        'password_confirmation' => 'password',
        'identifier' => '12343218912',
    ];

    $data = [
        'user' => [
            'first_name' => $newEmployeeData['first_name'],
            'last_name' => $newEmployeeData['last_name'],
            'email' => $newEmployeeData['email'],
            'password' => $newEmployeeData['password'],
            'password_confirmation' => $newEmployeeData['password_confirmation'],
        ],
        'identifier' => $newEmployeeData['identifier'],
    ];

    livewire(CreateEmployee::class)
        ->fillForm($data)
        ->call('create')
        ->assertHasNoFormErrors();

    $employee = Employee::whereIdentifier($data['identifier'])->first();

    expect($employee)->not()->toBeNull();
    expect($employee->user)->first_name->toBe($newEmployeeData['first_name'])
        ->last_name->toBe($newEmployeeData['last_name'])
        ->email->toBe($newEmployeeData['email']);
});

it('can create new employee with custom field', function (bool $required): void {
    $newEmployeeData = [
        'first_name' => 'Test',
        'last_name' => 'ing',
        'email' => 'testing@example.com',
        'password' => 'password',
        'password_confirmation' => 'password',
        'identifier' => '12343218912',
    ];

    $field = assignTestFieldToEmployee($required);

    $data = [
        'user' => [
            'first_name' => $newEmployeeData['first_name'],
            'last_name' => $newEmployeeData['last_name'],
            'email' => $newEmployeeData['email'],
            'password' => $newEmployeeData['password'],
            'password_confirmation' => $newEmployeeData['password_confirmation'],
        ],
        'identifier' => $newEmployeeData['identifier'],
    ];

    if ($required) {
        $data[CustomFieldHelper::getCustomFieldKey() . $field->getKey()] = 'TestField';
    }

    livewire(CreateEmployee::class)
        ->fillForm($data)
        ->call('create')
        ->assertHasNoFormErrors();

    $employee = Employee::whereIdentifier($data['identifier'])->first();

    expect($employee)->not()->toBeNull();
    expect($employee->user)->first_name->toBe($newEmployeeData['first_name'])
        ->last_name->toBe($newEmployeeData['last_name'])
        ->email->toBe($newEmployeeData['email']);

    if ($required) {
        expect($employee->responses)->toHaveCount(1);
        expect($employee->responses_flat())->toBe([$field->getKey() => 'TestField']);
    }
})->with([
    'non required' => [false],
    'required' => [true],
]);

it('can´t create new employee with empty data', function (): void {
    $data = [
        'user' => [
            'first_name' => null,
            'last_name' => null,
            'email' => null,
            'password' => null,
            'password_confirmation' => null,
        ],
        'identifier' => null,
    ];

    livewire(CreateEmployee::class)
        ->fillForm($data)
        ->call('create')
        ->assertHasFormErrors([
            'identifier' => 'required',
            'user.first_name' => 'required',
            'user.last_name' => 'required',
            'user.email' => 'required',
            'user.password' => 'required',
        ]);
});

it('can render edit page', function (): void {
    $employee = Employee::factory()->for($this->user)->create();

    $this->get(EmployeeResource::getUrl('edit', ['record' => $employee->getKey()]))->assertSuccessful();
});

it('can retrieve employee data from DB (edit)', function (): void {
    $employee = Employee::factory()->for($this->user)->create();

    livewire(EditEmployee::class, [
        'record' => $employee->getKey(),
    ])
        ->assertFormSet([
            'identifier' => $employee->identifier,
            'user.first_name' => $employee->user->first_name,
            'user.last_name' => $employee->user->last_name,
            'user.email' => $employee->user->email,
        ]);
});

it('can edit employee', function (): void {
    $employee = Employee::factory()->for($this->user)->create();

    $data = [
        'user' => [
            'first_name' => 'Other',
            'last_name' => 'Employee',
        ],
    ];

    livewire(EditEmployee::class, [
        'record' => $employee->getKey(),
    ])
        ->fillForm($data)
        ->call('save')
        ->assertHasNoFormErrors();

    expect($employee->refresh())
        ->user->first_name->toBe($data['user']['first_name'])
        ->user->last_name->toBe($data['user']['last_name']);
});

it('can´t edit employee with empty data', function (): void {
    $employee = Employee::factory()->for($this->user)->create();

    $data = [
        'user' => [
            'first_name' => null,
            'last_name' => null,
        ],
    ];

    livewire(EditEmployee::class, [
        'record' => $employee->getKey(),
    ])
        ->fillForm($data)
        ->call('save')
        ->assertHasFormErrors([
            'user.first_name' => 'required',
            'user.last_name' => 'required',
        ]);
});

it('can delete employee', function (): void {
    $employee = Employee::factory()->for($this->user)->create();

    livewire(EditEmployee::class, [
        'record' => $employee->getKey(),
    ])
        ->callAction(DeleteAction::class);

    $employee->refresh();

    expect($employee->trashed())->toBeTrue();
});

it('can´t delete employee (missing permission)', function (): void {
    $otherUser = $this->createUser('first_name', 'Viewer', data: ['first_name' => 'Viewer']);
    $this->actingAs($otherUser);

    if (! $otherUser->hasRole('HR')) {
        $otherUser->assignRole('HR');

        $role = Role::findByName('HR', 'web');
        $role->revokePermissionTo(['delete_employee', 'delete_any_employee', 'force_delete_employee', 'force_delete_any_employee']);
    }

    $employee = Employee::factory()->for($otherUser)->create();

    livewire(EditEmployee::class, [
        'record' => $employee->getKey(),
    ])
        ->assertActionHidden(DeleteAction::class);
});

it('can render view page', function (): void {
    $employee = Employee::factory()->for($this->user)->create();

    $this->get(EmployeeResource::getUrl('view', ['record' => $employee->getKey()]))->assertSuccessful();
});

it('can retrieve employee data from DB (view)', function (): void {
    $employee = Employee::factory()->for($this->user)->create();

    livewire(ViewEmployee::class, [
        'record' => $employee->getKey(),
    ])
        ->assertFormSet([
            'identifier' => $employee->identifier,
            'user.first_name' => $employee->user->first_name,
            'user.last_name' => $employee->user->last_name,
            'user.email' => $employee->user->email,
        ]);
});
