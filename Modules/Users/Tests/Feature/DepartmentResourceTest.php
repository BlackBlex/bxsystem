<?php

declare(strict_types=1);

use App\Models\Permission;
use App\Models\Role;
use Filament\Actions\DeleteAction;
use Filament\Facades\Filament;
use Modules\Shared\Tests\TestCase;
use Modules\Users\Entities\Department;
use Modules\Users\Filament\Admin\Resources\DepartmentResource;
use Modules\Users\Filament\Admin\Resources\DepartmentResource\Pages\CreateDepartment;
use Modules\Users\Filament\Admin\Resources\DepartmentResource\Pages\EditDepartment;
use Modules\Users\Filament\Admin\Resources\DepartmentResource\Pages\ListDepartments;
use Modules\Users\Filament\Admin\Resources\DepartmentResource\Pages\ViewDepartment;

use function Pest\Livewire\livewire;

uses(Tests\Traits\RefreshTestDatabase::class);
uses(TestCase::class);

function createHRroleDepartment(): void
{
    $departmentPermissions = ['view_department', 'view_any_department', 'create_department', 'update_department', 'restore_department', 'restore_any_department', 'replicate_department', 'reorder_department', 'delete_department', 'delete_any_department', 'force_delete_department', 'force_delete_any_department'];

    $hrRole = Role::findOrCreate('HR', 'web');

    if (! Permission::whereName($departmentPermissions[0])->exists()) {
        foreach ($departmentPermissions as $permission) {
            Permission::findOrCreate($permission, 'web');
        }
    }

    if (! $hrRole->hasAllPermissions($departmentPermissions)) {
        $hrRole->givePermissionTo($departmentPermissions);
    }
}

beforeEach(function (): void {
    Filament::setCurrentPanel(Filament::getPanel('users::admin'));

    createHRroleDepartment();

    $this->user = $this->createUser('first_name', 'Jovani', data: ['first_name' => 'Jovani']);
    $this->actingAs($this->user);

    if (! $this->user->hasRole('HR')) {
        $this->user->assignRole('HR');
    }
});

it('can render index page', function (): void {
    $this->get(DepartmentResource::getUrl('index'))->assertSuccessful();
});

it('can list department', function (): void {
    $department = Department::factory()->create();

    livewire(ListDepartments::class)
        ->assertCanSeeTableRecords([$department]);
});

it('can render create page', function (): void {
    $this->get(DepartmentResource::getUrl('create'))->assertSuccessful();
});

it('can create new department', function (): void {
    $data = [
        'name' => 'Test',
        'description' => 'Testing department',
        'is_auxiliary' => true,
    ];

    livewire(CreateDepartment::class)
        ->fillForm($data)
        ->call('create')
        ->assertHasNoFormErrors();

    $department = Department::whereName($data['name'])->whereDescription($data['description'])->whereIsAuxiliary($data['is_auxiliary'])->first();

    expect($department)->not()->toBeNull();
});

it('can´t create new department with empty data', function (): void {
    $data = [
        'name' => null,
        'description' => null,
    ];

    livewire(CreateDepartment::class)
        ->fillForm($data)
        ->call('create')
        ->assertHasFormErrors([
            'name' => 'required',
            'description' => 'required',
        ]);
});

it('can render edit page', function (): void {
    $department = Department::factory()->create();

    $this->get(DepartmentResource::getUrl('edit', ['record' => $department->getKey()]))->assertSuccessful();
});

it('can retrieve department data from DB (edit)', function (): void {
    $department = Department::factory()->create();

    livewire(EditDepartment::class, [
        'record' => $department->getKey(),
    ])
        ->assertFormSet([
            'name' => $department->name,
            'description' => $department->description,
            'is_auxiliary' => $department->is_auxiliary,
        ]);
});

it('can edit department', function (): void {
    $department = Department::factory()->create();

    $data = [
        'description' => 'Prueba department',
        'is_auxiliary' => false,
    ];

    livewire(EditDepartment::class, [
        'record' => $department->getKey(),
    ])
        ->fillForm($data)
        ->call('save')
        ->assertHasNoFormErrors();

    expect($department->refresh())
        ->description->toBe($data['description'])
        ->is_auxiliary->toBe($data['is_auxiliary']);
});

it('can´t edit department with empty data', function (): void {
    $department = Department::factory()->create();

    $data = [
        'description' => null,
        'is_auxiliary' => null,
    ];

    livewire(EditDepartment::class, [
        'record' => $department->getKey(),
    ])
        ->fillForm($data)
        ->call('save')
        ->assertHasFormErrors([
            'description' => 'required',
        ]);
});

it('can delete department', function (): void {
    $department = Department::factory()->create();

    livewire(EditDepartment::class, [
        'record' => $department->getKey(),
    ])
        ->callAction(DeleteAction::class);

    $department->refresh();

    expect($department->trashed())->toBeTrue();
});

it('can´t delete department (missing permission)', function (): void {
    $otherUser = $this->createUser('first_name', 'Viewer', data: ['first_name' => 'Viewer']);
    $this->actingAs($otherUser);

    if (! $otherUser->hasRole('HR')) {
        $otherUser->assignRole('HR');

        $role = Role::findByName('HR', 'web');
        $role->revokePermissionTo(['delete_department', 'delete_any_department', 'force_delete_department', 'force_delete_any_department']);
    }

    $department = Department::factory()->create();

    livewire(EditDepartment::class, [
        'record' => $department->getKey(),
    ])
        ->assertActionHidden(DeleteAction::class);
});

it('can render view page', function (): void {
    $department = Department::factory()->create();

    $this->get(DepartmentResource::getUrl('view', ['record' => $department->getKey()]))->assertSuccessful();
});

it('can retrieve department data from DB (view)', function (): void {
    $department = Department::factory()->create();

    livewire(ViewDepartment::class, [
        'record' => $department->getKey(),
    ])
        ->assertFormSet([
            'name' => $department->name,
            'description' => $department->description,
            'is_auxiliary' => $department->is_auxiliary,
        ]);
});
