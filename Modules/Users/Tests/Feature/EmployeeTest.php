<?php

declare(strict_types=1);

use App\Models\User;
use Illuminate\Database\QueryException;
use Modules\Users\Entities\Employee;

uses(Tests\Traits\RefreshTestDatabase::class);
uses(Tests\TestCase::class);

test('can create a employee', function (): void {
    $user = User::factory()->create();
    $employee = Employee::factory()->for($user)->create();

    expect($employee->id)->not()->toBeNull();
});

test('can´t create a employee without identifier [Throw: QueryException]', function (): void {
    $_employee = Employee::factory()->withoutIdentifier()->create();
})->throws(QueryException::class);
