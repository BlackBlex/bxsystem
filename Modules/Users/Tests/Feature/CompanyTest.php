<?php

declare(strict_types=1);

use App\Models\User;
// use Illuminate\Database\QueryException;
use Illuminate\Http\UploadedFile;
use Modules\Users\Entities\Company;
use Modules\Users\Entities\Employee;

uses(Tests\Traits\RefreshTestDatabase::class);
uses(Tests\TestCase::class);

beforeEach(function (): void {
    Storage::fake('logos');

    $this->logo = UploadedFile::fake()->image('logo_test.jpg')->storeAs('', 'logo_test.jpg', ['disk' => 'logos']);

    $this->company = Company::factory()->create(['name' => 'Test company', 'logo' => $this->logo]);
});

afterEach(function (): void {
    Storage::disk('logos')->delete('Test company.jpg');
});

test('can create a company', function (): void {
    expect($this->company->id)->not()->toBeNull();
    Storage::disk('logos')->assertExists('Test company.jpg');
    expect($this->company->getFolderByName())->not->toBeNull();
});

test('can create a company and associte a employee', function (): void {
    $user = User::factory()->create();
    $employee = Employee::factory()->for($user)->create();
    $folder = $this->company->getFolderByName();

    $this->company->associateModel($employee);

    $folder->refresh();

    expect($folder)->not->toBeNull();
    expect($folder->createdBy->getKey())->toBe($employee->getKey());
});

test('can create a company and associte a two employees', function (): void {
    $user = User::factory()->create();
    $employee = Employee::factory()->for($user)->create();
    $userTwo = User::factory()->create();
    $employeeTwo = Employee::factory()->for($userTwo)->create();

    $folder = $this->company->getFolderByName();

    $this->company->associateModel([$employee, $employeeTwo]);

    $folder->refresh();

    expect($folder)->not->toBeNull();
    expect($folder->createdBy->getKey())->toBe($employee->getKey());
    expect($folder->createdBy->getKey())->not()->toBe($employeeTwo->getKey());
});

// test('can´t create a employee without identifier [Throw: QueryException]', function (): void {
//     $_employee = Employee::factory()->withoutIdentifier()->create();
// })->throws(QueryException::class);
