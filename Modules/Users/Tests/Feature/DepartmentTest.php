<?php

declare(strict_types=1);

use App\Models\User;
use Illuminate\Database\QueryException;
use Modules\Users\Entities\Department;
use Modules\Users\Entities\Employee;

uses(Tests\Traits\RefreshTestDatabase::class);
uses(Tests\TestCase::class);

test('can create a department', function (): void {
    $user = User::factory()->create();
    $department = Department::factory()->create();
    $department->boss()->associate($user);
    $department->save();

    expect($department->id)->not()->toBeNull();
    expect($department->boss_id)->not()->toBeNull();
    expect($department->boss_id)->toBe($user->id);
});

test('can´t create a department without name and description [Throw: QueryException]', function (): void {
    $_department = Department::factory()->create(['name' => null, 'description' => null]);
})->throws(QueryException::class);

//TODO: Check this, its possible assign user to other department when already have one a department
test('can´t assign a department to a user that have other department', function (): void {
    $user = User::factory()->create();
    $employee = Employee::factory()->for($user)->create();
    $department = Department::factory()->create();
    $departmentOther = Department::factory()->create();
    $department->boss()->associate($employee->user);
    $department->save();

    $availableUsersWithoutDepartment = Employee::doesntHave('chargeOfDepartment')->get();

    $departmentOther->boss()->associate($employee->user);

    expect($department->id)->not()->toBeNull();
    expect($department->boss_id)->not()->toBeNull();
    expect($department->boss_id)->toBe($user->id);
    expect($availableUsersWithoutDepartment)->toHaveCount(0);
});
