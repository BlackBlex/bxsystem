<?php

declare(strict_types=1);

use function Illuminate\Filesystem\join_paths;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Modules\FileShare\Entities\File;
use Modules\FileShare\Helper\FileUtils;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('files/view/{code}', function (string $code) {
    /** @var \Illuminate\Filesystem\FilesystemAdapter $fileStorage */
    $fileStorage = Storage::disk('files');
    $file = File::whereCode($code)->first();

    if ($file !== null) {
        $path = join_paths(FileUtils::getFolderPathComplete($file->folder), $file->filename);

        if ($fileStorage->exists($path)) {
            if (Request::get('action') == 'view') {
                return $fileStorage->response($path);
            } else {
                $file->downloads++;
                $file->saveQuietly();

                return $fileStorage->download($path);
            }
        }
    }

    return response()->json(['message' => 'Not Found!'], 404);
})->name('file.view')->where('code', '(.*)?')->middleware(['web', 'signed']);
