<?php

declare(strict_types=1);

namespace Modules\FileShare\Enums;

use Filament\Support\Contracts\HasLabel;

enum FileStatus: string implements HasLabel
{
    //Authorized means: the client accept the file
    case Authorized = 'authorized';
    //Initial means: when the user upload the file is the default status
    case Initial = 'initial';
    //Reviewed means: when the client open file and press review button
    case Reviewed = 'reviewed';
    //ReviewedNeed means: when the client reviewed the file and leave a comment the file needs changes
    case ReviewedNeed = 'reviewed_need';
    //Signed means: that the client agree with the file but he need another approval
    case Signed = 'signed';
    //Updated means: when the employee upload a revision of file
    case Updated = 'updated';

    /**
     * Get all values from FileStatus
     *
     * @return array<string>
     */
    public static function values(): array
    {
        return array_column(self::cases(), 'value');
    }

    public function getLabel(): string
    {
        return __('status.' . $this->value);
    }
}
