<?php

declare(strict_types=1);

return [
    'name' => 'FileShare',
    'panel_title' => [
        'admin' => 'file_share',
    ],
];
