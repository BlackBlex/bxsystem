<?php

declare(strict_types=1);

namespace Modules\FileShare\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Modules\FileShare\Entities\Folder;

/**
 * @extends Factory<Folder>
 */
class FolderFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var class-string<Folder>
     */
    protected $model = Folder::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [];
    }
}
