<?php

declare(strict_types=1);

namespace Modules\FileShare\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Modules\FileShare\Entities\ZoneSign;

/**
 * @extends Factory<zoneSign>
 */
class ZoneSignFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var class-string<ZoneSign>
     */
    protected $model = ZoneSign::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'empty' => '',
        ];
    }
}
