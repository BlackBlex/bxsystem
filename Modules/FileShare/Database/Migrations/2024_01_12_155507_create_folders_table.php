<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('folders');
    }

    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('folders', function (Blueprint $table): void {
            $table->id();
            $table->string('name');
            $table->text('description')->default('');
            $table->foreignId('created_by')->nullable()->constrained('employees');
            $table->foreignId('parent_id')->nullable()->constrained('folders');
            $table->timestamps();
            $table->softDeletes();

            $table->unique(['name', 'parent_id']);
        });
    }
};
