<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('zone_signs');
    }

    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('zone_signs', function (Blueprint $table): void {
            $table->id();
            $table->string('name');
            $table->json('coordinate');
            $table->json('size');
            $table->tinyInteger('page');
            $table->timestamps();
            $table->softDeletes();
        });
    }
};
