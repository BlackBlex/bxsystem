<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Modules\FileShare\Enums\FileStatus;

return new class extends Migration
{
    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('files');
    }

    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('files', function (Blueprint $table): void {
            $table->id();
            $table->string('code', 12)->unique();
            $table->foreignId('folder_id')->nullable()->constrained('folders');
            $table->foreignId('uploaded_by')->nullable()->constrained('employees');
            $table->text('filename')->unique();
            // $table->text('description')->default('');
            $table->integer('downloads')->default(0);
            $table->string('status')->default(FileStatus::Initial);
            $table->timestamps();
            $table->softDeletes();
        });
    }
};
