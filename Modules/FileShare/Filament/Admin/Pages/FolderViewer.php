<?php

declare(strict_types=1);

namespace Modules\FileShare\Filament\Admin\Pages;

use BezhanSalleh\FilamentShield\Traits\HasPageShield;
use Filament\Actions\CreateAction;
use Filament\Actions\DeleteAction;
use Filament\Facades\Filament;
use Filament\Forms\Components;
use Filament\Navigation\NavigationItem;
use Filament\Notifications\Notification;
use Filament\Pages\Page;
use Filament\Widgets\WidgetConfiguration;
use Illuminate\Contracts\Support\Htmlable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Str;
use Livewire\Features\SupportRedirects\Redirector;
use Modules\FileShare\Entities\File;
use Modules\FileShare\Entities\Folder;
use Modules\FileShare\Filament\Admin\Widgets\ViewFiles;
use Modules\FileShare\Filament\Admin\Widgets\ViewFolders;
use Modules\FileShare\Helper\FileUtils;
use Modules\Users\Entities\Company;

class FolderViewer extends Page
{
    use HasPageShield;

    public static ?Company $currentCompany = null;

    public Folder $liveCurrentFolder;

    public static ?Folder $rootFolder = null;

    public static ?Folder $staticCurrentFolder = null;

    protected static ?string $navigationIcon = 'heroicon-m-folder';

    protected static ?int $navigationSort = 1;

    protected static ?string $slug = 'folder-viewer/{record?}';

    protected static string $view = 'modules.file-share.filament.admin.pages.folder-viewer';

    public static function canView(): bool
    {
        if (Filament::auth()->user()->isSuperAdmin()) {
            return false;
        }

        return Filament::auth()->user()->can(static::getPermissionName());
    }

    /**
     * @return array<NavigationItem>
     */
    public static function getNavigationItems(): array
    {
        $navigationItems = [];
        if (self::$staticCurrentFolder !== null && self::$staticCurrentFolder->parent !== null) {
            $urls = FileUtils::getFolderUrls(self::$staticCurrentFolder->parent, static::class);

            $backNavigationItem = NavigationItem::make(__('go_to') . ' ' . self::$staticCurrentFolder->parent->name)
                ->icon('heroicon-s-folder-minus')
                ->activeIcon(static::getActiveNavigationIcon())
                ->isActiveWhen(fn (): bool => request()->is($urls['flatten']))
                ->badge(static::getNavigationBadge(), color: static::getNavigationBadgeColor())
                ->url($urls['url']);

            $navigationItems[] = $backNavigationItem;
        }

        $navigationItems = array_merge($navigationItems, self::buildTreeNavigationItems(self::$staticCurrentFolder ?? self::$rootFolder));

        if (self::$staticCurrentFolder === null) {
            if (count($navigationItems) > 0) {
                $navigationItems[0]->sort(static::getNavigationSort());
            } else {
                return parent::getNavigationItems();
            }
        }

        return $navigationItems;
    }

    public static function getNavigationLabel(): string
    {
        return __('explorer');
    }

    /**
     * @return array<string, string>
     */
    public function getBreadcrumbs(): array
    {
        return FileUtils::getBreadcrumbsForFolder($this->liveCurrentFolder, static::class);
    }

    /**
     * @return int | string | array<string, int | string | null>
     */
    public function getHeaderWidgetsColumns(): int|string|array
    {
        return 1;
    }

    public function getHeading(): string|Htmlable
    {
        return self::getNavigationLabel();
    }

    /**
     * @return void|Redirector
     */
    public function mount(int|Folder|null $record = null)
    {
        if (self::$currentCompany === null) {
            self::$currentCompany = Filament::auth()->user()->employee->company;
        }

        self::$rootFolder = Folder::forCompany('createdBy', self::$currentCompany)->whereParentId(null)->first();

        if (self::$rootFolder === null) {
            /** @var Redirector $result */
            $result = redirect(url(Filament::getPanel('admin')->getPath()));

            return $result->with('error', ['icon' => 'forkawesome-folder', 'text' => __('folder_not_found')]);
        } else {
            if ($record instanceof Folder) {
                $this->liveCurrentFolder = $record;
            } elseif ($record !== null) {
                $folder = Folder::forCompany('createdBy', self::$currentCompany)->whereId($record)->first();

                if ($folder !== null) {
                    $this->liveCurrentFolder = $folder;
                } else {
                    /** @var Redirector $result */
                    $result = redirect(url(Filament::getPanel('admin')->getPath()));

                    return $result->with('error', ['icon' => 'forkawesome-folder', 'text' => __('folder_not_found')]);
                }
            } else {
                $this->liveCurrentFolder = self::$rootFolder;
            }

            self::$staticCurrentFolder = $this->liveCurrentFolder;
        }

        if (request()->session()->has('error')) {
            $error = request()->session()->get('error');
            Notification::make()
                ->title($error['text'])
                ->icon($error['icon'])
                ->danger()
                ->send();
        }
    }

    protected function getHeaderActions(): array
    {
        return [
            CreateAction::make('createFolder')
                ->authorize(Filament::auth()->user()->can('create_folder'))
                ->color('info')
                ->createAnother(false)
                ->label(__('create_folder'))
                ->model(Folder::class)
                ->modalHeading(__('create_folder'))
                ->form([
                    Components\TextInput::make('name')
                        ->label(__('name'))
                        ->required()
                        ->maxLength(255),
                    Components\Textarea::make('description')
                        ->label(__('description'))
                        ->required()
                        ->maxLength(65535)
                        ->columnSpanFull()
                        ->default(''),
                ])
                ->using(function (array $data): Folder {
                    /** @var Folder $result */
                    $result = new Folder();
                    $result->fill($data);
                    $result->parent()->associate($this->liveCurrentFolder);
                    $result->save();

                    return $result;
                })
                ->successRedirectUrl(fn (): string => FileUtils::getFolderUrls($this->liveCurrentFolder, static::class)['url']),
            CreateAction::make('uploadFile')
                ->authorize(Filament::auth()->user()->can('create_file'))
                ->color('success')
                ->createAnother(false)
                ->label(__('upload_file'))
                ->model(File::class)
                ->modalHeading(__('upload_file'))
                ->form([
                    // TODO: Hook that check if file exist or not
                    Components\FileUpload::make('files')
                        ->disk('files')
                        ->label(__('files'))
                        ->loadingIndicatorPosition('left')
                        ->multiple()
                        ->removeUploadedFileButtonPosition('right')
                        ->visibility('private')
                        ->openable()
                        ->preserveFilenames()
                        ->moveFiles(),
                ])
                ->using(function (array $data): File {
                    $result = null;
                    foreach ($data['files'] as $file) {
                        /** @var File $result */
                        $result = new File();
                        $result->fill(['filename' => $file, 'code' => Str::random(12)]);
                        $result->folder()->associate($this->liveCurrentFolder);
                        $result->save();
                    }

                    return $result;
                })
                ->successRedirectUrl(fn (): string => FileUtils::getFolderUrls($this->liveCurrentFolder, static::class)['url']),
            DeleteAction::make('deleteFolder')
                ->authorize(Filament::auth()->user()->can('delete_folder'))
                ->label(__('delete_folder'))
                ->modalHeading(__('delete_folder'))
                ->record(fn (): Folder => $this->liveCurrentFolder)
                ->hidden(fn (): bool => self::$rootFolder->getKey() === $this->liveCurrentFolder->getKey())
                ->successRedirectUrl(fn (): string => FileUtils::getFolderUrls($this->liveCurrentFolder->parent, static::class)['url']),
        ];
    }

    /**
     * @return array<WidgetConfiguration>
     */
    protected function getHeaderWidgets(): array
    {
        return [
            ViewFolders::make(['currentFolder' => $this->liveCurrentFolder]),
            ViewFiles::make(['currentFolder' => $this->liveCurrentFolder]),
        ];
    }

    /**
     * @param  Collection<int, Folder>  $children
     * @return array<NavigationItem>
     */
    private static function buildSubTreeNavigationItem(Collection $children): array
    {
        $navigationItems = [];

        foreach ($children as $child) {
            $urls = FileUtils::getFolderUrls($child, static::class);
            $navItem = NavigationItem::make($child->name)
                ->icon(static::getNavigationIcon())
                ->activeIcon(static::getActiveNavigationIcon())
                ->isActiveWhen(fn (): bool => request()->is($urls['flatten']))
                ->badge(static::getNavigationBadge(), color: static::getNavigationBadgeColor())
                ->url($urls['url']);

            $navigationItems[] = $navItem;
        }

        return $navigationItems;
    }

    /**
     * @return array<NavigationItem>
     */
    private static function buildTreeNavigationItems(?Folder $target): array
    {
        if ($target === null) {
            return [];
        }

        $urls = FileUtils::getFolderUrls($target, static::class);

        $navName = $target->name;

        if ($target->parent === null && ! request()->is($urls['flatten'])) {
            $navName = __('go_to') . ' ' . $navName;
        }

        $navigationItem = NavigationItem::make($navName)
            ->icon(static::getNavigationIcon())
            ->activeIcon(static::getActiveNavigationIcon())
            ->isActiveWhen(fn (): bool => request()->is($urls['flatten']))
            ->badge(static::getNavigationBadge(), color: static::getNavigationBadgeColor())
            ->url($urls['url']);

        $navigationItem->childItems(self::buildSubTreeNavigationItem($target->children));

        return [$navigationItem];
    }
}
