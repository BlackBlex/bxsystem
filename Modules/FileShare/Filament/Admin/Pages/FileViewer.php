<?php

declare(strict_types=1);

namespace Modules\FileShare\Filament\Admin\Pages;

use BezhanSalleh\FilamentShield\Traits\HasPageShield;
use Filament\Actions\Action;
use Filament\Actions\DeleteAction;
use Filament\Facades\Filament;
use Filament\Forms\Components\Textarea;
use Filament\Forms\Components\ToggleButtons;
use Filament\Forms\Get;
use Filament\Navigation\NavigationItem;
use Filament\Notifications;
use Filament\Notifications\Notification;
use Filament\Pages\Page;
use Filament\Support\Enums\Alignment;
use Filament\Support\Enums\MaxWidth;
use Illuminate\Contracts\Support\Htmlable;
use Illuminate\Database\Eloquent\Collection;

use function Illuminate\Filesystem\join_paths;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Livewire\Features\SupportRedirects\Redirector;
use Modules\FileShare\Entities\File;
use Modules\FileShare\Enums\FileStatus;
use Modules\FileShare\Helper\FileUtils;

class FileViewer extends Page
{
    use HasPageShield;

    public ?File $currentFile = null;

    public static ?File $staticCurrentFile = null;

    public string $url = '';

    protected static ?string $navigationIcon = 'heroicon-o-document-text';

    protected static ?int $navigationSort = 2;

    protected static ?string $slug = 'file-viewer/{code}';

    protected static string $view = 'modules.file-share.filament.admin.pages.file-viewer';

    private string $filePath = '';

    public static function canView(): bool
    {
        if (Filament::auth()->user()->isSuperAdmin()) {
            return false;
        }

        return Filament::auth()->user()->can(static::getPermissionName());
    }

    /**
     * @return array<NavigationItem>
     */
    public static function getNavigationItems(): array
    {
        $navigationItem = [];
        if (self::$staticCurrentFile !== null) {
            if (self::$staticCurrentFile->folder !== null) {
                $urls = FileUtils::getFolderUrls(self::$staticCurrentFile->folder, FolderViewer::class);

                $currentNavItem = NavigationItem::make(self::$staticCurrentFile->folder->name)
                    ->icon('heroicon-s-folder-minus')
                    ->activeIcon(static::getActiveNavigationIcon())
                    ->isActiveWhen(fn (): bool => request()->is($urls['flatten']))
                    ->badge(static::getNavigationBadge(), color: static::getNavigationBadgeColor())
                    ->sort(static::getNavigationSort())
                    ->url($urls['url']);

                $currentNavItem->childItems(self::buildTreeNavigationItems(self::$staticCurrentFile->folder->files));
                $navigationItem[] = $currentNavItem;
            }
        }

        return $navigationItem;
    }

    public function authorizeFileAction(): Action
    {
        return Action::make('authorizeFile')
            ->authorize(fn (File $record): bool => Filament::auth()->user()->can('authorize_file') && $record->status === FileStatus::Signed)
            ->label(__('authorize'))
            ->color('warning')
            ->record($this->currentFile)
            ->requiresConfirmation()
            ->successRedirectUrl(fn (File $record): string => FileUtils::getFolderUrls($record->folder, FolderViewer::class)['url']);
    }

    public function deleteFileAction(): Action
    {
        return DeleteAction::make('deleteFile')
            ->authorize(Filament::auth()->user()->can('delete_file'))
            ->label(__('delete'))
            ->modalHeading(__('delete'))
            ->record($this->currentFile)
            ->requiresConfirmation()
            ->successRedirectUrl(fn (File $record): string => FileUtils::getFolderUrls($record->folder, FolderViewer::class)['url']);
    }

    public function disallowFileAction(): Action
    {
        return Action::make('disallowFile')
            ->action(function (Action $action, File $record): void {
                // dd('here');
                $action->success();
            })
            ->authorize(fn (File $record): bool => Filament::auth()->user()->can('disallow_file') && $record->status === FileStatus::Authorized)
            ->label(__('disallow'))
            ->record($this->currentFile)
            ->requiresConfirmation()
            ->successRedirectUrl(fn (File $record): string => FileUtils::getFolderUrls($record->folder, FolderViewer::class)['url']);
    }

    public function downloadFileAction(): Action
    {
        return Action::make('downloadFile')
            ->authorize(Filament::auth()->user()->can('view_file'))
            ->color('teal')
            ->label(__('download'))
            ->record($this->currentFile)
            ->action(function (File $record): void {
                Notification::make()
                    ->title(__('download_ready'))
                    ->body(__('download_how_to'))
                    ->icon('forkawesome-download')
                    ->success()
                    ->actions([
                        Notifications\Actions\Action::make('download_url')
                            ->label(__('download'))
                            ->button()
                            ->url($this->generateFileUrl($record), shouldOpenInNewTab: true),
                    ])
                    ->send();
            });
    }

    public function downloadWithAction(): Action
    {
        return Action::make('downloadWith')
            ->authorize(Filament::auth()->user()->can('view_file'))
            ->color('teal')
            ->label(__('download'))
            ->requiresConfirmation()
            ->modalHeading(__('pending_changes_title'))
            ->modalDescription(__('pending_changes_desc'))
            ->record($this->currentFile)
            ->action(fn () => $this->replaceMountedAction('downloadFile'));
    }

    /**
     * @return array<string, string>
     */
    public function getBreadcrumbs(): array
    {
        if ($this->currentFile === null) {
            return [];
        }

        $file = $this->currentFile;

        $result = FileUtils::getBreadcrumbsForFolder($file->folder, FolderViewer::class);

        $result[static::getUrl(['code' => $file->code])] = pathinfo($file->filename, PATHINFO_FILENAME);

        return $result;
    }

    public function getHeading(): string|Htmlable
    {
        if ($this->currentFile === null) {
            return __('explorer');
        }

        return pathinfo($this->currentFile->filename, PATHINFO_FILENAME);
    }

    public function getViewData(): array
    {
        return ['url' => $this->url];
    }

    /**
     * @return void|Redirector
     */
    public function mount(string $code)
    {
        if (Str::length($code) === 12) {
            $this->currentFile = File::whereCode($code)->get()[0];

            if ($this->currentFile === null) {
                /** @var Redirector $result */
                $result = redirect(Filament::getCurrentPanel()->getUrl());

                return $result->with('file_not_found', 'File not found');
            }

            $this->filePath = FileUtils::getFolderPathComplete($this->currentFile->folder);
            $fileDisk = Storage::disk('files');
            $pathComplete = join_paths($this->filePath, $this->currentFile->filename);
            if ($fileDisk->exists($pathComplete)) {
                $this->url = $fileDisk->temporaryUrl(
                    $this->currentFile->code,
                    now()->addMinutes(5),
                    ['action' => 'view']
                );
            }

            static::$staticCurrentFile = $this->currentFile;
        } else {
            /** @var Redirector $result */
            $result = redirect(Filament::getCurrentPanel()->getUrl());

            return $result->with('error', ['icon' => 'forkawesome-chain-broken', 'text' => __('file_not_found')]);
        }
    }

    public function reviewFileAction(): Action
    {
        return Action::make('reviewFile')
            ->form([
                ToggleButtons::make('needChanges')
                    ->label(__('need_changes_question'))
                    ->options([
                        'yes' => __('yes'),
                        'no' => __('no'),
                    ])
                    ->icons([
                        'yes' => 'heroicon-m-x-mark',
                        'no' => 'heroicon-m-check',
                    ])
                    ->colors([
                        'yes' => 'danger',
                        'no' => 'success',
                    ])
                    ->default('no')
                    ->grouped()
                    ->live(),
                Textarea::make('changes')
                    ->label(__('changes'))
                    ->required(fn (Get $get) => $get('needChanges') === 'yes')
                    ->maxLength(65535)
                    ->columnSpanFull()
                    ->hidden(fn (Get $get) => $get('needChanges') === 'no'),
            ])
            ->action(function (array $data, File $record): void {
                if ($record->status === FileStatus::Initial) {
                    if ($data['needChanges'] === 'yes') {
                        $record->status = FileStatus::ReviewedNeed;
                    } else {
                        $record->status = FileStatus::Reviewed;
                    }
                }

                $record->saveQuietly();
            })
            ->authorize(fn (File $record) => Filament::auth()->user()->can('sign_file') && ($record->status !== FileStatus::Reviewed && $record->status !== FileStatus::ReviewedNeed))
            ->color('cyan')
            ->label(__('review'))
            ->modalAlignment(Alignment::Center)
            ->modalFooterActionsAlignment(Alignment::End)
            ->modalWidth(MaxWidth::ScreenSmall)
            ->record($this->currentFile)
            ->successRedirectUrl(fn (File $record): string => FileUtils::getFileUrls($record, FileViewer::class)['url']);
    }

    public function signFileAction(): Action
    {
        return Action::make('signFile')
            ->authorize(fn (File $record) => Filament::auth()->user()->can('sign_file') && $record->status === FileStatus::Reviewed)
            ->label(__('sign'))
            ->color('warning')
            ->record($this->currentFile)
            ->requiresConfirmation()
            ->successRedirectUrl(fn (File $record): string => FileUtils::getFolderUrls($record->folder, FolderViewer::class)['url']);
    }

    public function unsignFileAction(): Action
    {
        return Action::make('unsignFile')
            ->action(function (Action $action, File $record): void {
                // dd('here');
                $action->success();
            })
            ->authorize(fn (File $record) => Filament::auth()->user()->can('unsign_file') && $record->status === FileStatus::Signed)
            ->label(__('unsign'))
            ->record($this->currentFile)
            ->requiresConfirmation()
            ->successRedirectUrl(fn (File $record): string => FileUtils::getFolderUrls($record->folder, FolderViewer::class)['url']);
    }

    /**
     * @param  Collection<int, File>  $children
     * @return array<NavigationItem>
     */
    private static function buildTreeNavigationItems(Collection $children): array
    {
        $navigationItems = [];

        foreach ($children as $child) {
            $urls = FileUtils::getFileUrls($child, static::class);
            $navItem = NavigationItem::make($child->filename)
                ->icon(static::getNavigationIcon())
                ->activeIcon(static::getActiveNavigationIcon())
                ->isActiveWhen(fn (): bool => request()->is($urls['flatten']))
                ->badge(static::getNavigationBadge(), color: static::getNavigationBadgeColor())
                ->sort(static::getNavigationSort())
                ->url($urls['url']);

            $navigationItems[] = $navItem;
        }

        return $navigationItems;
    }

    private function generateFileUrl(File $file): string
    {
        return Storage::disk('files')->temporaryUrl($file->code, now()->addMinutes(3), ['action' => 'download']);
    }
}
