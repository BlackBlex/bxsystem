<?php

declare(strict_types=1);

namespace Modules\FileShare\Filament\Admin\Resources\FolderResource\Pages;

use Filament\Actions;
use Filament\Resources\Pages\ListRecords;
use Modules\FileShare\Filament\Admin\Resources\FolderResource;

class ListFolders extends ListRecords
{
    protected static string $resource = FolderResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}
