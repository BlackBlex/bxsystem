<?php

declare(strict_types=1);

namespace Modules\FileShare\Filament\Admin\Resources\FolderResource\Pages;

use Filament\Resources\Pages\CreateRecord;
use Modules\FileShare\Filament\Admin\Resources\FolderResource;

class CreateFolder extends CreateRecord
{
    protected static string $resource = FolderResource::class;
}
