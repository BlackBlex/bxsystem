<?php

declare(strict_types=1);

namespace Modules\FileShare\Filament\Admin\Resources\ZoneSignResource\Pages;

use Filament\Actions;
use Filament\Resources\Pages\ListRecords;
use Modules\FileShare\Filament\Admin\Resources\ZoneSignResource;

class ListZoneSigns extends ListRecords
{
    protected static string $resource = ZoneSignResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}
