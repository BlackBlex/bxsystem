<?php

declare(strict_types=1);

namespace Modules\FileShare\Filament\Admin\Resources\ZoneSignResource\Pages;

use Filament\Actions;
use Filament\Resources\Pages\ViewRecord;
use Modules\FileShare\Filament\Admin\Resources\ZoneSignResource;

class ViewZoneSign extends ViewRecord
{
    protected static string $resource = ZoneSignResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\EditAction::make(),
        ];
    }
}
