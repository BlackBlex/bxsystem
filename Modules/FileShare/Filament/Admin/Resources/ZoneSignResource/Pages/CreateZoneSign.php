<?php

declare(strict_types=1);

namespace Modules\FileShare\Filament\Admin\Resources\ZoneSignResource\Pages;

use Filament\Resources\Pages\CreateRecord;
use Modules\FileShare\Filament\Admin\Resources\ZoneSignResource;

class CreateZoneSign extends CreateRecord
{
    protected static string $resource = ZoneSignResource::class;
}
