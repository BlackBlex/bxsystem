<?php

declare(strict_types=1);

namespace Modules\FileShare\Filament\Admin\Resources\FileResource\Pages;

use Filament\Actions;
use Filament\Resources\Pages\EditRecord;
use Modules\FileShare\Filament\Admin\Resources\FileResource;

class EditFile extends EditRecord
{
    protected static string $resource = FileResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\ViewAction::make(),
            Actions\DeleteAction::make(),
            Actions\ForceDeleteAction::make(),
            Actions\RestoreAction::make(),
        ];
    }
}
