<?php

declare(strict_types=1);

namespace Modules\FileShare\Filament\Admin\Resources\FileResource\Pages;

use Filament\Resources\Pages\CreateRecord;
use Illuminate\Support\Str;
use Modules\FileShare\Filament\Admin\Resources\FileResource;

class CreateFile extends CreateRecord
{
    protected static string $resource = FileResource::class;

    protected function mutateFormDataBeforeCreate(array $data): array
    {
        $data['code'] = Str::random(12);

        return $data;
    }
}
