<?php

declare(strict_types=1);

namespace Modules\FileShare\Filament\Admin\Resources\FileResource\Pages;

use Filament\Actions;
use Filament\Resources\Pages\ListRecords;
use Modules\FileShare\Filament\Admin\Resources\FileResource;

class ListFiles extends ListRecords
{
    protected static string $resource = FileResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}
