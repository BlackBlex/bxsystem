<?php

declare(strict_types=1);

namespace Modules\FileShare\Filament\Admin\Resources\SignatureResource\Pages;

use Filament\Actions;
use Filament\Resources\Pages\EditRecord;
use Modules\FileShare\Filament\Admin\Resources\SignatureResource;

class EditSignature extends EditRecord
{
    protected static string $resource = SignatureResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\ViewAction::make(),
            Actions\DeleteAction::make(),
            Actions\ForceDeleteAction::make(),
            Actions\RestoreAction::make(),
        ];
    }
}
