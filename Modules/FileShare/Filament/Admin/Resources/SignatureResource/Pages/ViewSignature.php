<?php

declare(strict_types=1);

namespace Modules\FileShare\Filament\Admin\Resources\SignatureResource\Pages;

use Filament\Actions;
use Filament\Resources\Pages\ViewRecord;
use Modules\FileShare\Filament\Admin\Resources\SignatureResource;

class ViewSignature extends ViewRecord
{
    protected static string $resource = SignatureResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\EditAction::make(),
        ];
    }
}
