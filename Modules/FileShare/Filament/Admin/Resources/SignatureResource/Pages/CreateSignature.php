<?php

declare(strict_types=1);

namespace Modules\FileShare\Filament\Admin\Resources\SignatureResource\Pages;

use Filament\Resources\Pages\CreateRecord;
use Modules\FileShare\Filament\Admin\Resources\SignatureResource;

class CreateSignature extends CreateRecord
{
    protected static string $resource = SignatureResource::class;
}
