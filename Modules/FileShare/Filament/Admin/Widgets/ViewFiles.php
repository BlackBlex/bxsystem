<?php

declare(strict_types=1);

namespace Modules\FileShare\Filament\Admin\Widgets;

use BezhanSalleh\FilamentShield\Traits\HasWidgetShield;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Table;
use Filament\Widgets\TableWidget as BaseWidget;
use Illuminate\Database\Eloquent\Builder;
use Modules\FileShare\Entities\File;
use Modules\FileShare\Entities\Folder;
use Modules\FileShare\Enums\FileStatus;

class ViewFiles extends BaseWidget
{
    use HasWidgetShield;

    public ?Folder $currentFolder = null;

    public function table(Table $table): Table
    {
        return $table
            ->columns([
                TextColumn::make('filename')
                    ->label(__('filename')),
                TextColumn::make('status')
                    ->label(__('status'))
                    ->badge()
                    ->color(fn (FileStatus $state): string => match ($state) {
                        FileStatus::Authorized => 'success',
                        FileStatus::Initial => 'gray',
                        FileStatus::Reviewed => 'cyan',
                        FileStatus::ReviewedNeed => 'rose',
                        FileStatus::Signed => 'warning',
                        FileStatus::Updated => 'info',
                    }),
                TextColumn::make('created_at')
                    ->label(__('created_at'))
                    ->wrap()
                    ->dateTime(),
                TextColumn::make('updated_at')
                    ->label(__('updated_at'))
                    ->wrap()
                    ->dateTime(),
            ])
            ->striped()
            ->heading('')
            ->paginated(false)
            ->query(function (): Builder {
                $query = File::query();
                $query->whereFolderId($this->currentFolder !== null ? $this->currentFolder->getKey() : null);

                return $query;
            })
            ->recordUrl(
                fn (File $record): string => route('filament.fileshare::admin.pages.file-viewer.{code}', ['code' => $record->code]),
            );
    }
}
