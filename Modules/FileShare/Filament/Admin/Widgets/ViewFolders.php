<?php

declare(strict_types=1);

namespace Modules\FileShare\Filament\Admin\Widgets;

use BezhanSalleh\FilamentShield\Traits\HasWidgetShield;
use Filament\Tables\Columns\IconColumn;
use Filament\Tables\Columns\Layout\Split;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Table;
use Filament\Widgets\TableWidget as BaseWidget;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\HtmlString;
use Modules\FileShare\Entities\Folder;

class ViewFolders extends BaseWidget
{
    use HasWidgetShield;

    public ?Folder $currentFolder = null;

    public function table(Table $table): Table
    {
        return $table
            ->columns([
                Split::make([
                    IconColumn::make('description')
                        ->icon(fn (Folder $record): string => 'heroicon-m-folder')
                        ->size('fi-ta-icon-item-size-2xl w-16 h-16')
                        ->grow(false),
                    TextColumn::make('name')
                        ->description(fn (Folder $record): string => $record->description)
                        ->tooltip(fn (Folder $record): string => __('updated') . ' ' . ($record->updated_at !== null ? $record->updated_at->diffForHumans() : '')),
                ])->from('sm'),
            ])
            ->contentGrid([
                'md' => 2,
                'xl' => 3,
            ])
            ->heading('')
            ->emptyState(new HtmlString('<div class="hidden"></div>'))
            ->paginated(false)
            ->query(function (): Builder {
                $query = Folder::query();

                $query->whereParentId($this->currentFolder !== null ? $this->currentFolder->getKey() : null);

                return $query;
            })
            ->recordUrl(
                fn (Folder $record): string => route('filament.fileshare::admin.pages.folder-viewer.{record?}', ['record' => $record]),
            );
    }
}
