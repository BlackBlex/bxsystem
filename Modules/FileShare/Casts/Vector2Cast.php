<?php

declare(strict_types=1);

namespace Modules\FileShare\Casts;

use Illuminate\Contracts\Database\Eloquent\CastsAttributes;
use Illuminate\Database\Eloquent\Model;
use Modules\FileShare\Entities\WDB\Vector2;

/**
 * @template-implements CastsAttributes<Vector2, mixed>
 */
final class Vector2Cast implements CastsAttributes
{
    /**
     * Transform the attribute from the underlying model values.
     *
     * @param  array<string, mixed>  $attributes
     */
    public function get(Model $model, string $key, mixed $value, array $attributes): Vector2
    {
        if ($value === '') {
            return Vector2::Inf();
        }

        /* @var Array<string, float> $futureVecto2 */
        $futureVector2 = \Safe\json_decode($value);

        if (count($futureVector2) != 2) {
            return Vector2::Inf();
        }

        return new Vector2($futureVector2['x'], $futureVector2['y']);
    }

    /**
     * Transform the attribute to its underlying model values.
     *
     * @param  array<string, mixed>  $attributes
     */
    public function set(Model $model, string $key, mixed $value, array $attributes): string
    {
        if ($value === null) {
            return '';
        }

        if (! $value instanceof Vector2) {
            throw new \InvalidArgumentException();
        }

        return (string) $value;
    }
}
