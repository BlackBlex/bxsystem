<?php

declare(strict_types=1);

namespace Modules\FileShare\Helper;

use Modules\FileShare\Entities\File;
use Modules\FileShare\Entities\Folder;

class FileUtils
{
    /**
     * @param  class-string  $resource
     * @return array<string, string>
     */
    public static function getBreadcrumbsForFolder(?Folder $folder, $resource): array
    {
        /** @var array<string, string> $breadcrumbs */
        $breadcrumbs = [];

        /** @var Folder $folderTo */
        $folderTo = $folder ?? Folder::whereParentId(null)->get()[0];

        $urls = FileUtils::getFolderUrls($folderTo, $resource);

        $breadcrumbs['/' . $urls['flatten']] = $folderTo->name;

        $breadcrumbs = array_merge($breadcrumbs, FileUtils::getSubsequentBreadcrumbsForFolder($folderTo, $resource));

        $breadcrumbs = array_reverse($breadcrumbs);

        return $breadcrumbs;
    }

    /**
     * @param  class-string  $resource
     * @return array<string, string>
     */
    public static function getFileUrls(File $file, $resource): array
    {
        /** @var string $url */
        $url = $resource::getUrl(['code' => $file->code]);

        /** @var string $url */
        $flatten = str_replace(config('app.url') . '/', '', $url);

        return ['url' => $url, 'flatten' => $flatten];
    }

    public static function getFolderPathComplete(Folder $folder): string
    {
        $result = [];

        $currentFolder = $folder;
        while ($currentFolder !== null) {
            $result[] = $currentFolder->name;

            $currentFolder = $currentFolder->parent;
        }

        $result = array_reverse($result);

        return implode(DIRECTORY_SEPARATOR, $result);
    }

    /**
     * @param  class-string  $resource
     * @return array<string, string>
     */
    public static function getFolderUrls(Folder $folder, $resource): array
    {
        /** @var string $url */
        $url = $resource::getUrl(['record' => $folder->getKey()]);

        if ($folder->parent === null) {
            $url = $resource::getUrl();
        }

        /** @var string $url */
        $flatten = str_replace(config('app.url') . '/', '', $url);

        return ['url' => $url, 'flatten' => $flatten];
    }

    /**
     * @param  class-string  $resource
     * @return array<string, string>
     */
    public static function getSubsequentBreadcrumbsForFolder(?Folder $folder, $resource): array
    {
        $breadcrumbs = [];

        if ($folder === null) {
            return $breadcrumbs;
        }

        $urls = FileUtils::getFolderUrls($folder, $resource);

        $breadcrumbs['/' . $urls['flatten']] = $folder->name;

        $breadcrumbs = array_merge($breadcrumbs, FileUtils::getSubsequentBreadcrumbsForFolder($folder->parent, $resource));

        return $breadcrumbs;
    }
}
