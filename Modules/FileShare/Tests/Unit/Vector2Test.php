<?php

declare(strict_types=1);

use Modules\FileShare\Entities\WDB\Vector2;

uses(Tests\TestCase::class);

test('Vector2(0, 0) is Vector2::Zero', function (): void {
    $zeroVector2 = new Vector2(0, 0);

    expect(Vector2::isZero($zeroVector2))->toBeTrue();
});

test('Vector2(0, 0) isn´t Vector2::One', function (): void {
    $zeroVector2 = new Vector2(0, 0);

    expect(Vector2::isOne($zeroVector2))->toBeFalse();
});

test('Vector2(1, 1) is Vector2::One', function (): void {
    $oneVector2 = Vector2::One();

    expect(Vector2::isOne($oneVector2))->toBeTrue();
});

test('Vector2(1, 1) isn´t Vector2::Zero', function (): void {
    $oneVector2 = new Vector2(1, 1);

    expect(Vector2::isZero($oneVector2))->toBeFalse();
});

test('Vector2::Inf is Vector2::Inf', function (): void {
    $infVector2 = Vector2::Inf();

    expect(Vector2::isINF($infVector2))->toBeTrue();
});

test('Vector2::Inf isn´t Vector2::One', function (): void {
    $infVector2 = Vector2::Inf();

    expect(Vector2::isOne($infVector2))->toBeFalse();
});

test('Vector2::Inf isn´t Vector2::Zero', function (): void {
    $infVector2 = Vector2::Inf();

    expect(Vector2::isZero($infVector2))->toBeFalse();
});
