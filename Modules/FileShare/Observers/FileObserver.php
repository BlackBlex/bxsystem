<?php

declare(strict_types=1);

namespace Modules\FileShare\Observers;

use Filament\Facades\Filament;
use Illuminate\Support\Facades\Storage;
use Modules\FileShare\Entities\File;
use Modules\FileShare\Helper\FileUtils;

class FileObserver
{
    /**
     * Handle the File "created" event.
     */
    public function created(File $file): void
    {
        $fileDisk = Storage::disk('files');

        $pathComplete = FileUtils::getFolderPathComplete($file->folder);

        if (! $fileDisk->exists($pathComplete)) {
            $fileDisk->makeDirectory($pathComplete);
        }

        $resultFilename = $pathComplete . DIRECTORY_SEPARATOR . $file->filename;

        $fileDisk->move($file->filename, $resultFilename);
    }

    /**
     * Handle the File "creating" event.
     */
    public function creating(File $file): void
    {
        if (! Filament::auth()->check()) {
            return;
        }

        $file->uploadedBy()->associate(Filament::auth()->user()->employee);
    }

    /**
     * Handle the File "deleted" event.
     */
    public function deleted(File $file): void
    {
        //
    }

    /**
     * Handle the File "force deleted" event.
     */
    public function forceDeleted(File $file): void
    {
        //
    }

    /**
     * Handle the File "restored" event.
     */
    public function restored(File $file): void
    {
        //
    }

    /**
     * Handle the File "updated" event.
     */
    public function updated(File $file): void
    {
        //
    }
}
