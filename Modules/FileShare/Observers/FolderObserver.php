<?php

declare(strict_types=1);

namespace Modules\FileShare\Observers;

use Filament\Facades\Filament;
use Modules\FileShare\Entities\File;
use Modules\FileShare\Entities\Folder;

class FolderObserver
{
    /**
     * Handle the Folder "created" event.
     */
    public function created(Folder $folder): void
    {
        //
    }

    /**
     * Handle the File "creating" event.
     */
    public function creating(Folder $folder): void
    {
        if (! Filament::auth()->check()) {
            return;
        }

        $folder->createdBy()->associate(Filament::auth()->user()->employee);
    }

    /**
     * Handle the Folder "deleted" event.
     */
    public function deleted(Folder $folder): void
    {
        $folder->children->each(fn (Folder $child): bool => $child->delete() ?? false);
        $folder->files->each(fn (File $file): bool => $file->delete() ?? false);
    }

    /**
     * Handle the Folder "force deleted" event.
     */
    public function forceDeleted(Folder $folder): void
    {
        //
    }

    /**
     * Handle the Folder "restored" event.
     */
    public function restored(Folder $folder): void
    {
        $folder->children()->onlyTrashed()->each(fn (Folder $child): bool => $child->restore());
        $folder->files()->onlyTrashed()->each(fn (File $file): bool => $file->restore());
    }

    /**
     * Handle the Folder "updated" event.
     */
    public function updated(Folder $folder): void
    {
        //
    }
}
