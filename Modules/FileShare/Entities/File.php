<?php

declare(strict_types=1);

namespace Modules\FileShare\Entities;

use App\Traits\HasComments;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\FileShare\Database\Factories\FileFactory;
use Modules\FileShare\Enums\FileStatus;
use Modules\Users\Entities\Employee;
use Modules\Users\Entities\Scopes\CompanyThroughScope;

class File extends Model
{
    use CompanyThroughScope;
    use HasComments;
    use HasFactory;
    use SoftDeletes;

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'status' => FileStatus::class,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array<string>
     */
    protected $fillable = ['code', 'filename', 'status']; // 'description',

    public function folder(): BelongsTo
    {
        return $this->belongsTo(Folder::class, 'folder_id');
    }

    public function uploadedBy(): BelongsTo
    {
        return $this->belongsTo(Employee::class, 'uploaded_by');
    }

    protected static function newFactory(): FileFactory
    {
        return FileFactory::new();
    }
}
