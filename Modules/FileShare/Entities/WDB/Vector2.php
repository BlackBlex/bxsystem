<?php

/*
 ***************************************************************************************
 *    Title: PocketMine-MP:Math source code
 *    Author: pmmp
 *    Date: Agust 3, 2023
 *    Code version: 1.0.0
 *    Availability: https://github.com/pmmp/Math
 ***************************************************************************************
*/

declare(strict_types=1);

namespace Modules\FileShare\Entities\WDB;

use function abs;
use function ceil;
use function floor;
use function round;
use function sqrt;

class Vector2
{
    public function __construct(
        public float $x,
        public float $y
    ) {
    }

    public function __toString(): string
    {
        return \Safe\json_encode(['x' => $this->x, 'y' => $this->y]);
    }

    public static function Inf(): Vector2
    {
        return new Vector2(99999, -99999);
    }

    public static function isINF(Vector2 $vector2): bool
    {
        return $vector2->getXInt() === 99999 && $vector2->getYInt() === -99999;
    }

    public static function isOne(Vector2 $vector2): bool
    {
        return $vector2->getX() === 1.0 && $vector2->getY() === 1.0;
    }

    public static function isZero(Vector2 $vector2): bool
    {
        return $vector2->getX() === 0.0 && $vector2->getY() === 0.0;
    }

    public static function One(): Vector2
    {
        return new Vector2(1, 1);
    }

    public static function Zero(): Vector2
    {
        return new Vector2(0, 0);
    }

    public function abs(): Vector2
    {
        return new Vector2(abs($this->x), abs($this->y));
    }

    public function add(float $x, float $y): Vector2
    {
        return new Vector2($this->x + $x, $this->y + $y);
    }

    public function addVector(Vector2 $vector2): Vector2
    {
        return $this->add($vector2->x, $vector2->y);
    }

    public function ceil(): Vector2
    {
        return new Vector2((int) ceil($this->x), (int) ceil($this->y));
    }

    public function distance(Vector2 $pos): float
    {
        return sqrt($this->distanceSquared($pos));
    }

    public function distanceSquared(Vector2 $pos): float
    {
        return (($this->x - $pos->x) ** 2) + (($this->y - $pos->y) ** 2);
    }

    public function divide(float $number): Vector2
    {
        return new Vector2($this->x / $number, $this->y / $number);
    }

    public function dot(Vector2 $v): float
    {
        return $this->x * $v->x + $this->y * $v->y;
    }

    public function floor(): Vector2
    {
        return new Vector2((int) floor($this->x), (int) floor($this->y));
    }

    public function getX(): float
    {
        return $this->x;
    }

    public function getXFloor(): int
    {
        return (int) floor($this->x);
    }

    public function getXInt(): int
    {
        return (int) $this->x;
    }

    public function getY(): float
    {
        return $this->y;
    }

    public function getYFloor(): int
    {
        return (int) floor($this->y);
    }

    public function getYInt(): int
    {
        return (int) $this->y;
    }

    public function length(): float
    {
        return sqrt($this->lengthSquared());
    }

    public function lengthSquared(): float
    {
        return $this->x * $this->x + $this->y * $this->y;
    }

    public function multiply(float $number): Vector2
    {
        return new Vector2($this->x * $number, $this->y * $number);
    }

    public function normalize(): Vector2
    {
        $len = $this->lengthSquared();
        if ($len > 0) {
            return $this->divide(sqrt($len));
        }

        return new Vector2(0, 0);
    }

    public function round(): Vector2
    {
        return new Vector2(round($this->x), round($this->y));
    }

    public function subtract(float $x, float $y): Vector2
    {
        return $this->add(-$x, -$y);
    }

    public function subtractVector(Vector2 $vector2): Vector2
    {
        return $this->add(-$vector2->x, -$vector2->y);
    }
}
