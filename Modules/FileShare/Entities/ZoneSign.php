<?php

declare(strict_types=1);

namespace Modules\FileShare\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\FileShare\Casts\Vector2Cast;
use Modules\FileShare\Database\Factories\ZoneSignFactory;

class ZoneSign extends Model
{
    use HasFactory;
    use SoftDeletes;

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'coordinate' => Vector2Cast::class,
        'size' => Vector2Cast::class,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array<string>
     */
    protected $fillable = ['name', 'coordinate', 'size', 'page'];

    protected static function newFactory(): ZoneSignFactory
    {
        return ZoneSignFactory::new();
    }
}
