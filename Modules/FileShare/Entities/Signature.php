<?php

declare(strict_types=1);

namespace Modules\FileShare\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\FileShare\Database\Factories\SignatureFactory;
use Modules\Users\Entities\Employee;

class Signature extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = ['filename'];

    public function user(): BelongsTo
    {
        return $this->belongsTo(Employee::class, 'user_id');
    }

    protected static function newFactory(): SignatureFactory
    {
        return SignatureFactory::new();
    }
}
