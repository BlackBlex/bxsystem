<?php

declare(strict_types=1);

namespace Modules\FileShare\Entities\Scopes;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Modules\FileShare\Entities\Folder;

trait HasFolderScope
{
    /**
     * @param  Model|array<Model>  $models
     */
    abstract public function associateModel($models): void;

    /**
     * @param  Model|array<Model>  $models
     */
    abstract public function dissociateModel($models): void;

    abstract public function getNameOfModel(): string;

    /**
     * @param  string|null  $name
     * @return Folder|null
     */
    public function scopeGetFolderByName(Builder $_query, $name = null)
    {
        if ($name === null) {
            /** @phpstan-ignore-next-line */
            $name = $this->{$this->getNameOfModel()};
        }

        return Folder::whereName($name)->whereParentId(null)->first();
    }

    protected static function bootHasFolderScope(): void
    {
        static::created(function (Model $model): void {
            if (method_exists($model, 'getNameOfModel')) {
                /**
                 * @phpstan-ignore-next-line
                 *
                 * @var Folder $folder */
                $folder = Folder::create(['name' => $model->{$model->getNameOfModel()}, 'description' => 'Main folder']);

                $folder->createdBy()->disassociate();
                $folder->saveQuietly();
            }
        });

        static::updated(function (Model $model): void {
            if (method_exists($model, 'getNameOfModel') && method_exists($model, 'getFolderByName')) {
                if ($model->isDirty($model->getNameOfModel())) {
                    /**
                     * @phpstan-ignore-next-line
                     *
                     * @var string $originalName */
                    $originalName = $model->getOriginal($model->getNameOfModel());

                    $folder = $model->getFolderByName($originalName);

                    if ($folder !== null) {
                        /** @phpstan-ignore-next-line */
                        $folder->name = $model->{$model->getNameOfModel()};
                        $folder->saveQuietly();
                    }
                }
            }
        });

        static::deleted(function (Model $model): void {
            if (method_exists($model, 'getNameOfModel')) {
                /** @phpstan-ignore-next-line */
                $folder = Folder::whereName($model->{$model->getNameOfModel()})->first();

                if ($folder !== null) {
                    $folder->delete();
                }
            }
        });

        static::restored(function (Model $model): void {
            if (method_exists($model, 'getNameOfModel')) {
                /** @phpstan-ignore-next-line */
                $folder = Folder::onlyTrashed()->whereName($model->{$model->getNameOfModel()})->first();

                if ($folder !== null) {
                    $folder->restore();
                }
            }
        });
    }
}
