<?php

declare(strict_types=1);

namespace Modules\FileShare\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Modules\FileShare\Entities\File;
use Modules\FileShare\Entities\Folder;
use Modules\FileShare\Entities\Signature;
use Modules\FileShare\Entities\ZoneSign;
use Modules\FileShare\Policies\FilePolicy;
use Modules\FileShare\Policies\FolderPolicy;
use Modules\FileShare\Policies\SignaturePolicy;
use Modules\FileShare\Policies\ZoneSignPolicy;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The model to policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        File::class => FilePolicy::class,
        Folder::class => FolderPolicy::class,
        Signature::class => SignaturePolicy::class,
        ZoneSign::class => ZoneSignPolicy::class,
    ];

    /**
     * Register any application authentication / authorization services.
     */
    public function boot(): void
    {
        $this->registerPolicies();
    }
}
