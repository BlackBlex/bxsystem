<?php

declare(strict_types=1);

namespace Modules\FileShare\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Modules\FileShare\Entities\File;
use Modules\FileShare\Entities\Folder;
use Modules\FileShare\Observers\FileObserver;
use Modules\FileShare\Observers\FolderObserver;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The model observers to register.
     *
     * @var array<string, string|object|array<int, string|object>>
     */
    protected $observers = [
        Folder::class => FolderObserver::class,
        File::class => FileObserver::class,
    ];
}
