<?php

declare(strict_types=1);

namespace Modules\FileShare\Providers\Filament;

use App\Filament\Pages\Auth\EditProfile;
use App\Filament\Providers\Avatar\BoringAvatars;
use Filament\Facades\Filament;
use Filament\Http\Middleware\Authenticate;
use Filament\Http\Middleware\DisableBladeIconComponents;
use Filament\Http\Middleware\DispatchServingFilamentEvent;
use Filament\Panel;
use Filament\PanelProvider;
use Filament\Support\Colors\Color;
use Filament\Widgets;
use Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse;
use Illuminate\Cookie\Middleware\EncryptCookies;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken;
use Illuminate\Routing\Middleware\SubstituteBindings;
use Illuminate\Session\Middleware\AuthenticateSession;
use Illuminate\Session\Middleware\StartSession;
use Illuminate\View\Middleware\ShareErrorsFromSession;

class AdminPanelProvider extends PanelProvider
{
    public const PERMISSIONS = ['view_any_file', 'page_FolderViewer', 'page_FileViewer', 'view_any_folder', 'view_any_signature', 'view_any_zone::sign'];

    private string $module = 'FileShare';

    public function panel(Panel $panel): Panel
    {
        $moduleNamespace = $this->getModuleNamespace();

        return $panel
            ->id('fileshare::admin')
            ->path('admin/fileshare')
            ->permission(self::PERMISSIONS)
            ->login()
            ->profile(EditProfile::class)
            ->colors([
                'primary' => Color::Red,
            ])
            ->brandLogo(function (): string {
                if (Filament::auth()->check()) {
                    /** @var \App\Models\User $user */
                    $user = Filament::auth()->user();

                    if ($user->employee->company !== null) {
                        return asset('logos/' . $user->employee->company->logo);
                    }
                }

                return '';
            })
            ->brandLogoHeight('5rem')
            ->defaultAvatarProvider(BoringAvatars::class)
            ->discoverResources(in: module_path($this->module, 'Filament/Admin/Resources'), for: "$moduleNamespace\\Filament\\Admin\\Resources")
            ->discoverPages(in: module_path($this->module, 'Filament/Admin/Pages'), for: "$moduleNamespace\\Filament\\Admin\\Pages")
            ->discoverWidgets(in: module_path($this->module, 'Filament/Admin/Widgets'), for: "$moduleNamespace\\Filament\\Admin\\Widgets")
            ->discoverWidgets(in: module_path($this->module, 'Filament/Admin/Widgets'), for: "$moduleNamespace\\Filament\\Admin\\Widgets")
            ->widgets([
                Widgets\AccountWidget::class,
                // Widgets\FilamentInfoWidget::class,
            ])
            ->middleware([
                EncryptCookies::class,
                AddQueuedCookiesToResponse::class,
                StartSession::class,
                AuthenticateSession::class,
                ShareErrorsFromSession::class,
                VerifyCsrfToken::class,
                SubstituteBindings::class,
                DisableBladeIconComponents::class,
                DispatchServingFilamentEvent::class,
            ])
            ->authMiddleware([
                Authenticate::class,
            ])
            ->sidebarWidth('18rem')
            ->viteTheme(['resources/css/filament/modules/theme.css', 'resources/css/filament/admin/theme.css']);
    }

    protected function getModuleNamespace(): string
    {
        return config('modules.namespace') . '\\' . $this->module;
    }
}
