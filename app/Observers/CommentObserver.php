<?php

declare(strict_types=1);

namespace App\Observers;

use App\Models\Comment;
use Filament\Facades\Filament;

class CommentObserver
{
    /**
     * Handle the Comment "created" event.
     */
    public function created(Comment $comment): void
    {
        //
    }

    /**
     * Handle the Comment "creating" event.
     */
    public function creating(Comment $comment): void
    {
        if (! Filament::auth()->check()) {
            return;
        }

        $comment->commentator()->associate(Filament::auth()->user());
        // TODO: Get method for user choose approved or not
        if (config('comments.automatically_approve_comment') === true) {
            $comment->approved = true;
        }
    }

    /**
     * Handle the Comment "deleted" event.
     */
    public function deleted(Comment $comment): void
    {
        $comment->children()->each(fn (Comment $child): bool => $child->delete() ?? false);
    }

    /**
     * Handle the Comment "force deleted" event.
     */
    public function forceDeleted(Comment $comment): void
    {
        $comment->children()->each(fn (Comment $child): bool => $child->forceDelete() ?? false);
    }

    /**
     * Handle the Comment "restored" event.
     */
    public function restored(Comment $comment): void
    {
        /** @phpstan-ignore-next-line */
        $comment->children()->onlyTrashed()->each(fn (Comment $child): bool => $child->restore());
    }

    /**
     * Handle the Comment "updated" event.
     */
    public function updated(Comment $comment): void
    {
        //
    }
}
