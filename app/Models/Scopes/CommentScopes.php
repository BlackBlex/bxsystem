<?php

declare(strict_types=1);

namespace App\Models\Scopes;

use Illuminate\Database\Eloquent\Builder;

trait CommentScopes
{
    public function scopeParent(Builder $query): void
    {
        $query->whereNull('parent_id');
    }
}
