<?php

declare(strict_types=1);

namespace App\Models;

use ALajusticia\Expirable\Traits\Expirable;
use App\Contracts\HasCommentSearchByUser;
use App\Traits\HasApiTokens;
use App\Traits\HasCommentAvatar;
use BackedEnum;
use BezhanSalleh\FilamentShield\Support\Utils;
use BezhanSalleh\FilamentShield\Traits\HasPanelShield;
use Carbon\Carbon;
use Filament\Facades\Filament;
use Filament\Models\Contracts\FilamentUser;
use Filament\Models\Contracts\HasAvatar;
use Filament\Models\Contracts\HasName;
use Filament\Panel;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;
use Modules\Users\Traits\HasEmployee;
use Spatie\Permission\Contracts\Permission;
use Spatie\Permission\Traits\HasPermissions;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable implements FilamentUser, HasAvatar, HasCommentSearchByUser, HasName, MustVerifyEmail
{
    use Expirable;
    use HasApiTokens;
    use HasCommentAvatar;
    use HasEmployee;
    use HasFactory;
    use HasPanelShield;
    use HasPermissions;
    use HasRoles;
    use Notifiable;
    use SoftDeletes;

    public const EXPIRES_AT = 'ends_at';

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
        User::EXPIRES_AT => 'datetime',
        'password' => 'hashed',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array<string>
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'password',
        'avatar_url',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
        'email_verified_at',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    public static function defaultExpiresAt(): Carbon
    {
        return Carbon::now()->addMonths(3);
    }

    public function avatar(): string
    {
        return $this->getFilamentAvatarUrl() ?? Filament::getUserAvatarUrl($this);
    }

    public function canAccessPanel(Panel $panel): bool
    {
        if ($this->isSuperAdmin()) {
            return true;
        }

        if (! str_ends_with($this->email, env('SAFE_DOMAIN', 'example.com')) || ! $this->hasVerifiedEmail()) {
            return false;
        }

        if (count($panel->getPermission()) === 0) {
            return false;
        }

        if (in_array(Panel::BYPASSPERMISSION, $panel->getPermission(), true)) {
            return true;
        } else {
            return $this->hasAnyPermission($panel->getPermission());
        }
    }

    public function getFilamentAvatarUrl(): ?string
    {
        return ! is_null($this->avatar_url) ? Storage::disk('avatars')->url($this->avatar_url) : null;
    }

    public function getFilamentName(): string
    {
        return $this->fullName;
    }

    /**
     * Determine if the model has any of the given permissions.
     *
     * @param  string|int|array<mixed>|Permission|Collection|BackedEnum  ...$permissions
     */
    public function hasAnyPermission(...$permissions): bool
    {
        $permissions = collect($permissions)->flatten();

        if ($this->isSuperAdmin()) {
            return true;
        }

        if ($permissions->contains(Panel::BYPASSPERMISSION)) {
            return true;
        }

        foreach ($permissions as $permission) {
            if ($this->checkPermissionTo($permission)) {
                return true;
            }
        }

        return false;
    }

    public function isSuperAdmin(): bool
    {
        return $this->hasRole(Utils::getSuperAdminName());
    }

    /**
     * Get all user roles regardless of team
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany<\Illuminate\Database\Eloquent\Model>
     */
    public function rolesWithGlobal(): MorphToMany
    {
        return $this->morphToMany(
            config('permission.models.role'),
            'model',
            config('permission.table_names.model_has_roles'),
            config('permission.column_names.model_morph_key'),
            config('permission.column_names.role_pivot_key') ?? 'role_id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection<int, \Illuminate\Database\Eloquent\Model>|\Illuminate\Database\Eloquent\Collection<int, \Illuminate\Database\Query\Builder&static>|\Illuminate\Support\Collection<array-key, mixed>
     */
    public function searchUserBy(string $value)
    {
        $values = explode(' ', $value);
        $results = collect();

        if (count($values) > 1) {
            $results = User::where(function (Builder $query) use ($values): void {
                $query->whereIn('first_name', $values);
                $query->orWhere(function (Builder $query) use ($values): void {
                    $query->whereIn('last_name', $values);
                });
            });
        } else {
            $results = User::where(function (Builder $query) use ($value): void {
                $query->where('first_name', 'LIKE', '%' . $value . '%');
                $query->orWhere(function (Builder $query) use ($value): void {
                    $query->where('last_name', 'LIKE', '%' . $value . '%');
                });
            });
        }

        return $results->take(5)->get();
    }

    /**
     * Get full name from the current user
     *
     * @return Attribute<string, never>
     */
    protected function fullName(): Attribute
    {
        return new Attribute(
            get: fn (): string => $this->first_name . ' ' . $this->last_name
        );
    }
}
