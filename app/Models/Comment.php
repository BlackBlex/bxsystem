<?php

declare(strict_types=1);

namespace App\Models;

use App\Models\Presenters\CommentPresenter;
use App\Models\Scopes\CommentApprovedScope;
use App\Models\Scopes\CommentScopes;
use App\Traits\HasCommentModels;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends Model
{
    use CommentScopes;
    use HasCommentModels;
    use HasFactory;
    use SoftDeletes;

    /**
     * The accessors to append to the model's array form.
     *
     * @var array<int, string>
     */
    protected $appends = ['is_parent'];

    protected $casts = [
        'approved' => 'boolean',
    ];

    protected $fillable = ['body'];

    public function children(): HasMany
    {
        return $this->hasMany($this->commentModel(), 'parent_id')->oldest();
    }

    public function commentable(): MorphTo
    {
        return $this->morphTo();
    }

    public function commentator(): BelongsTo
    {
        return $this->belongsTo($this->commentatorModel(), 'commentator_id');
    }

    public function presenter(): CommentPresenter
    {
        return new CommentPresenter($this);
    }

    protected static function boot(): void
    {
        parent::boot();
        static::addGlobalScope(new CommentApprovedScope);
    }

    /**
     * @return Attribute<bool, never>
     */
    protected function isEdited(): Attribute
    {
        return new Attribute(
            get: function (): bool {
                if ($this->created_at === null) {
                    return false;
                }

                return $this->created_at->ne($this->updated_at);
            });
    }

    /**
     * @return Attribute<bool, never>
     */
    protected function isParent(): Attribute
    {
        return new Attribute(
            get: function (): bool {
                return is_null($this->parent_id);
            });
    }
}
