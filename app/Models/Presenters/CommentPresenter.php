<?php

declare(strict_types=1);

namespace App\Models\Presenters;

use App\Contracts\HasCommentSearchByUser;
use App\Models\Comment;
use App\Traits\HasCommentModels;
use Illuminate\Support\HtmlString;

class CommentPresenter
{
    use HasCommentModels;

    public Comment $comment;

    public function __construct(Comment $comment)
    {
        $this->comment = $comment;
    }

    public function markdownBody(): HtmlString
    {
        return new HtmlString((string) app('markdown')->convert($this->comment->body));
    }

    public function relativeCreatedAt(): mixed
    {
        return $this->comment->created_at?->diffForHumans();
    }

    /**
     * @return string|array<string>
     */
    public function replaceMentions(HtmlString $text)
    {
        /** @var string $textString */
        $textString = (string) $text;

        \Safe\preg_match_all('/@([A-Za-z0-9_]+)/', $textString, $matches);

        if ($matches === null) {
            return $text->toHtml();
        }

        /** @var array<string> $names */
        $names = $matches[1];
        $replacements = [];

        // TODO: Check performance
        $modelUser = $this->userModel();
        $modelUser = new $modelUser();
        $modelInstanceSearchBy = $modelUser instanceof HasCommentSearchByUser;

        foreach ($names as $name) {
            /** @var string $nameToSearch */
            $nameToSearch = str_replace('_', ' ', $name);
            if ($modelInstanceSearchBy) {
                $commentator = $modelUser->searchUserBy($nameToSearch)->first();
            } else {
                $commentator = $this->userModel()::where(config('comments.field_for_search', 'name'), 'LIKE', '%' . $nameToSearch . '%')->first();
            }

            if ($commentator) {
                $commentatorProfileRoute = config('comments.profile_route', 'users');

                $replacements['@' . $name] = '<a class="text-blue-500 dark:text-blue-400" href="/' . $commentatorProfileRoute . '/' . $name . '">@' . $name .
                    '</a>';
            } else {
                $replacements['@' . $name] = '@' . $name;
            }
        }

        return str_replace(array_keys($replacements), array_values($replacements), $textString);
    }
}
