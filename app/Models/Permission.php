<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Permission as SpatiePermission;

class Permission extends SpatiePermission
{
    /**
     * Scope the model query to certain user(and team) only.
     *
     * @param  Builder<Permission>  $query
     * @param  int|string|Model  $userId
     * @param  null|int|string|Model  $companyId
     * @return \Illuminate\Database\Eloquent\Builder<\App\Models\Permission>
     */
    public function scopeUser(Builder $query, $userId, $companyId = null): Builder
    {
        if ($userId instanceof Model) {
            $userId = $userId->getKey();
        }

        if ($companyId instanceof Model) {
            $companyId = $companyId->getKey();
        }

        /** @var bool $teams */
        $teams = config('permission.teams');
        $columnNames = config('permission.column_names');
        $teamsKey = $columnNames['team_foreign_key'];

        return $query->whereHas('users', function (Builder $query) use ($userId, $companyId, $teams, $teamsKey): void {
            $query->where(config('permission.table_names.model_has_permissions') . '.' . config('permission.column_names.model_morph_key'), $userId);

            if ($teams) {
                $query->where(config('permission.table_names.model_has_permissions') . '.' . $teamsKey, $companyId ?? getPermissionsTeamId());
            }
        });
    }
}
