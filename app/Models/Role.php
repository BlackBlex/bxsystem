<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Role as SpatieRole;

class Role extends SpatieRole
{
    /**
     * Scope the model query to certain permissions only.
     *r
     *
     * @param  \Illuminate\Database\Eloquent\Builder<\App\Models\Role>  $query
     * @param  string|int|array<Model|int>|Permission|\Illuminate\Support\Collection|\BackedEnum  $permissions
     * @return \Illuminate\Database\Eloquent\Builder<\App\Models\Role>
     */
    public function scopeExactPermission(Builder $query, $permissions): Builder
    {
        $permissions = $this->convertToPermissionModels($permissions);

        /**
         * @psalm-suppress InvalidStringClass
         *
         * @var \Spatie\Permission\Models\Permission $permissionModel
         */
        $permissionModel = (new ($this->getPermissionClass())());
        $permissionKey = $permissionModel->getKeyName();

        $resultQuery = $query->has('permissions', '=', count($permissions))
            ->whereHas(
                'permissions',
                fn (Builder $query) => $query->distinct()->whereIn(config('permission.table_names.permissions') . ".$permissionKey", \array_column($permissions, $permissionKey)),
                '=',
                count($permissions)
            );

        return $resultQuery;
    }

    /**
     * Scope the model query to certain user(and team) only.
     *
     * @param  \Illuminate\Database\Eloquent\Builder<\App\Models\Role>  $query
     * @param  int|string|\Illuminate\Database\Eloquent\Model  $userId
     * @param  bool|int|string|\Illuminate\Database\Eloquent\Model  $companyId
     * @return \Illuminate\Database\Eloquent\Builder<\App\Models\Role>
     */
    public function scopeUser(Builder $query, $userId, $companyId = null): Builder
    {
        if ($userId instanceof Model) {
            $userId = $userId->getKey();
        }

        if ($companyId instanceof Model) {
            $companyId = $companyId->getKey();
        }

        /** @var bool $teams */
        $teams = config('permission.teams');
        $columnNames = config('permission.column_names');
        $teamsKey = $columnNames['team_foreign_key'];

        if ($teams) {
            $companyId = $companyId ?? getPermissionsTeamId();

            $query->where(function (Builder $query) use ($companyId, $teamsKey): void {
                $query->whereNull($teamsKey)
                    ->orWhere($teamsKey, $companyId);
            });
        }

        return $query->whereHas('users', function (Builder $query) use ($userId, $companyId, $teams, $teamsKey): void {
            $query->where(config('permission.table_names.model_has_roles') . '.' . config('permission.column_names.model_morph_key'), $userId);

            if ($teams) {
                $query->where(config('permission.table_names.model_has_roles') . '.' . $teamsKey, $companyId);
            }
        });
    }
}
