<?php

declare(strict_types=1);

namespace App\Livewire;

use App\Models\Comment;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Model;
use Illuminate\View\View as ViewView;
use Livewire\Attributes\On;
use Livewire\Component;
use Livewire\WithPagination;

class Comments extends Component
{
    use WithPagination;

    public string $classes = 'w-full';

    /**
     * @var Model
     */
    public $model;

    /**
     * @var array<string, string>
     */
    public array $newCommentState = [
        'body' => '',
    ];

    protected $listeners = [
        'refresh' => '$refresh',
    ];

    /**
     * @var array<string, string>
     */
    protected array $validationAttributes = [
        'newCommentState.body' => 'comment',
    ];

    #[On('refresh')]
    public function postComment(): void
    {
        $this->validate([
            'newCommentState.body' => 'required',
        ]);

        if (method_exists($this->model, 'comments')) {
            /** @var Comment $comment */
            $comment = $this->model->comments()->make($this->newCommentState);
            $comment->save();

            $this->newCommentState = [
                'body' => '',
            ];

            $this->resetPage();
            session()->flash('message', __('comment_success'));
        }
    }

    public function render(): View|ViewView
    {
        $comments = [];

        if (method_exists($this->model, 'comments')) {
            $comments = $this->model
                ->comments()
                ->with('commentator', 'children.commentator', 'children.children')
                ->parent()
                ->latest()
                ->paginate(config('comments.pagination_count', 10));
        }

        return view('livewire.comments')->with([
            'comments' => $comments,
        ]);
    }
}
