<?php

declare(strict_types=1);

namespace App\Livewire;

use App\Contracts\HasCommentSearchByUser;
use App\Models\Comment as ModelComment;
use App\Traits\HasCommentModels;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Str;
use Illuminate\View\View as ViewView;
use Livewire\Attributes\On;
use Livewire\Component;

class Comment extends Component
{
    use AuthorizesRequests;
    use HasCommentModels;

    public ModelComment $comment;

    /**
     * @var array<string, string>
     */
    public array $editState = [
        'body' => '',
    ];

    public bool $hasReplies = false;

    public bool $isEditing = false;

    public bool $isReplying = false;

    /**
     * @var array<string, string>
     */
    public array $replyState = [
        'body' => '',
    ];

    public bool $showOptions = false;

    /**
     * @var \Illuminate\Database\Eloquent\Collection<int, \Illuminate\Database\Eloquent\Model>|\Illuminate\Database\Eloquent\Collection<int, \Illuminate\Database\Query\Builder&static>|\Illuminate\Support\Collection<array-key, mixed>
     */
    public $users;

    /**
     * @var array<string, string>
     */
    protected array $validationAttributes = [
        'replyState.body' => 'Reply',
        'editState.body' => 'Reply',
    ];

    /**
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function deleteComment(): void
    {
        $this->authorize('destroy', $this->comment);
        $this->comment->delete();
        $this->showOptions = false;
    }

    /**
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function editComment(): void
    {
        $this->authorize('update', $this->comment);
        $this->validate([
            'editState.body' => 'required|min:2',
        ]);
        $this->comment->update($this->editState);
        $this->isEditing = false;
        $this->showOptions = false;
    }

    #[On('getUsers')]
    public function getUsers(string $searchTerm): void
    {
        if ($searchTerm !== '') {
            // TODO: Check performance
            $modelUser = $this->userModel();
            $modelUser = new $modelUser();
            if ($modelUser instanceof HasCommentSearchByUser) {
                $this->users = $modelUser->searchUserBy($searchTerm);
            } else {
                $this->users = $this->userModel()::where(config('comments.field_for_search', 'name'), 'LIKE', '%' . $searchTerm . '%')->take(5)->get();
            }
        } else {
            $this->users = new Collection();
        }
    }

    #[On('refresh')]
    public function postReply(): void
    {
        if (! $this->comment->is_parent) {
            return;
        }
        $this->validate([
            'replyState.body' => 'required',
        ]);

        /** @var ModelComment $reply */
        $reply = $this->comment->children()->make($this->replyState);
        $reply->commentable()->associate($this->comment->commentable);
        $reply->save();

        $this->replyState = [
            'body' => '',
        ];
        $this->isReplying = false;
        $this->showOptions = false;
        $this->dispatch('refresh')->self();
    }

    public function render(): View|ViewView
    {
        return view('livewire.comment');
    }

    public function selectUser(string $userName): void
    {
        $body = $this->replyState['body'] !== '' ? $this->replyState['body'] : $this->editState['body'];

        /** @var string $result */
        $result = \Safe\preg_replace('/@(\w+)$/', '@' . str_replace(' ', '_', Str::lower($userName)) . ' ', $body);

        if ($this->replyState['body'] !== '') {
            $this->replyState['body'] = $result;
        } elseif ($this->editState['body'] !== '') {
            $this->editState['body'] = $result;
        }

        $this->users = new Collection();
    }

    public function updatedIsEditing(bool $isEditing): void
    {
        if (! $isEditing) {
            return;
        }

        $this->editState = [
            'body' => $this->comment->body,
        ];
    }
}
