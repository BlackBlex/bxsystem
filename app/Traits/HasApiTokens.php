<?php

declare(strict_types=1);

namespace App\Traits;

use Laravel\Sanctum\HasApiTokens as SanctumHasApiTokens;

trait HasApiTokens
{
    use SanctumHasApiTokens;

    /**
     * Get token with specify name
     *
     * @param  string  $tokenName
     */
    public function tokenWithName($tokenName): string
    {
        return $this->tokens()->where('name', $tokenName)->first()->token ?? '';
    }
}
