<?php

declare(strict_types=1);

namespace App\Traits;

use App\Models\Comment;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphMany;

trait HasComments
{
    use HasCommentModels;

    public function comments(): MorphMany
    {
        return $this->morphMany($this->commentModel(), 'commentable');
    }

    protected static function bootHasComments(): void
    {
        static::deleting(function (Model $model): void {
            if (method_exists($model, 'comments')) {
                $model->comments()->parent()->get()->each(fn (Comment $comment): bool => $comment->delete() ?? false);
            }
        });

        static::restoring(function (Model $model): void {
            if (method_exists($model, 'comments')) {
                $model->comments()->onlyTrashed()->parent()->each(fn (Comment $comment): bool => $comment->restore());
            }
        });
    }
}
