<?php

declare(strict_types=1);

namespace App\Traits;

trait HasCommentAvatar
{
    public function avatar(): string
    {
        return 'https://source.boringavatars.com/beam/120/' . urlencode($this->email);
    }
}
