<?php

declare(strict_types=1);

namespace App\Traits;

use App\Models\Comment;
use Exception;

trait HasCommentModels
{
    /**
     * @return class-string
     *
     * @throws Exception
     */
    public function commentatorModel()
    {
        if (! is_null(config('comments.models.commentator'))) {
            return config('comments.models.commentator');
        }

        if (! is_null(config('auth.providers.users.model'))) {
            return config('auth.providers.users.model');
        }

        throw new Exception('Could not determine the commentator model name.');
    }

    /**
     * @return class-string
     */
    public function commentModel()
    {
        return config('comments.models.comment', Comment::class);
    }

    /**
     * @return class-string
     *
     * @throws Exception
     */
    public function userModel()
    {
        if (! is_null(config('comments.models.user'))) {
            return config('comments.models.user');
        }

        if (! is_null(config('auth.providers.users.model'))) {
            return config('auth.providers.users.model');
        }

        throw new Exception('Could not determine the user model name.');
    }
}
