<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Api\BaseController;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Response;

class AuthController extends BaseController
{
    /**
     * Gives to user the possibility to login (with email, password) and use the api
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $credentials = $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
        ]);

        if (Auth::attempt($credentials)) {
            /** @var \App\Models\User $user */
            $user = Auth::user();
            $tokenExistent = $user->tokenWithName('api');

            return $this->sendResponse('User login successfully', [
                'user' => $user,
                'authorization' => [
                    'token' => ($tokenExistent === '') ? $user->createToken('api')->plainTextToken : $tokenExistent,
                    'type' => 'bearer',
                ],
            ]);
        }

        return $this->sendError('Unauthenticated', [
            'validation' => 'Invalid login credentials',
        ], Response::HTTP_UNAUTHORIZED);
    }

    /**
     * Gives to user the possibility to logout
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        /** @var \App\Models\User */
        $user = Auth::user();
        $user->tokens()->delete();

        return $this->sendResponse('Logged out', [
            'result' => 'Successfully',
        ]);
    }

    /**
     * Gives to user the possibility to obtain himself
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me(Request $request)
    {
        return $this->sendResponse('Ping pong!', [
            'user' => $request->user(),
        ]);
    }

    /**
     * Gives to user the possibility to register (with first and last name, email, password)
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        $request->validate([
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
        ]);

        $user = User::create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        return $this->sendResponse('User created successfully', [
            'user' => $user,
        ]);
    }
}
