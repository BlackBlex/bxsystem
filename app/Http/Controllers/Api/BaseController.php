<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Response;

abstract class BaseController extends Controller
{
    /**
     * return error response.
     *
     * @param  string  $messageError
     * @param  array<mixed, mixed>  $errors
     * @param  int  $responseCode
     * @return \Illuminate\Http\JsonResponse
     */
    final public function sendError($messageError, $errors = [], $responseCode = Response::HTTP_NOT_FOUND)
    {
        $response = [
            'message' => $messageError,
        ];

        if (count($errors) !== 0) {
            $response['errors'] = $errors;
        }

        return response()->json($response, $responseCode);
    }

    /**
     * success response method.
     *
     * @param  string  $message
     * @param  mixed  $data
     * @param  int  $responseCode
     * @return \Illuminate\Http\JsonResponse
     */
    final public function sendResponse($message, $data, $responseCode = Response::HTTP_OK)
    {
        $response = [
            'data' => $data,
            'message' => $message,
        ];

        return response()->json($response, $responseCode);
    }
}
