<?php

declare(strict_types=1);

namespace App\Contracts;

interface HasCommentSearchByUser
{
    /**
     * @return \Illuminate\Database\Eloquent\Collection<int, \Illuminate\Database\Eloquent\Model>|\Illuminate\Database\Eloquent\Collection<int, \Illuminate\Database\Query\Builder&static>|\Illuminate\Support\Collection<array-key, mixed>
     */
    public function searchUserBy(string $value);
}
