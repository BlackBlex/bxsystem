<?php

declare(strict_types=1);

namespace App\Providers\Filament;

use App\Filament\Pages\Auth\EditProfile;
use App\Filament\Providers\Avatar\BoringAvatars;
use Filament\Http\Middleware\Authenticate;
use Filament\Http\Middleware\DisableBladeIconComponents;
use Filament\Http\Middleware\DispatchServingFilamentEvent;
use Filament\Navigation\NavigationItem;
use Filament\Panel;
use Filament\PanelProvider;
use Filament\Support\Colors\Color;
use Filament\Widgets;
use Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse;
use Illuminate\Cookie\Middleware\EncryptCookies;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken;
use Illuminate\Routing\Middleware\SubstituteBindings;
use Illuminate\Session\Middleware\AuthenticateSession;
use Illuminate\Session\Middleware\StartSession;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\Middleware\ShareErrorsFromSession;
use Modules\Users\Filament\Pages;

class AdminPanelProvider extends PanelProvider
{
    public function panel(Panel $panel): Panel
    {
        return $panel
            ->default()
            ->id('admin')
            ->path('admin')
            ->permission([Panel::BYPASSPERMISSION])
            ->login()
            ->registration(null)
            ->passwordReset()
            ->emailVerification()
            ->profile(EditProfile::class)
            ->colors([
                'primary' => Color::Red,
            ])
            ->defaultAvatarProvider(BoringAvatars::class)
            ->discoverResources(in: app_path('Filament/Admin/Resources'), for: 'App\\Filament\\Admin\\Resources')
            ->discoverPages(in: app_path('Filament/Admin/Pages'), for: 'App\\Filament\\Admin\\Pages')
            ->pages([
                Pages\Dashboard::class,
            ])
            ->discoverWidgets(in: app_path('Filament/Admin/Widgets'), for: 'App\\Filament\\Admin\\Widgets')
            ->widgets([
                Widgets\AccountWidget::class,
                // Widgets\FilamentInfoWidget::class,
            ])
            ->middleware([
                EncryptCookies::class,
                AddQueuedCookiesToResponse::class,
                StartSession::class,
                AuthenticateSession::class,
                ShareErrorsFromSession::class,
                VerifyCsrfToken::class,
                SubstituteBindings::class,
                DisableBladeIconComponents::class,
                DispatchServingFilamentEvent::class,
            ])
            ->authMiddleware([
                Authenticate::class,
            ])->navigationItems([
                NavigationItem::make('users')
                    ->label(fn (): string => __('users'))
                    ->url(fn (): string => '/admin/users')
                    ->icon('heroicon-o-user-group')
                    ->visible(function (): bool {
                        /** @var \App\Models\User $user */
                        $user = Auth::user();

                        return $user->hasAnyPermission(\Modules\Users\Providers\Filament\AdminPanelProvider::PERMISSIONS);
                    }),
                NavigationItem::make('customfields')
                    ->label(fn (): string => __('custom_fields'))
                    ->url(fn (): string => '/admin/customfields')
                    ->icon('heroicon-o-rectangle-stack')
                    ->visible(function (): bool {
                        /** @var \App\Models\User $user */
                        $user = Auth::user();

                        return $user->hasAnyPermission(\Modules\CustomFields\Providers\Filament\AdminPanelProvider::PERMISSIONS);
                    }),
                NavigationItem::make('fileshare')
                    ->label(fn (): string => __('file_share'))
                    ->url(fn (): string => '/admin/fileshare')
                    ->icon('forkawesome-file-text')
                    ->visible(function (): bool {
                        /** @var \App\Models\User $user */
                        $user = Auth::user();

                        return $user->hasAnyPermission(\Modules\FileShare\Providers\Filament\AdminPanelProvider::PERMISSIONS);
                    }),
            ]);
    }
}
