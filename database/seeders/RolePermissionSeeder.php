<?php

declare(strict_types=1);

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Artisan;

class RolePermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //Generate permissions
        Artisan::call('shield:generate --option=permissions --all');

        //Assign the super_admin role to first user
        Artisan::call('shield:super-admin --user=1');
    }
}
