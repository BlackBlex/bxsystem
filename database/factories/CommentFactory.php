<?php

declare(strict_types=1);

namespace Database\Factories;

use App\Models\Comment;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Eloquent\Model;

/**
 * @template TModel of Comment
 *
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<TModel>
 */
class CommentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var class-string<TModel>
     */
    protected $model = Comment::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'body' => fake()->text,
            'commentator_id' => null,
            'parent_id' => null,
            'approved' => true,
            'created_at' => now(),
        ];
    }

    public function forCommentable(Model $commentable): static
    {
        return $this->state(function () use ($commentable) {
            return [
                'commentable_type' => $commentable::class,
                'commentable_id' => $commentable->getKey(),
            ];
        });
    }

    public function forCommentator(Model $commentator): static
    {
        return $this->state(function () use ($commentator) {
            return [
                'commentator_id' => $commentator->getKey(),
            ];
        });
    }

    public function notApproved(): static
    {
        return $this->state(fn (array $_attributes) => [
            'approved' => false,
        ]);
    }
}
