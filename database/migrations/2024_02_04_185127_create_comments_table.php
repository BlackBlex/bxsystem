<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('comments');
    }

    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('comments', function (Blueprint $table): void {
            $table->id();
            $table->unsignedBigInteger('commentator_id')->nullable();
            $table->foreignId('parent_id')->nullable()->constrained('comments');
            $table->text('body');
            $table->morphs('commentable');
            $table->boolean('approved');
            $table->timestamps();
            $table->softDeletes();
        });
    }
};
