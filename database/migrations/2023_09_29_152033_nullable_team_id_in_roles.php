<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class() extends Migration
{
    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        $teams = config('permission.teams');
        if ($teams) {
            $columnNames = config('permission.column_names');
            $pivotRole = $columnNames['role_pivot_key'] ?? 'role_id';
            $pivotPermission = $columnNames['permission_pivot_key'] ?? 'permission_id';

            Schema::table(config('permission.table_names.model_has_permissions'), function (Blueprint $table): void {
                $table->dropPrimary('model_has_permissions_permission_model_type_primary');
                $table->unsignedBigInteger(config('permission.column_names.team_foreign_key'))->nullable(false)->change();
            });

            Schema::table(config('permission.table_names.model_has_permissions'), function (Blueprint $table) use ($pivotPermission): void {
                $table->primary(
                    [config('permission.column_names.team_foreign_key'), $pivotPermission, config('permission.column_names.model_morph_key'), 'model_type'],
                    'model_has_permissions_permission_model_type_primary'
                );
            });

            Schema::table(config('permission.table_names.model_has_roles'), function (Blueprint $table): void {
                $table->dropPrimary('model_has_roles_role_model_type_primary');
                $table->unsignedBigInteger(config('permission.column_names.team_foreign_key'))->nullable(false)->change();
            });

            Schema::table(config('permission.table_names.model_has_roles'), function (Blueprint $table) use ($pivotRole): void {
                $table->primary(
                    [config('permission.column_names.team_foreign_key'), $pivotRole, config('permission.column_names.model_morph_key'), 'model_type'],
                    'model_has_roles_role_model_type_primary'
                );
            });
        }
    }

    /**
     * Run the migrations.
     */
    public function up(): void
    {
        $teams = config('permission.teams');
        if ($teams) {
            $columnNames = config('permission.column_names');
            $pivotRole = $columnNames['role_pivot_key'] ?? 'role_id';
            $pivotPermission = $columnNames['permission_pivot_key'] ?? 'permission_id';

            Schema::table(config('permission.table_names.model_has_permissions'), function (Blueprint $table) use ($pivotPermission): void {
                $table->dropPrimary('model_has_permissions_permission_model_type_primary');
                $table->primary(
                    [$pivotPermission, config('permission.column_names.model_morph_key'), 'model_type'],
                    'model_has_permissions_permission_model_type_primary'
                );

                $schemaManager = Schema::getConnection()->getDoctrineSchemaManager();
                $indexesFound = $schemaManager->listTableIndexes(config('permission.table_names.model_has_permissions'));

                if (! array_key_exists('model_has_permissions_permission_id_foreign', $indexesFound)) {
                    $table->index($pivotPermission, 'model_has_permissions_permission_id_foreign');
                }
            });

            Schema::table(config('permission.table_names.model_has_permissions'), function (Blueprint $table): void {
                $table->unsignedBigInteger(config('permission.column_names.team_foreign_key'))->nullable()->change();
            });

            Schema::table(config('permission.table_names.model_has_roles'), function (Blueprint $table) use ($pivotRole): void {
                $table->dropPrimary('model_has_roles_role_model_type_primary');
                $table->primary(
                    [$pivotRole, config('permission.column_names.model_morph_key'), 'model_type'],
                    'model_has_roles_role_model_type_primary'
                );

                $schemaManager = Schema::getConnection()->getDoctrineSchemaManager();
                $indexesFound = $schemaManager->listTableIndexes(config('permission.table_names.model_has_roles'));

                if (! array_key_exists('model_has_roles_role_id_foreign', $indexesFound)) {
                    $table->index($pivotRole, 'model_has_roles_role_id_foreign');
                }
            });

            Schema::table(config('permission.table_names.model_has_roles'), function (Blueprint $table): void {
                $table->unsignedBigInteger(config('permission.column_names.team_foreign_key'))->nullable()->change();
            });
        }
    }
};
