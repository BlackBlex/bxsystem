<?php

declare(strict_types=1);

use App\Models\Comment;

return [
    'automatically_approve_comment' => true,
    'pagination_count' => 5,
    'field_for_search' => 'full_name',
    'profile_route' => 'users',
    'commentator_through_user' => 'user',

    'models' => [
        'comment' => Comment::class,
        'commentator' => null,
        'user' => null,
    ],
];
