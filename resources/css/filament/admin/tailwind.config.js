import preset from '../../../../vendor/filament/filament/tailwind.config.preset'

export default {
    presets: [preset],
    content: [
        './app/Filament/Admin/**/*.php',
        './app/Livewire/**/*.php',
        './app/Models/Presenters/*.php',
        './resources/views/filament/admin/**/*.blade.php',
        './resources/views/livewire/**/*.blade.php',
        './resources/views/livewire/*.blade.php',
        './vendor/filament/**/*.blade.php',
    ],
}
