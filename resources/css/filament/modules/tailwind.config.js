import preset from '../../../../vendor/filament/filament/tailwind.config.preset'

export default {
    presets: [preset],
    content: [
        './Modules/**/Filament/Admin/**/*.php',
        './resources/views/modules/**/filament/admin/**/*.blade.php',
        './vendor/filament/**/*.blade.php',
    ],
}
