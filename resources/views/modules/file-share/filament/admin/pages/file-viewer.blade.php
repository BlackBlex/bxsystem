<x-filament-panels::page>
    <div>
        <div class="row justify-content-center">
            <iframe class="mb-4 h-[600px] w-full border" id="framePDF"
                src="{{ $url }}#view=FitH&toolbar=0&navpanes=0" type="application/pdf">
                <p>This browser does not support PDFs. Please download the PDF to view it: <a
                        href="{{ $url }}">Download PDF</a></p>
            </iframe>
            <div class="flex justify-center gap-x-4">
                @if ($this->signFileAction->isVisible())
                    {{ $this->signFileAction }}
                @endif
                @if ($this->unsignFileAction->isVisible())
                    {{ $this->unsignFileAction }}
                @endif
                @if ($this->currentFile->status->value === 'reviewed_need')
                    @if ($this->downloadWithAction->isVisible())
                        {{ $this->downloadWithAction }}
                    @endif
                @else
                    @if ($this->downloadFileAction->isVisible())
                        {{ $this->downloadFileAction }}
                    @endif
                @endif
                @if ($this->reviewFileAction->isVisible())
                    {{ $this->reviewFileAction }}
                @endif

                @if ($this->authorizeFileAction->isVisible() || $this->disallowFileAction->isVisible())
                    <div class="grow">
                    </div>
                @endif

                @if ($this->authorizeFileAction->isVisible())
                    {{ $this->authorizeFileAction }}
                @endif

                @if ($this->disallowFileAction->isVisible())
                    {{ $this->disallowFileAction }}
                @endif

                @if ($this->deleteFileAction->isVisible())
                    <div class="grow">
                    </div>
                @endif

                @if ($this->deleteFileAction->isVisible())
                    {{ $this->deleteFileAction }}
                @endif
            </div>
            <div class="flex items-start gap-x-8 justify-center py-4">
                <livewire:comments :model="$currentFile" classes="flex-1 w-full" />
                <div class="w-1/3 flex-none">
                    <h4>{{ __('actions') }}</h4>
                    <div class="m-2">
                        here
                    </div>
                </div>
            </div>
        </div>
</x-filament-panels::page>
