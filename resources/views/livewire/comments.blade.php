<div class="{{ $classes }}">
    <section>
        <div class="mx-auto">
            <div class="mb-4 flex items-center justify-between">
                <h2 class="flex gap-x-1 text-lg font-bold lg:text-2xl">
                    @svg('heroicon-m-chat-bubble-bottom-center-text', 'size-6 mt-1')
                    {{ __('comments') }}
                    ({{ $comments->total() }})</h2>
            </div>
            @auth
                @include('livewire.partials.comment-form', [
                    'method' => 'postComment',
                    'state' => 'newCommentState',
                    'inputId' => 'comment',
                    'inputLabel' => __('comment_your_comment'),
                    'button' => __('comment_button'),
                ])
            @else
                <a class="mt-2 text-sm" href="/login">{{ __('comment_login') }}</a>
            @endauth
            @if ($comments->count())
                <div class="mb-2 flex flex-col gap-y-2">
                    @foreach ($comments as $comment)
                        <livewire:comment :$comment :key="$comment->id" />
                    @endforeach
                </div>
                <x-filament::pagination :paginator="$comments" extreme-links />
            @else
                <p>{{ __('comment_no_yet') }}</p>
            @endif
        </div>
    </section>
</div>
