<form class="mb-6" wire:submit="{{ $method }}">
    @if (session()->has('message'))
        <div x-data="{ show: true }" x-show="show" x-init="setTimeout(() => show = false, 3000)">
            <div class="mb-4 rounded-lg bg-success-500 p-4 text-sm text-white dark:bg-success-400" role="alert">
                <span class="font-medium">{{ session('message') }}</span>
            </div>
        </div>
    @endif
    @csrf
    <div class="mb-4 flex items-start rounded-lg border border-gray-200 bg-white px-4 py-2 dark:border-gray-700 dark:bg-gray-800">
        <label class="sr-only" for="{{ $inputId }}">{{ $inputLabel }}</label>
        <textarea
            class="@error($state . '.body')
                              border-red-500 @enderror w-full border-0 px-0 text-sm text-gray-900 focus:outline-none focus:ring-0 dark:bg-gray-800 dark:text-white dark:placeholder-gray-400"
            id="{{ $inputId }}" rows="6" placeholder="{{ __('comment_write') }}"
            wire:model.live="{{ $state }}.body" oninput="detectAtSymbol()"></textarea>
        @if (!empty($users) && $users->count() > 0)
            @include('livewire.partials.dropdowns.users')
        @endif
        @error($state . '.body')
            <p class="mt-2 text-sm text-red-600">
                {{ $message }}
            </p>
        @enderror
    </div>

    <button
        class="inline-flex items-center gap-x-1 place-self-end rounded-lg bg-primary-500 px-4 py-2.5 text-center text-xs font-medium text-white hover:bg-primary-600 focus:ring-4 focus:ring-primary-200 dark:bg-primary-400 dark:hover:bg-primary-500 dark:focus:ring-primary-900"
        type="submit" wire:loading.attr="disabled">
        <div wire:loading wire:target="{{ $method }}">
            <x-filament::loading-indicator class="size-5 inline" />
        </div>
        {{ $button }}
    </button>

</form>
