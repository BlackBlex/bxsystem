<div class="z-10 w-60 rounded-lg bg-white shadow dark:bg-gray-700 absolute inline-block mt-10">
    <ul class="h-auto overflow-y-auto py-2 text-gray-700 dark:text-gray-200">
        @foreach ($users as $user)
            <li wire:click="selectUser('{{ $user->{config('comments.field_for_search', 'name')} }}')" class="cursor-pointer" wire:key="{{ $user->id }}">
                <a class="flex items-center px-4 py-2 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white">
                    <img class="mr-2 size-6 rounded-full" src="{{ $user->avatar() }}" alt="{{ $user->{config('comments.field_for_search', 'name')} }}">
                    {{ $user->{config('comments.field_for_search', 'name')} }}
                </a>
            </li>
        @endforeach
    </ul>
</div>
