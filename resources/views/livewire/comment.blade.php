<div>
    @if ($isEditing)
        @include('livewire.partials.comment-form', [
            'method' => 'editComment',
            'state' => 'editState',
            'inputId' => 'reply-comment',
            'inputLabel' => __('comment_your_comment'),
            'button' => __('comment_button_edit'),
        ])
    @else
        <article class="rounded-lg bg-white px-4 py-2 text-base dark:bg-gray-900">
            <div class="mb-1 flex items-center justify-between">
                <div class="flex items-center gap-x-2">
                    <x-filament::avatar src="{{ $comment->commentator->avatar() }}"
                        alt="{{ $comment->commentator->{config('comments.field_for_search', 'name')} }}"
                        size="size-10 ring-2 ring-primary-500 ring-offset-2 ring-offset-gray-50 dark:ring-primary-400 dark:ring-offset-gray-900" />
                    <div>
                        <p class="text-sm font-medium text-gray-900 dark:text-white">
                            {{ Str::ucfirst($comment->commentator->{config('comments.field_for_search', 'name')}) }}
                        </p>
                        <p class="-mt-1 text-sm text-gray-600 dark:text-gray-400">
                            <time title="{{ $comment->updated_at }}" pubdate
                                datetime="{{ $comment->presenter()->relativeCreatedAt() }}">
                                {{ $comment->presenter()->relativeCreatedAt() }}
                            </time>

                            @if ($comment->is_edited)
                                <span>
                                    ({{ __('edited') }})
                                </span>
                            @endif
                        </p>
                    </div>
                </div>
                <div class="relative">
                    @if (auth()->user()->can('update', $comment) &&
                            auth()->user()->can('delete', $comment))
                        <button
                            class="inline-flex items-center rounded-lg bg-white p-2 text-center text-sm font-medium text-gray-400 hover:bg-gray-100 focus:outline-none focus:ring-4 focus:ring-gray-50 dark:bg-gray-900 dark:hover:bg-gray-700 dark:focus:ring-gray-600"
                            type="button" wire:click="$toggle('showOptions')">
                            <svg class="h-5 w-5" aria-hidden="true" fill="currentColor" viewBox="0 0 20 20"
                                xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="M6 10a2 2 0 11-4 0 2 2 0 014 0zM12 10a2 2 0 11-4 0 2 2 0 014 0zM16 12a2 2 0 100-4 2 2 0 000 4z">
                                </path>
                            </svg>
                        </button>
                    @endif
                    <!-- Dropdown menu -->
                    @if ($showOptions)
                        <div
                            class="absolute right-0 top-full z-10 mt-1 w-36 divide-y divide-gray-100 rounded bg-white shadow dark:divide-gray-600 dark:bg-gray-700">
                            <ul class="py-1 text-sm text-gray-700 dark:text-gray-200">
                                @can('update', $comment)
                                    <li>
                                        <button
                                            class="block w-full px-4 py-2 text-left hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white"
                                            type="button" wire:click="$toggle('isEditing')">{{ __('edit') }}
                                        </button>
                                    </li>
                                @endcan
                                @can('delete', $comment)
                                    <li>

                                        <button
                                            class="block w-full px-4 py-2 text-left hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white"
                                            x-on:click="confirmCommentDeletion" x-data="{
                                                confirmCommentDeletion() {
                                                    if (window.confirm('{{ __('comment_delete_confirm') }}')) {
                                                        @this.call('deleteComment')
                                                    }
                                                }
                                            }">
                                            {{ __('delete') }}
                                        </button>
                                    </li>
                                @endcan
                            </ul>
                        </div>
                    @endif
                </div>
            </div>
            <p class="text-gray-500 dark:text-gray-400">
                {!! $comment->presenter()->replaceMentions($comment->presenter()->markdownBody()) !!}
            </p>
            <div class="mt-1 flex items-center gap-x-4">
                {{-- <livewire:like :$comment :key="$comment->id" /> --}}
                @auth
                    @if ($comment->is_parent)
                        <button class="flex items-center gap-x-1 text-sm text-gray-500 hover:underline dark:text-gray-400"
                            type="button" wire:click="$toggle('isReplying')">
                            @svg('heroicon-m-chat-bubble-left-right', 'size-4')
                            {{ __('reply') }}
                        </button>
                        <div wire:loading wire:target="$toggle('isReplying')">
                            <x-filament::loading-indicator class="size-5 inline" />
                        </div>
                    @endif
                @endauth
                @if ($comment->children->count())
                    <button class="flex items-center gap-x-1 text-sm text-gray-500 hover:underline dark:text-gray-400"
                        type="button" wire:click="$toggle('hasReplies')">
                        @if (!$hasReplies)
                            @svg('heroicon-m-eye', 'size-4') {{ __('comment_view_replies') }} ({{ $comment->children->count() }})
                        @else
                            @svg('heroicon-m-eye-slash', 'size-4') {{ __('comment_hide_replies') }}
                        @endif
                    </button>
                    <div wire:loading wire:target="$toggle('hasReplies')">
                        <x-filament::loading-indicator class="size-5 inline" />
                    </div>
                @endif
            </div>
        </article>
    @endif
    @if ($isReplying)
        @include('livewire.partials.comment-form', [
            'method' => 'postReply',
            'state' => 'replyState',
            'inputId' => 'reply-comment',
            'inputLabel' => 'Your Reply',
            'button' => 'Post Reply',
        ])
    @endif
    @if ($hasReplies)
        <article class="ml-1 flex flex-col gap-y-1 rounded-lg pt-1 text-base lg:ml-12">
            @foreach ($comment->children as $child)
                <livewire:comment :comment="$child" :key="$child->id" />
            @endforeach
        </article>
    @endif
    <script>
        function detectAtSymbol() {
            const textarea = document.getElementById('reply-comment');

            // Check if the textarea element exists
            if (!textarea) {
                console.warn("Couldn't find the 'reply-comment' element.");
                return;
            }

            const cursorPosition = textarea.selectionStart;
            const textBeforeCursor = textarea.value.substring(0, cursorPosition);
            const atSymbolPosition = textBeforeCursor.lastIndexOf('@');

            if (atSymbolPosition !== -1) {
                const searchTerm = textBeforeCursor.substring(atSymbolPosition + 1);

                // if (searchTerm.trim().length > 0) {
                    window.Livewire.dispatch('getUsers', {
                        'searchTerm': searchTerm
                    });
                // }
            }
        }
    </script>
</div>
