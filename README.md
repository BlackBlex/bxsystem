<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

<!-- <p align="center">
<a href="https://github.com/laravel/framework/actions"><img src="https://github.com/laravel/framework/workflows/tests/badge.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p> -->

## BxSystem _Multiple systems for differents things_

Is a web application with multiple systems. You will be able to manage the HR process, inventory, purchase orders, reports, and many other things

It's made with:

-   Language: **PHP v8.2**
-   Design: **Blade**
-   Framework: **Laravel v10.29**
-   Libraries: **Livewire v3**, **AlpineJS v3**, **TailwindCSS v3**, **alajusticia/Laravel-expirable v2**, **bezhansalleh/filament-shield v3.1**, **coolsam/modules v3.x-dev**, **filament/filament v3**, **laravel/sanctum v3.3**, **nwidart/laravel-modules v10**, **spatie/laravel-permission v6.1**, **thecodingmachine/safe v2.5**, **usamamuneerchaudhary/Commentify vdev-main**
-   Libraries dev: **amirami/localizator v0.12**, **barryvdh/laravel-ide-helper v2.1**, **fakerphp/faker v1.23**, **laravel/sail v1.25**, **mockery/mockery v1.6**, **nunomaduro/collision v7.10**, **pestphp/pest v2.24**, **pestphp/pest-plugin-faker v2.0**, **pestphp/pest-plugin-laravel v2.2**, **pestphp/pest-plugin-livewire v2.1**, **spatie/laravel-ignition v2.3**, **nunomaduro/larastan v2.6**, **laravel/pint v1.13**, **ergebnis/phpstan-rules v2.1**, **phpstan/extension-installer V1.3**, **phpstan/phpstan-deprecation-rules V1.1**, **phpstan/phpstan-strict-rules V1.5**, **thecodingmachine/phpstan-safe-rule V1.2**, **psalm/plugin-laravel v2.8**

---

Features in this version:

-   User system (register and login)
-   Roles and permission
-   Dashboard and multiples CRUDS

<!-- ## Images -->

## How to run

**Unfortunately there will be no releases, until it becomes a stable version**

Read more in [Your First Code Contribution](https://gitlab.com/BlackBlex/bxsystem/blob/main/CONTRIBUTING.md#your-first-code-contribution)

## Contributing

In favor of active development we accept contributions for everyone. You can contribute by submitting a bug, creating pull requests or even improving documentation
Read more in [Contributing](https://gitlab.com/BlackBlex/bxsystem/blob/main/CONTRIBUTING.md) page

## License

BxSystem is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
