<?php

declare(strict_types=1);

namespace Tests;

class UtilIO
{
    public const BASE_DIR = __DIR__ . '/tmp/';

    protected static ?self $instance = null;

    public function __construct()
    {
        if (! file_exists(self::BASE_DIR)) {
            \Safe\mkdir(self::BASE_DIR, 0755, true);
        }
    }

    public static function getInstance(): static
    {
        if (self::$instance === null) {
            self::$instance = new UtilIO();
        }

        return self::$instance;
    }

    /**
     * Check if file exist in the path ./tmp/
     *
     * @param  string  $filePath  file path to check
     *
     **/
    public function existFile($filePath): bool
    {
        $path = self::validPath($filePath);

        if ($path !== '') {
            return file_exists($path);
        }

        return false;
    }

    /**
     * Read file if exist in the path ./tmp/$filePath
     *
     * @param  string  $filePath  file path to read
     *
     **/
    public function readFile($filePath): ?string
    {
        $path = self::validPath($filePath);

        if ($path === '' || ! self::existFile($filePath)) {
            return null;
        }

        return \Safe\file_get_contents($path);
    }

    /**
     * Check if given file path is valid in: ./tmp/$filePath
     *
     * @param  string  $filePath  File path to check
     *
     **/
    public function validPath($filePath): string
    {
        // $pathJoin = self::BASE_DIR . $filePath;
        $pathSanitize = self::BASE_DIR . self::sanitizeFilePath($filePath);

        return $pathSanitize;

        // if ($pathJoin === $pathSanitize) {
        //     return $pathSanitize;
        // }

        // return '';
    }

    /**
     * Write file in the path ./tmp/$filePath
     *
     * @param  string  $filePath  file path to write
     * @param  mixed  $data  Information to put into file
     *
     **/
    public function writeFile($filePath, $data): int
    {
        $path = self::validPath($filePath);

        if ($path !== '') {
            return \Safe\file_put_contents($path, $data);
        }

        return -1;
    }

    /**
     * Sanitize file path
     *
     * @param  string  $filePath  File path to sanitize
     *
     **/
    private function sanitizeFilePath($filePath): string
    {
        $info = pathinfo($filePath);

        preg_match_all('/\.?[a-z]+/i', $filePath, $matches);
        $setOfStrings = $matches[0];

        $basename = $info['basename'];
        $name = $info['filename'];
        $ext = $info['extension'] ?? '';

        if ($ext !== '') {
            if (count($setOfStrings) > 1 && ! str_starts_with($basename, '.')) {
                if (str_contains($setOfStrings[array_key_last($setOfStrings)], $ext)) {
                    array_pop($setOfStrings);
                    $setOfStrings[array_key_last($setOfStrings)] .= '.' . $ext;
                }
            } else {
                $dotFilepath = '.' . $ext;
                if ($basename === $dotFilepath) {
                    $setOfStrings[array_key_last($setOfStrings)] = $dotFilepath;
                }
            }
        }

        return implode(DIRECTORY_SEPARATOR, $setOfStrings);
    }
}
