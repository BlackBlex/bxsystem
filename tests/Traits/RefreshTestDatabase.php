<?php

declare(strict_types=1);

namespace Tests\Traits;

use Illuminate\Contracts\Console\Kernel;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\RefreshDatabaseState;
use Symfony\Component\Finder\Finder;

trait RefreshTestDatabase
{
    use RefreshDatabase;

    protected function refreshTestDatabase(): void
    {
        if (! RefreshDatabaseState::$migrated) {
            $this->runMigrationsIfNecessary();

            $this->app[Kernel::class]->setArtisan(null);

            RefreshDatabaseState::$migrated = true;
        }

        $this->beginDatabaseTransaction();
    }

    protected function runMigrationsIfNecessary(): void
    {
        if ($this->identicalChecksum() === false) {
            $this->artisan('migrate:fresh', $this->migrateFreshUsing());

            $this->createChecksum();
        }
    }

    /**
     * Calculate checksum from database directory excluding factories and seeders directory
     **/
    private function calculateChecksum(): string
    {
        $files = Finder::create()
            ->files()
            ->exclude([
                'factories',
                'Factories',
                'seeders',
                'Seeders',
            ])
            ->in([database_path(), base_path('Modules') . DIRECTORY_SEPARATOR . '*' . DIRECTORY_SEPARATOR . 'Database'])
            ->ignoreDotFiles(true)
            ->ignoreVCS(true)
            ->getIterator();

        $files = array_keys(iterator_to_array($files));

        $checksum = collect($files)->map(fn ($file) => md5_file($file))->implode('');

        return md5($checksum);
    }

    private function checksumFileContents(): bool|string
    {
        return file_get_contents($this->checksumFilePath());
    }

    private function checksumFilePath(): string
    {
        return base_path('.pest.database.checksum');
    }

    private function createChecksum(): void
    {
        file_put_contents($this->checksumFilePath(), $this->calculateChecksum());
    }

    private function identicalChecksum(): bool
    {
        if ($this->isChecksumExists() === false) {
            return false;
        }

        if ($this->checksumFileContents() === $this->calculateChecksum()) {
            return true;
        }

        return false;
    }

    private function isChecksumExists(): bool
    {
        return file_exists($this->checksumFilePath());
    }
}
