<?php

declare(strict_types=1);

namespace Tests;

use App\Models\User;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    /**
     * Create a user if not exist
     *
     * @param  string  $column
     * @param  mixed  $operator
     * @param  mixed  $value
     * @param  array<string, mixed>  $data
     */
    public function createUser($column, $operator = null, $value = null, $data = []): User
    {
        $result = User::where($column, $operator, $value)->first();
        if ($result === null) {
            $result = User::factory()->create($data);
        }

        return $result;
    }
}
