<?php

declare(strict_types=1);

use Symfony\Component\HttpFoundation\Response;

uses(Tests\Traits\RefreshTestDatabase::class);
uses(Tests\TestCase::class);

$firstName = 'Romaine';
$lastName = 'Wehner';
$email = 'romaine.wehner@example.com';
$password = 'password';

$data = ['first_name' => $firstName,
    'last_name' => $lastName,
    'email' => $email,
    'password' => $password];

function makeApiToken(array $userData, int $expectedStatus = Response::HTTP_OK): mixed
{
    /** @var \Illuminate\Testing\TestResponse $loginResponse */
    $loginResponse = test()->postJson('/api/v1/auth/login', $userData);
    $loginResponse->assertStatus($expectedStatus);

    return $loginResponse->json();
}

/** @var \Tests\TestCase $this */
beforeEach(function () use ($firstName, $data): void {
    $this->user = $this->createUser('first_name', $firstName, data: $data);
});

/** @var \Tests\TestCase $this */
test('Can login with correct credentials', function () use ($email, $password): void {
    $userData = [
        'email' => $email,
        'password' => $password,
    ];

    $loginData = makeApiToken($userData);

    /** @var \Pest\Expectation<mixed> $loginExpectation */
    $loginExpectation = expect($loginData);
    $loginExpectation->message->toBe('User login successfully');
});

test('Can´t login with invalid credentials', function () use ($email): void {
    $userData = [
        'email' => $email,
        'password' => 'wrongpassword',
    ];

    $loginData = makeApiToken($userData, Response::HTTP_UNAUTHORIZED);

    /** @var \Pest\Expectation<mixed> $loginExpectation */
    $loginExpectation = expect($loginData);
    $loginExpectation->message->toContain('Unauthenticated')
        ->errors->toHaveKey('validation');
});

test('Can get the user with valid token', function () use ($firstName, $lastName, $email, $password): void {
    $userData = [
        'email' => $email,
        'password' => $password,
    ];

    $loginData = makeApiToken($userData);
    $token = $loginData['data']['authorization']['token'];

    /** @var \Illuminate\Testing\TestResponse $userResponse */
    $userResponse = $this->withToken($token)->getJson('/api/v1/auth/me');
    $userResponse->assertStatus(Response::HTTP_OK);
    $userData = $userResponse->json();

    /** @var \Pest\Expectation<mixed> $userExpectation */
    $userExpectation = expect($userData);
    $userExpectation->data
        ->user->not->toBeNull();

    $userExpectation->data->user->id->toBe($this->user->id);
    $userExpectation->data->user->first_name->toBe($firstName);
    $userExpectation->data->user->last_name->toBe($lastName);
});

test('Can´t get the user with invalid token', function (): void {
    $token = 'invalidToken';

    /** @var \Illuminate\Testing\TestResponse $userResponse */
    $userResponse = $this->withToken($token)->getJson('/api/v1/auth/me');
    $userResponse->assertStatus(Response::HTTP_UNAUTHORIZED);
    $userData = $userResponse->json();

    /** @var \Pest\Expectation<mixed> $userExpectation */
    $userExpectation = expect($userData);
    $userExpectation->message->toContain('Unauthenticated');
});

test('Can´t logout with invalid token', function (): void {
    $token = 'invalidToken';

    /** @var \Illuminate\Testing\TestResponse $userResponse */
    $userResponse = $this->withToken($token)->postJson('/api/v1/auth/logout');
    $userResponse->assertStatus(Response::HTTP_UNAUTHORIZED);
    $userData = $userResponse->json();

    /** @var \Pest\Expectation<mixed> $userExpectation */
    $userExpectation = expect($userData);
    $userExpectation->message->toContain('Unauthenticated');
});

test('Can logout with valid token', function () use ($email, $password): void {
    $userData = [
        'email' => $email,
        'password' => $password,
    ];

    $loginData = makeApiToken($userData);
    $token = $loginData['data']['authorization']['token'];

    /** @var \Illuminate\Testing\TestResponse $userResponse */
    $userResponse = $this->withToken($token)->postJson('/api/v1/auth/logout');
    $userResponse->assertStatus(Response::HTTP_OK);
    $userData = $userResponse->json();

    /** @var \Pest\Expectation<mixed> $userExpectation */
    $userExpectation = expect($userData);
    $userExpectation->message->toContain('Logged out');
});
