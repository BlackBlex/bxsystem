<?php

declare(strict_types=1);

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Support\Collection;
use Tests\Feature\SpatieRoles\TestCase;

uses(Tests\Traits\RefreshTestDatabase::class);
uses(TestCase::class);

test('Roles creation: five roles created', function (): void {
    $this->createRoles();

    /** @var Pest\Expectation expect */
    $expect = expect(Role::get());
    $expect->toBeInstanceOf(Collection::class)->toHaveCount(count($this->roles));
    //expect()->
});

test('Permission creation: four permissions created', function (): void {
    $this->createRoles();
    $this->createPermissions();

    $permissions = Permission::get();

    /** @var Pest\Expectation expect */
    $expect = expect($permissions);
    $expect->toBeInstanceOf(Collection::class)->toHaveCount(4);
});

test('Assign role to user', function (): void {
    $this->createRoles();
    $this->createPermissions();

    $user = $this->createUser('first_name', 'Jovani', data: ['first_name' => 'Jovani']);
    $userOther = $this->createUser('first_name', 'Roberto', data: ['first_name' => 'Roberto']);

    //Prepair to set roles
    if (! $user->hasRole('super-admin')) {
        $user->assignRole('super-admin');
    }

    //Prepair to set roles
    if (! $userOther->hasRole('member')) {
        $userOther->assignRole('member');
    }

    expect($user->hasRole('super-admin'))->toBeTrue();
    expect($userOther->hasRole('member'))->toBeTrue();
});

test('Verify current role from user Jovani', function (): void {
    $this->createRoles();

    $jovaniUser = $this->createUser('first_name', 'Jovani', data: ['first_name' => 'Jovani']);
    //Set roles
    if (! $jovaniUser->hasRole('super-admin')) {
        $jovaniUser->assignRole('super-admin');
    }

    $roleFromJovani = Role::user($jovaniUser)->get()->pluck('name');

    /** @var Pest\Expectation expect */
    $expect = expect($jovaniUser);
    $expect->not()->toBeNull();

    expect($roleFromJovani->toArray())->toContain('super-admin');
});
