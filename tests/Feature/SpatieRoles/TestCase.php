<?php

declare(strict_types=1);

namespace Tests\Feature\SpatieRoles;

use App\Models\Permission;
use App\Models\Role;
use Tests\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    protected array $roles = [
        'super-admin' => [],
        'admin' => [
            'view',
            'create',
            'update',
            'delete',
        ],
        'editor' => [
            'view',
            'create',
            'update',
        ],
        'member' => [
            'view',
        ],
        'another' => [
            'create',
            'update',
            'delete',
        ],
    ];

    public function createPermissions(): void
    {
        collect($this->roles)->each(function ($permissions, $roleName): void {
            $role = Role::findByName($roleName);

            collect($permissions)->each(function ($permission) use ($role): void {
                $permi = Permission::findOrCreate($permission);

                if (! $role->hasPermissionTo($permission)) {
                    /** @var Permission */
                    $role->permissions()->save($permi);
                }
            });
        });
    }

    public function createRoles(): void
    {
        collect($this->roles)->each(function ($permissions, $role): void {
            if (! Role::where('name', $role)->exists()) {
                $role = Role::create(['name' => $role]);
            }
        });
    }
}
