<?php

declare(strict_types=1);

use App\Models\Permission;
use App\Models\Role;
use Tests\Feature\SpatieRoles\TestCase;

uses(Tests\Traits\RefreshTestDatabase::class);
uses(TestCase::class);

beforeEach(function (): void {
    $this->createRoles();
    $this->createPermissions();
});

test('Get member role with exact permissions (view)', function (): void {
    $permissionsID = [Permission::where('name', 'view')->first()->id];

    $roles = Role::exactPermission($permissionsID)->get()->pluck('name');

    expect($roles->toArray())->toBeArray()->toHaveCount(1);
    expect($roles[0])->toBe('member');
});

test('Get editor role with exact permissions (view, create, update)', function (): void {
    $permissionsID = Permission::whereIn('name', ['view', 'create', 'update'])->get()->pluck('id');

    $roles = Role::exactPermission($permissionsID)->get()->pluck('name');

    expect($roles->toArray())->toBeArray()->toHaveCount(1);
    expect($roles[0])->toBe('editor');
});

test('Get another role with exact permissions (delete, create, update)', function (): void {
    $permissionsID = Permission::whereIn('name', ['create', 'delete', 'update'])->get()->pluck('id');

    $roles = Role::exactPermission($permissionsID)->get()->pluck('name');

    expect($roles->toArray())->toBeArray()->toHaveCount(1);
    expect($roles[0])->toBe('another');
});

test('Get admin role with exact permissions (view, delete, create, update)', function (): void {
    $permissionsID = Permission::whereIn('name', ['view', 'create', 'delete', 'update'])->get()->pluck('id');

    $roles = Role::exactPermission($permissionsID)->get()->pluck('name');

    expect($roles->toArray())->toBeArray()->toHaveCount(1);
    expect($roles[0])->toBe('admin');
});

test('No get role with exact permissions', function (): void {
    $permissionsID = Permission::whereIn('name', ['view', 'delete', 'update'])->get()->pluck('id');

    $roles = Role::exactPermission($permissionsID)->get()->pluck('name');

    expect($roles->toArray())->toBeArray()->toHaveCount(0);
});
