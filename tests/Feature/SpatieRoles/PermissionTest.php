<?php

declare(strict_types=1);

use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Support\Collection;
use Tests\Feature\SpatieRoles\TestCase;

uses(Tests\Traits\RefreshTestDatabase::class);
uses(TestCase::class);

beforeEach(function (): void {
    $this->createRoles();
    $this->createPermissions();
});

test('Admin role has view, update, create, delete permissions', function (): void {
    $permissions = Role::where('name', 'admin')->first()->permissions;
    $permissionExpectation = expect($permissions);

    $permissionExpectation->not->toBeNull();
    $permissionExpectation->toBeInstanceOf(Collection::class)->toHaveCount(4);
});

test('Adding new user as editor, get two users with view permission', function (): void {
    $ferdinanUser = $this->createUser('first_name', 'Ferdinan', data: ['first_name' => 'Ferdinan']);
    $ferdinanUser->assignRole('editor');
    $osonUser = $this->createUser('first_name', 'Oson', data: ['first_name' => 'Oson']);
    $osonUser->assignRole('admin');

    $viewPermission = Permission::where('name', '=', 'view')->first();
    $usersWithViewPermission = User::permission($viewPermission)->get();
    $usersNameWithViewPermission = $usersWithViewPermission->pluck('first_name')->toArray();

    expect($viewPermission)->not->toBeNull();
    $usersWithViewExpectation = expect($usersWithViewPermission);
    $usersWithViewExpectation->toBeInstanceOf(Collection::class)->toHaveCount(2);
    expect($usersNameWithViewPermission)->toContain('Oson', 'Ferdinan');
});
