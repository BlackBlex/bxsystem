<?php

declare(strict_types=1);

/* Take from: https://github.com/filamentphp/filament */

use App\Models\User;
use Filament\Facades\Filament;
use Filament\Http\Responses\Auth\Contracts\LogoutResponse;
use Illuminate\Http\RedirectResponse;

uses(Tests\Traits\RefreshTestDatabase::class);
uses(Tests\TestCase::class);

it('can log a user out', function (): void {
    $this->actingAs(User::factory()->create())
        ->post(Filament::getLogoutUrl())
        ->assertRedirect(Filament::getLoginUrl());

    $this->assertGuest();
});

it('allows a user to override the logout response', function (): void {
    $logoutResponseFake = new class() implements LogoutResponse
    {
        public function toResponse($request): RedirectResponse
        {
            return redirect()->to('https://example.com');
        }
    };

    $this->app->instance(LogoutResponse::class, $logoutResponseFake);

    $this->actingAs(User::factory()->create())
        ->post(Filament::getLogoutUrl())
        ->assertRedirect('https://example.com');
});
