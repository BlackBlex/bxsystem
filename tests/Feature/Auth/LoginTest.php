<?php

declare(strict_types=1);

/* Take from: https://github.com/filamentphp/filament */

use Filament\Facades\Filament;
use Filament\Pages\Auth\Login;
use Illuminate\Support\Str;

use function Pest\Livewire\livewire;

uses(Tests\Traits\RefreshTestDatabase::class);
uses(Tests\TestCase::class);

beforeEach(function (): void {
    $this->user = $this->createUser('first_name', 'Jovani', data: ['first_name' => 'Jovani']);
});

test('can render page', function (): void {
    $response = $this->get(Filament::getLoginUrl());
    $response->assertStatus(200);
});

test('can redirect unauthenticated app requests', function (): void {
    $this->get(route('filament.admin.pages.dashboard'))->assertRedirect(Filament::getLoginUrl());
});

test('can login with valid credentials', function (): void {
    $this->assertGuest();

    livewire(Login::class)
        ->fillForm([
            'email' => $this->user->email,
            'password' => 'password',
        ])
        ->call('authenticate')
        ->assertRedirect(Filament::getUrl());

    $this->assertAuthenticatedAs($this->user);
});

test('can\'t login with invalid credentials', function (): void {
    livewire(Login::class)
        ->fillForm([
            'email' => $this->user->email,
            'password' => 'WrongPassword',
        ])
        ->call('authenticate')
        ->assertHasFormErrors(['email']);

    $this->assertGuest();
});

test('can register and redirect user to their intended URL', function (): void {
    session()->put('url.intended', $intendedUrl = Str::random());

    Filament::getCurrentPanel()->requiresEmailVerification(false);

    livewire(Login::class)
        ->fillForm([
            'email' => $this->user->email,
            'password' => 'password',
        ])
        ->call('authenticate')
        ->assertRedirect($intendedUrl);
});

test('can\'t view login page when already login valid credentials', function (): void {
    $this->assertGuest();

    livewire(Login::class)
        ->fillForm([
            'email' => $this->user->email,
            'password' => 'password',
        ])
        ->call('authenticate')
        ->assertRedirect(Filament::getUrl());

    $this->assertAuthenticatedAs($this->user);
    $this->get(Filament::getLoginUrl())->assertRedirect(Filament::getUrl());
});
