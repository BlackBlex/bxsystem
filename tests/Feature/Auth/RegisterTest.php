<?php

declare(strict_types=1);

/* Take from: https://github.com/filamentphp/filament */

use App\Filament\Pages\Auth\Register;
use App\Models\User;
use Filament\Events\Auth\Registered;
use Filament\Facades\Filament;
use Filament\Notifications\Auth\VerifyEmail;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Str;

use function Pest\Livewire\livewire;

uses(Tests\Traits\RefreshTestDatabase::class);
uses(Tests\TestCase::class);

test('Can render page', function (): void {
    $response = $this->get(Filament::getRegistrationUrl());
    $response->assertStatus(200);
})
    ->skip(fn () => ! Filament::hasRegistration(), 'No register enabled');

test('can register', function (): void {
    Notification::fake();
    Event::fake();

    $this->assertGuest();

    Filament::getCurrentPanel()->requiresEmailVerification(false);

    $userToRegister = User::factory()->make();

    livewire(Register::class)
        ->fillForm([
            'first_name' => $userToRegister->first_name,
            'last_name' => $userToRegister->last_name,
            'email' => $userToRegister->email,
            'password' => 'password',
            'passwordConfirmation' => 'password',
        ])
        ->call('register')
        ->assertRedirect(Filament::getUrl());

    Notification::assertSentTimes(VerifyEmail::class, expectedCount: 1);
    Event::assertDispatched(Registered::class);

    $this->assertAuthenticated();

    $this->assertCredentials([
        'email' => $userToRegister->email,
        'password' => 'password',
    ]);
})
    ->skip(fn () => ! Filament::hasRegistration(), 'No register enabled');

test('can register and redirect user to their intended URL', function (): void {
    session()->put('url.intended', $intendedUrl = Str::random());

    Filament::getCurrentPanel()->requiresEmailVerification(false);

    $userToRegister = User::factory()->make();

    livewire(Register::class)
        ->fillForm([
            'first_name' => $userToRegister->first_name,
            'last_name' => $userToRegister->last_name,
            'email' => $userToRegister->email,
            'password' => 'password',
            'passwordConfirmation' => 'password',
        ])
        ->call('register')
        ->assertRedirect($intendedUrl);
})
    ->skip(fn () => ! Filament::hasRegistration(), 'No register enabled');

test('can throttle registration attempts', function (): void {
    Notification::fake();
    Event::fake();

    $this->assertGuest();

    foreach (range(1, 2) as $i) {
        $userToRegister = User::factory()->make();

        livewire(Register::class)
            ->fillForm([
                'first_name' => $userToRegister->first_name,
                'last_name' => $userToRegister->last_name,
                'email' => $userToRegister->email,
                'password' => 'password',
                'passwordConfirmation' => 'password',
            ])
            ->call('register')
            ->assertRedirect(Filament::getUrl());

        $this->assertAuthenticated();

        auth()->logout();
    }

    Notification::assertSentTimes(VerifyEmail::class, expectedCount: 2);
    Event::assertDispatchedTimes(Registered::class, times: 2);

    livewire(Register::class)
        ->fillForm([
            'first_name' => $userToRegister->first_name,
            'last_name' => $userToRegister->last_name,
            'email' => $userToRegister->email,
            'password' => 'password',
            'passwordConfirmation' => 'password',
        ])
        ->call('register')
        ->assertNotified()
        ->assertNoRedirect();

    Notification::assertSentTimes(VerifyEmail::class, expectedCount: 2);
    Event::assertDispatchedTimes(Registered::class, times: 2);

    $this->assertGuest();
})
    ->skip(fn () => ! Filament::hasRegistration(), 'No register enabled');

test('can validate `first name` is required', function (): void {
    livewire(Register::class)
        ->fillForm(['first_name' => ''])
        ->call('register')
        ->assertHasFormErrors(['first_name' => ['required']]);
})
    ->skip(fn () => ! Filament::hasRegistration(), 'No register enabled');

test('can validate `first name` is max 255 characters', function (): void {
    livewire(Register::class)
        ->fillForm(['first_name' => Str::random(256)])
        ->call('register')
        ->assertHasFormErrors(['first_name' => ['max']]);
})
    ->skip(fn () => ! Filament::hasRegistration(), 'No register enabled');

test('can validate `last name` is required', function (): void {
    livewire(Register::class)
        ->fillForm(['last_name' => ''])
        ->call('register')
        ->assertHasFormErrors(['last_name' => ['required']]);
})
    ->skip(fn () => ! Filament::hasRegistration(), 'No register enabled');

test('can validate `last name` is max 255 characters', function (): void {
    livewire(Register::class)
        ->fillForm(['last_name' => Str::random(256)])
        ->call('register')
        ->assertHasFormErrors(['last_name' => ['max']]);
})
    ->skip(fn () => ! Filament::hasRegistration(), 'No register enabled');

test('can validate `email` is required', function (): void {
    livewire(Register::class)
        ->fillForm(['email' => ''])
        ->call('register')
        ->assertHasFormErrors(['email' => ['required']]);
})
    ->skip(fn () => ! Filament::hasRegistration(), 'No register enabled');

test('can validate `email` is valid email', function (): void {
    livewire(Register::class)
        ->fillForm(['email' => 'invalid-email'])
        ->call('register')
        ->assertHasFormErrors(['email' => ['email']]);
})
    ->skip(fn () => ! Filament::hasRegistration(), 'No register enabled');

test('can validate `email` is max 255 characters', function (): void {
    livewire(Register::class)
        ->fillForm(['email' => Str::random(256)])
        ->call('register')
        ->assertHasFormErrors(['email' => ['max']]);
})
    ->skip(fn () => ! Filament::hasRegistration(), 'No register enabled');

test('can validate `email` is unique', function (): void {
    $existingEmail = User::factory()->create()->email;

    livewire(Register::class)
        ->fillForm(['email' => $existingEmail])
        ->call('register')
        ->assertHasFormErrors(['email' => ['unique']]);
})
    ->skip(fn () => ! Filament::hasRegistration(), 'No register enabled');

test('can validate `password` is required', function (): void {
    livewire(Register::class)
        ->fillForm(['password' => ''])
        ->call('register')
        ->assertHasFormErrors(['password' => ['required']]);
})
    ->skip(fn () => ! Filament::hasRegistration(), 'No register enabled');

test('can validate `password` is confirmed', function (): void {
    livewire(Register::class)
        ->fillForm([
            'password' => Str::random(),
            'passwordConfirmation' => Str::random(),
        ])
        ->call('register')
        ->assertHasFormErrors(['password' => ['same']]);
})
    ->skip(fn () => ! Filament::hasRegistration(), 'No register enabled');

test('can validate `passwordConfirmation` is required', function (): void {
    livewire(Register::class)
        ->fillForm(['passwordConfirmation' => ''])
        ->call('register')
        ->assertHasFormErrors(['passwordConfirmation' => ['required']]);
})
    ->skip(fn () => ! Filament::hasRegistration(), 'No register enabled');
