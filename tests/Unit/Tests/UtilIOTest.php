<?php

declare(strict_types=1);

use Tests\UtilIO;

uses(Tests\TestCase::class);

test('Get a valid path with a normal path', function (): void {
    $pathOriginal = 'test' . DIRECTORY_SEPARATOR . 'NORMAL';
    $pathExpected = UtilIO::BASE_DIR . $pathOriginal;

    $pathValid = UtilIO::getInstance()->validPath($pathOriginal);

    expect($pathValid)->toBe($pathExpected);
});

test('Get a valid path with a weird path', function (): void {
    $pathOriginal = '\\..\\.\\\\\\/\././test/////.././NORMAL';
    $pathExpected = UtilIO::BASE_DIR . 'test' . DIRECTORY_SEPARATOR . 'NORMAL';

    $pathValid = UtilIO::getInstance()->validPath($pathOriginal);

    expect($pathValid)->toBe($pathExpected);
});

test('Get a valid filepath with a normal path', function (): void {
    $pathOriginal = 'test' . DIRECTORY_SEPARATOR . 'normal' . DIRECTORY_SEPARATOR . 'file.php';
    $pathExpected = UtilIO::BASE_DIR . $pathOriginal;

    $pathValid = UtilIO::getInstance()->validPath($pathOriginal);

    expect($pathValid)->toBe($pathExpected);
});

test('Get a valid filepath with a weird path', function (): void {
    $pathOriginal = '\\..\\.\\\\\\/\././test/////.././normal' . DIRECTORY_SEPARATOR . '///po.php././' . DIRECTORY_SEPARATOR . '///.//okod..php/..///.././/./././' . DIRECTORY_SEPARATOR . 'file.php';
    $pathExpected = UtilIO::BASE_DIR . 'test' . DIRECTORY_SEPARATOR . 'normal' . DIRECTORY_SEPARATOR . 'po' . DIRECTORY_SEPARATOR . '.php' . DIRECTORY_SEPARATOR . 'okod' . DIRECTORY_SEPARATOR . '.php' . DIRECTORY_SEPARATOR . 'file.php';

    $pathValid = UtilIO::getInstance()->validPath($pathOriginal);

    expect($pathValid)->toBe($pathExpected);
});

test('Get a valid filepath with a normal path short dotfile', function (): void {
    $pathOriginal = '.token';
    $pathExpected = UtilIO::BASE_DIR . $pathOriginal;

    $pathValid = UtilIO::getInstance()->validPath($pathOriginal);

    expect($pathValid)->toBe($pathExpected);
});

test('Get a valid filepath with a normal path long dotfile', function (): void {
    $pathOriginal = 'test' . DIRECTORY_SEPARATOR . 'normal' . DIRECTORY_SEPARATOR . '.dotfolder' . DIRECTORY_SEPARATOR . '.token';
    $pathExpected = UtilIO::BASE_DIR . $pathOriginal;

    $pathValid = UtilIO::getInstance()->validPath($pathOriginal);

    expect($pathValid)->toBe($pathExpected);
});
